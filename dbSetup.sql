drop database if exists networkdb;
create database if not exists networkdb;
use networkdb;

drop table if exists callFailure;

CREATE TABLE IF NOT EXISTS callFailure (
    `id` int auto_increment primary key,
    `dateTime` varchar(100),
    `eventId` INT,
    `failureClass` VARCHAR(6),
    `ueType` INT,
    `market` INT,
    `operator` INT,
    `cellId` INT,
    `duration` INT,
    `causeCode` VARCHAR(6),
    `neVersion` VARCHAR(3),
    `imsi` VARCHAR(30),
    `hier3Id` VARCHAR(50),
    `hier32Id`VARCHAR(50),
    `hier321Id` VARCHAR(50)
);

drop table if exists users;

CREATE TABLE IF NOT EXISTS users (
    `id` int auto_increment primary key,
    userType varchar(100),
    userName varchar(100),
    userPassword varchar(100)
);

insert into users values(null,"Admin","root","root");
CREATE TABLE IF NOT EXISTS eventCause (
	 `id` int auto_increment primary key,
    `causeCode` VARCHAR(6),
    `eventId` INT,
    `description` VARCHAR(300)
    );
    
    CREATE TABLE IF NOT EXISTS failureClass (
     `id` int auto_increment primary key,
	`failureClass` int ,
    `description` VARCHAR(100)
    );
