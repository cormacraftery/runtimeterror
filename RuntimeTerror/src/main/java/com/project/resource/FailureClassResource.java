package com.project.resource;


import java.util.List;

import javax.ws.rs.Produces;
import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.MediaType;

import com.project.model.FailureClass;
import com.project.services.FailureClassService;

@Path("/failureclass")
public class FailureClassResource {
	
	@Inject
	private FailureClassService failureClassService;
	
	public void setFailureClassService(FailureClassService failureClassService) {
		this.failureClassService=failureClassService;
	}
	
	@POST
	@Path("/upload")
	public Response upload() {
		failureClassService.upload();
		return Response.status(200).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/imsicallfailurescount/{fromDate}/{fromTime}/{toDate}/{toTime}")
	public Response findIMSICallFailuresCountAndDurationWithinTimePeriod(@PathParam("fromDate") String fromDate,
			@PathParam("fromTime") String fromTime,
			@PathParam("toDate") String toDate,
			@PathParam("toTime") String toTime) {
			
		List<FailureClass> failureClasses = failureClassService.findIMSICallFailuresCountAndDurationWithinTimePeriod(fromDate, fromTime, toDate, toTime);
		if(failureClasses.isEmpty()) {
			return Response.status(400).entity("No data").build();
		}else {
			return Response.status(200).entity(failureClasses).build();
		}
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/imsicallfailuresgrouped/{fromDate}/{fromTime}/{toDate}/{toTime}")
	public Response findIMSICallFailuresCountAndDurationWithinTimePeriodGrouped(@PathParam("fromDate") String fromDate,
			@PathParam("fromTime") String fromTime,
			@PathParam("toDate") String toDate,
			@PathParam("toTime") String toTime) {
			
		List<FailureClass> failureClasses = failureClassService.getIMSICountAndDurationCallFailuresWithinTimePeriodGrouped(fromDate, fromTime, toDate, toTime);
		if(failureClasses.isEmpty()) {
			return Response.status(400).entity("No data").build();
		}else {
			return Response.status(200).entity(failureClasses).build();
		}
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/imsicallfailuresgrouped/{fromDate}/{fromTime}/{toDate}/{toTime}/{cellId}")
	public Response cellDrillDown(@PathParam("fromDate") String fromDate,
			@PathParam("fromTime") String fromTime,
			@PathParam("toDate") String toDate,
			@PathParam("toTime") String toTime,
			@PathParam("cellId") int cellId) {
			
		List<FailureClass> failureClasses = failureClassService.cellDrillDown(fromDate, fromTime, toDate, toTime, cellId);
		return Response.status(200).entity(failureClasses).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/imsicallfailuresgrouped/{fromDate}/{fromTime}/{toDate}/{toTime}/{cellId}/{failureClassDescription}")
	public Response failureClassDrillDown(@PathParam("fromDate") String fromDate,
			@PathParam("fromTime") String fromTime,
			@PathParam("toDate") String toDate,
			@PathParam("toTime") String toTime,
			@PathParam("cellId") int cellId,
			@PathParam("failureClassDescription") String failureClassDescription) {
			
		List<FailureClass> failureClasses = failureClassService.failureClassDrillDown(fromDate, fromTime, toDate, toTime, cellId, failureClassDescription);
		return Response.status(200).entity(failureClasses).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/imsicallfailuresgrouped/{fromDate}/{fromTime}/{toDate}/{toTime}/{cellId}/{failureClassDescription}/{eventCauseDescription}")
	public Response imsiDrillDown(@PathParam("fromDate") String fromDate,
			@PathParam("fromTime") String fromTime,
			@PathParam("toDate") String toDate,
			@PathParam("toTime") String toTime,
			@PathParam("cellId") int cellId,
			@PathParam("failureClassDescription") String failureClassDescription,
			@PathParam("eventCauseDescription") String eventCauseDescription) {
			
		List<FailureClass> failureClasses = failureClassService.imsiDrillDown(fromDate, fromTime, toDate, toTime, cellId, failureClassDescription, eventCauseDescription);
		return Response.status(200).entity(failureClasses).build();
	}

}
