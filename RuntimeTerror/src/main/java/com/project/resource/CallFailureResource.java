package com.project.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.project.model.CallFailure;
import com.project.model.EventCause;
import com.project.model.InvalidCallFailure;
import com.project.services.CallFailureService;

@Path("/callfailures")
public class CallFailureResource {
	
	@Inject
	private CallFailureService callFailureService;
	
	public void setCallFailureService(CallFailureService callFailureService) {
		this.callFailureService=callFailureService;
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/valid")
	public Response findAllCallFailures() {
		List<CallFailure> callFailures = callFailureService.getAllCallFailures();
		return Response.status(200).entity(callFailures).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/invalid")
	public Response findInvalidCallFailures() {
		List<InvalidCallFailure> invalidCallFailures = callFailureService.getInvalidCallFailures();
		return Response.status(200).entity(invalidCallFailures).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/callfailuredates/{fromDate}/{fromTime}/{toDate}/{toTime}")
	public Response findIMSICallFailuresWithinTimePeriod(@PathParam("fromDate") String fromDate, @PathParam("fromTime") String fromTime,
			@PathParam("toDate") String toDate, @PathParam("toTime") String toTime) {
				List<CallFailure> callFailures = callFailureService.findIMSICallFailuresWithinTimePeriod(fromDate, fromTime, toDate, toTime);
				if(callFailures.isEmpty()) {
					return Response.status(400).entity("No data").build();
				}else {
					return Response.status(200).entity(callFailures).build();
				}
	}
	
	@POST
	@Path("/upload")
	public Response saveCallFailure() {
		callFailureService.uploadValid();
		return Response.status(200).build();
	}
	
	@GET
	@Path("/imsiDate/{startDate}/{startTime}/{endDate}/{endTime}/{imsi}")
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response getImsiCountWithinDate(@PathParam("startDate") String startDate, @PathParam("startTime") String startTime,
			@PathParam("endDate") String endDate, @PathParam("endTime") String endTime, @PathParam("imsi") String imsi) {
		List<CallFailure> imsiCountWithinDate = callFailureService.getImsiCountWithinDate(startDate, startTime, endDate, endTime, imsi);
		return Response.status(200).entity(imsiCountWithinDate).build();
	}
	
	@GET
	@Path("/modelcount/{startDate}/{startTime}/{endDate}/{endTime}/{model}")
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response getModelCountWithinDate(@PathParam("startDate") String startDate, @PathParam("startTime") String startTime,
			@PathParam("endDate") String endDate, @PathParam("endTime") String endTime, @PathParam("model") String model) {
		List<CallFailure> imsiCountWithinDate = callFailureService.getModelCountWithinDate(startDate, startTime, endDate, endTime, model);
		return Response.status(200).entity(imsiCountWithinDate).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/top10imsicallfailures/{fromDate}/{fromTime}/{toDate}/{toTime}")
	public Response findtop10IMSICallFailuresWithinTimePeriod(@PathParam("fromDate") String fromDate, @PathParam("fromTime") String fromTime,
			@PathParam("toDate") String toDate, @PathParam("toTime") String toTime) {
				List<CallFailure> callFailures = callFailureService.getTop10ImsiCallfailuresWithinDate(fromDate, fromTime, toDate, toTime);
				if(callFailures.isEmpty()) {
					return Response.status(400).entity("No data").build();
				}else {
					return Response.status(200).entity(callFailures).build();
				}
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/top10imsicallfailures/{fromDate}/{fromTime}/{toDate}/{toTime}/{imsi}")
	public Response top10ImsiDrillDown(@PathParam("fromDate") String fromDate, @PathParam("fromTime") String fromTime,
			@PathParam("toDate") String toDate, @PathParam("toTime") String toTime, @PathParam("imsi") String imsi) {
				List<CallFailure> callFailures = callFailureService.top10ImsiDrillDown(fromDate, fromTime, toDate, toTime, imsi);
				return Response.status(200).entity(callFailures).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/top10imsicallfailures/{fromDate}/{fromTime}/{toDate}/{toTime}/{imsi}/{cellId}")
	public Response top10ImsiSecondDrillDown(@PathParam("fromDate") String fromDate, @PathParam("fromTime") String fromTime,
			@PathParam("toDate") String toDate, @PathParam("toTime") String toTime, @PathParam("imsi") String imsi, 
			@PathParam("cellId") int cellId) {
				List<CallFailure> callFailures = callFailureService.top10ImsiSecondDrillDown(fromDate, fromTime, toDate, toTime, imsi, cellId);
				return Response.status(200).entity(callFailures).build();
	}
	
	@GET
	@Path("/Top10Model/{startDate}/{startTime}/{endDate}/{endTime}")
	@Produces( {MediaType.APPLICATION_JSON} )
	public Response getTop10ModelOperatorCellId(@PathParam("startDate") String startDate, @PathParam("startTime") String startTime,
			@PathParam("endDate") String endDate, @PathParam("endTime") String endTime) {
		List<CallFailure> top10ModelOperatorCellId = callFailureService.findTop10ModelOperatorCellId(startDate, startTime, endDate, endTime);
		if(top10ModelOperatorCellId.isEmpty()) {
			return Response.status(400).entity("No data").build();
		}else {
			return Response.status(200).entity(top10ModelOperatorCellId).build();
		}
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/Top10Model/{fromDate}/{fromTime}/{endDate}/{endTime}/{market}/{operator}/{cellId}")
	public Response top10ModelDrillDown(@PathParam("fromDate") String fromDate, @PathParam("fromTime") String fromTime, @PathParam("endDate") String toDate, 
			@PathParam("endTime") String toTime, @PathParam("market") int market, @PathParam("operator") int operator, @PathParam("cellId") int cellId) {
		List<CallFailure> callFailures = callFailureService.top10ModelDrillDown(fromDate, fromTime, toDate, toTime, market, operator, cellId);
		return Response.status(200).entity(callFailures).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/Top10Model/{fromDate}/{fromTime}/{endDate}/{endTime}/{market}/{operator}/{cellId}/{description}")
	public Response top10ModelSecondDrillDown(@PathParam("fromDate") String fromDate, @PathParam("fromTime") String fromTime, @PathParam("endDate") String toDate, 
			@PathParam("endTime") String toTime, @PathParam("market") int market, @PathParam("operator") int operator, @PathParam("cellId") int cellId, 
			@PathParam("description") String description) {
		List<CallFailure> callFailures = callFailureService.top10ModelSecondDrillDown(fromDate, fromTime, toDate, toTime, market, operator, cellId, description);
		return Response.status(200).entity(callFailures).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/failureclass/{failureClass}")
	public Response getIMSIFailureCause(@PathParam("failureClass") String failureClass) {
		List<CallFailure> imsiFailureCause = callFailureService.getIMSIFailureCause(failureClass);
		return Response.status(200).entity(imsiFailureCause).build();
	}


}
