package com.project.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.project.model.EventCause;
import com.project.services.EventCauseService;

@Path("/eventcause")
public class EventCauseResource {
	
	@Inject
	private EventCauseService eventCauseService;
	
	public void setEventCauseService(EventCauseService eventCauseService) {
		this.eventCauseService=eventCauseService;
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/imsi/{imsi}")
	public Response findImsiEvents(@PathParam("imsi") String imsi) {
		List<EventCause> eventCause = eventCauseService.findImsiEvents(imsi);
		return Response.status(200).entity(eventCause).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/imsiUnique/{imsi}")
	public Response findImsiEventsUnique(@PathParam("imsi") String imsi) {
		List<EventCause> eventCause = eventCauseService.findImsiEventsUnique(imsi);
		return Response.status(200).entity(eventCause).build();
	}
	
	@POST
	@Path("/upload")
	public Response upload() {
		eventCauseService.upload();
		return Response.status(200).build();
	}

}
