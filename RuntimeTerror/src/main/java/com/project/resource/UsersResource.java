package com.project.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.project.model.Users;
import com.project.services.UsersService;

@Path("/users")
public class UsersResource {

	@Inject
	UsersService usersServices;
	
	public void setUserService(UsersService usersService) {
		this.usersServices=usersService;
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/valid")
	public Response findValidUsers() {
		List<Users> users = usersServices.getAllUsers();
		return Response.status(200).entity(users).build();
	}
	
	@POST
	@Path("/valid")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response saveUser(Users user) {
		usersServices.save(user);
		return Response.status(201).entity(user).build();
	}
	
	@GET
	@Path("/search/{userName}/{password}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findUser(@PathParam("userName") String userName, @PathParam("password") String password) {
		List<Users> usersList=usersServices.getSpecificUser(userName, password);
		if(usersList.isEmpty()) {
			return Response.status(400).entity("Invalid credentials entered").build();
		} else {
			return Response.status(200).entity(usersList).build();
		}
	}
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/{password}")
	public Response generatePasswordHash(@PathParam("password") String password) {
		String passwordHash = usersServices.generatePasswordHash(password);
		return Response.status(200).entity(passwordHash).build();
	}
	
}
