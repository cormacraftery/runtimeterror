package com.project.resource;

import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.project.model.CallFailure;
import com.project.model.EventCause;
import com.project.model.UE;
import com.project.services.EventCauseService;
import com.project.services.UEService;

@Path("/ues")
public class UEResource {

	@Inject
	private UEService ueService;

	public void setUEService(UEService ueService) {
		this.ueService = ueService;
	}
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/uetable")
	public Response getAllUEs() {
		List<UE> ues = ueService.getAllUE();
		return Response.status(200).entity(ues).build();
	}
	
	@POST
	@Path("/upload")
	public Response upload() {
		ueService.upload();
		return Response.status(200).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/phonemodel/{phonemodel}")
	public Response findPhoneModel(@PathParam("phonemodel") String phoneModel) {
		List<UE> ue = ueService.findPhoneModel(phoneModel);
		if(ue.isEmpty()) {
			return Response.status(400).entity("No data").build();
		} else {
			return Response.status(200).entity(ue).build();
		}
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/phonemodel/{phonemodel}/{causeCode}/{eventId}")
	public Response getCellValuesForPhoneModel(@PathParam("phonemodel") String phoneModel, @PathParam("causeCode") String causeCode, @PathParam("eventId") int eventId) {
		List<UE> ue = ueService.getCellValuesForPhoneModel(phoneModel, causeCode, eventId);
		return Response.status(200).entity(ue).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/phonemodel/{phonemodel}/{causeCode}/{eventId}/{cellId}")
	public Response getImsisForCellIds(@PathParam("phonemodel") String phoneModel, @PathParam("causeCode") String causeCode, 
			@PathParam("eventId") int eventId, @PathParam("cellId") int cellId) {
		List<UE> ue = ueService.getImsisForCellIds(phoneModel, causeCode, eventId, cellId);
		return Response.status(200).entity(ue).build();
	}
	
}
