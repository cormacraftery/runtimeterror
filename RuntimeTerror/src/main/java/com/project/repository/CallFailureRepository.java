package com.project.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.project.model.CallFailure;
import com.project.model.InvalidCallFailure;

@Stateless
public class CallFailureRepository {
	
	@PersistenceContext
	public EntityManager em;
	
	public List<CallFailure> getAllCallFailures() {
		Query query = em.createQuery("SELECT w FROM CallFailure w");
		List<CallFailure> totalList = query.getResultList();
		return totalList;
	}
	
	public List<InvalidCallFailure> getInvalidCallFailures() {
		Query query = em.createQuery("SELECT w FROM InvalidCallFailure w");
		List<InvalidCallFailure> invalidList = query.getResultList();
		return invalidList;
	}
	
public List<CallFailure> getIMSICallFailuresWithinTimePeriod(String fromDate, String fromTime, String toDate, String toTime) {
		
		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);

		Query query1 = em.createQuery(
				          "SELECT distinct CF.dateTime, CF.imsi, CF.failureClass, FCT.description, CF.id "
					    + "FROM CallFailure CF, FailureClass FCT "
						+ "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
						+ "AND CF.failureClass = FCT.failureClass");
		query1.setParameter("formattedFromDate", formattedFromDate);
		query1.setParameter("formattedToDate", formattedToDate);

		return query1.getResultList();
	}
	
	public List<CallFailure> getTop10ModelOperatorCellId(String fromDate, String fromTime, String toDate, String toTime) {
		
		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);
		
		Query query1 = em.createQuery(
				          "SELECT count(distinct CF.id), CF.market, CF.operator, CF.cellId "
					    + "FROM CallFailure CF "
				        + "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
						+ "group by CF.market, CF.operator, CF.cellId ORDER BY count(*) DESC");
		
		query1.setParameter("formattedFromDate", formattedFromDate);
		query1.setParameter("formattedToDate", formattedToDate);
		query1.setMaxResults(10);
		List<CallFailure> callFailures = query1.getResultList();
		return callFailures;
		
	}
	
	public List<CallFailure> top10ModelDrillDown(String fromDate, String fromTime, String toDate, String toTime, int market, int operator, int cellId){

		
		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);
		
		Query query = em.createQuery("select FC.description, count(distinct CF.id) from CallFailure CF, FailureClass FC "

				+ "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
				+ "and CF.market = :market and CF.operator = :operator and "
				+ "CF.cellId = :cellId and "
				+ "CF.failureClass = FC.failureClass "
				+ "group by FC.failureClass,FC.description");
		query.setParameter("formattedFromDate", formattedFromDate);
		query.setParameter("formattedToDate", formattedToDate);
		query.setParameter("market", market);
		query.setParameter("operator", operator);
		query.setParameter("cellId", cellId);
		return query.getResultList();
	}
	
	public List<CallFailure> top10ModelSecondDrillDown(String fromDate, String fromTime, String toDate, String toTime, int market, int operator, int cellId, String failureClassDescription){
		
		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);
		
		Query query = em.createQuery("select CF.causeCode, CF.eventId, count(distinct CF.id), EC.description "
				+ "from EventCause EC, CallFailure CF, FailureClass FC "
				+ "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
				+ "and CF.causeCode = EC.causeCode and CF.eventId = EC.eventId "
				+ "and CF.market = :market and CF.operator = :operator "
				+ "and CF.cellId = :cellId "
				+ "and FC.description = :failureClassDescription "
				+ "and CF.failureClass = FC.failureClass group by CF.causeCode, CF.eventId, EC.description");
		query.setParameter("formattedFromDate", formattedFromDate);
		query.setParameter("formattedToDate", formattedToDate);
		query.setParameter("market", market);
		query.setParameter("operator", operator);
		query.setParameter("cellId", cellId);
		query.setParameter("failureClassDescription", failureClassDescription);
		return query.getResultList();
	}

	public CallFailure save(CallFailure callFailure) {
		em.persist(callFailure);
		return callFailure;
	}
	
	public InvalidCallFailure save(InvalidCallFailure invalidCallFailure) {
		em.persist(invalidCallFailure);
		return invalidCallFailure;
	}
	
	public List<CallFailure> getImsiCountWithinDate(String fromDate, String fromTime, String toDate, String toTime, String imsi) {
		
		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);
		
		System.out.println("From: " + formattedFromDate);
		System.out.println("To: " + formattedToDate);
		
		Query query1 = em.createQuery("SELECT CF.id "
				                    + "FROM CallFailure CF "
				                    + "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
				                    + "AND CF.imsi = :imsi ");
		query1.setParameter("formattedFromDate", formattedFromDate);
		query1.setParameter("formattedToDate", formattedToDate);
		query1.setParameter("imsi", imsi);
		List<CallFailure> callFailures = query1.getResultList();
		return callFailures;
		
	}
	
	public List<CallFailure> getModelCountWithinDate(String fromDate, String fromTime, String toDate, String toTime, String model) {
		
		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);
		
		Query query1 = em.createQuery("SELECT distinct CF.id "
				                    + "FROM CallFailure CF, UE u "
				                    + "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
				                    + "AND CF.ueType = u.tac "
				                    + "AND u.marketingName = :model");
		query1.setParameter("formattedFromDate", formattedFromDate);
		query1.setParameter("formattedToDate", formattedToDate);
		query1.setParameter("model", model);
		List<CallFailure> callFailures = query1.getResultList();
		return callFailures;
	}
	
	public List<CallFailure> getTop10ImsiCallfailuresWithinDate(String fromDate, String fromTime, String toDate, String toTime) {
		
		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);
		
		Query query1 = em.createQuery(
				          "SELECT CF.imsi, count(distinct CF.id) "
					    + "FROM CallFailure CF "
					    + "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
						+ "group by CF.imsi ORDER BY count(*) DESC");
		
		query1.setParameter("formattedFromDate", formattedFromDate);
		query1.setParameter("formattedToDate", formattedToDate);
		query1.setMaxResults(10);
		List<CallFailure> callFailures = query1.getResultList();
		return callFailures;
		
	}
	
	public List<CallFailure> top10ImsiDrillDown(String fromDate, String fromTime, String toDate, String toTime, String imsi){
		
		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);
		
		Query query = em.createQuery("Select CF.cellId, count(distinct CF.id) "
				+ "from CallFailure CF "
				+ "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
				+ "and CF.imsi = :imsi group by CF.cellId");
		query.setParameter("formattedFromDate", formattedFromDate);
		query.setParameter("formattedToDate", formattedToDate);
		
		query.setParameter("imsi", imsi);
		return query.getResultList();
	}
	
	public List<CallFailure> top10ImsiSecondDrillDown(String fromDate, String fromTime, String toDate, String toTime, String imsi, int cellId){
		
		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);
		
		Query query = em.createQuery("Select CF.causeCode, CF.eventId, count(distinct CF.id), EC.description from CallFailure CF, EventCause EC "
				+ "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
				+ "and CF.imsi = :imsi and CF.cellId = :cellId "
				+ "and CF.causeCode = EC.causeCode "
				+ "and CF.eventId = EC.eventId "
				+ "group by EC.description");
		query.setParameter("formattedFromDate", formattedFromDate);
		query.setParameter("formattedToDate", formattedToDate);
		query.setParameter("imsi", imsi);
		query.setParameter("cellId", cellId);
		return query.getResultList();
	}
	
	//Distinct IMSI for failure class
		public List<CallFailure> getIMSIFailureCause(String failureClass) {
			Query query = em.createQuery("Select distinct c.imsi from CallFailure c, FailureClass f where c.failureClass = f.failureClass and f.failureClass = :failureClass");
			query.setParameter("failureClass", failureClass);
			return query.getResultList();
		}
		
		public String dateFormatter(String date, String time) {
			String day = date.substring(0, 2);
			String month = date.substring(3, 5);
			String year = date.substring(8);
			return year + month + day + time.substring(0, 2) + time.substring(3, 5);
		}

}
