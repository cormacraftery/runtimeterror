package com.project.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.project.model.EventCause;

@Stateless
public class EventCauseRepository {
	
	@PersistenceContext
	public EntityManager em;
	
	public EventCause save(EventCause eventCause) {
		em.persist(eventCause);
		return eventCause;
	}
	
	public List<EventCause> getAllEventCauses() {
		return em.createQuery("select e from EventCause as e").getResultList();
	}
	
	public List<EventCause> getCallFailure_IMSI(String imsi) {
		Query query = em.createQuery("SELECT distinct c.dateTime, e.eventId, e.causeCode, e.description FROM CallFailure c, EventCause e where c.causeCode = e.causeCode And c.eventId = e.eventId  AND c.imsi =:imsi");
		query.setParameter("imsi", imsi);
		List<EventCause> eventCause = query.getResultList();
		return eventCause;
	}
	
	public List<EventCause> getCallFailure_IMSI_Unique(String imsi) {
		Query query = em.createQuery("SELECT DISTINCT e.eventId, e.causeCode, e.description FROM CallFailure c, EventCause e where c.causeCode = e.causeCode And c.eventId = e.eventId  AND c.imsi =:imsi");
		query.setParameter("imsi", imsi);
		List<EventCause> eventCause = query.getResultList();
		return eventCause;
	}

}
