package com.project.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.project.model.CallFailure;
import com.project.model.EventCause;
import com.project.model.UE;

@Stateless
public class UERepository {
	
	@PersistenceContext
	public EntityManager em;
	
	public UE save(UE ue) {
		em.persist(ue);
		return ue;
	}
	
	public List<UE> getAllUE() {
		return em.createQuery("select u from UE as u").getResultList();
	}
	
	public List<UE> getPhoneModel(String phoneModel) {
		Query query = em.createQuery(
				"SELECT p.tac, p.marketingName,c.causeCode,c.eventId,count(distinct c.id), e.description FROM UE p, EventCause e, CallFailure c where p.marketingName =:phoneModel and p.tac=c.ueType and e.causeCode=c.causeCode and e.eventId=c.eventId group by c.causeCode,c.eventId");
		query.setParameter("phoneModel", phoneModel);
		return query.getResultList();
	}
	
	public List<UE> getCellValuesForPhoneModel(String phoneModel, String causeCode, int eventId){
		Query query = em.createQuery("select c.cellId, count(distinct c.id) FROM UE p, EventCause e, CallFailure c where p.marketingName =:phoneModel and "
				+ "p.tac=c.ueType and c.eventId=:eventId and c.causeCode=:causeCode"
				+ " and e.causeCode=c.causeCode and e.eventId=c.eventId group by c.cellId");
		query.setParameter("phoneModel", phoneModel).setParameter("eventId", eventId).setParameter("causeCode", causeCode);
		return query.getResultList();
	}
	
	public List<UE> getImsisForCellIds(String phoneModel, String causeCode, int eventId, int cellId){
		Query query = em.createQuery("select distinct c.imsi, sum(c.duration) FROM UE p, EventCause e, CallFailure c where p.marketingName =:phoneModel and "
				+ "p.tac=c.ueType and c.eventId=:eventId and c.causeCode=:causeCode and c.cellId=:cellId "
				+ " and e.causeCode=c.causeCode and e.eventId=c.eventId group by c.imsi");
		query.setParameter("phoneModel", phoneModel).setParameter("eventId", eventId).setParameter("causeCode", causeCode).setParameter("cellId", cellId);
		return query.getResultList();
	}
	
	public long getPhoneModelFailuresWithinTimePeriod(String fromDate, String toDate, String fromTime, String toTime, String model) {
		
		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);

		Query query1 = em.createQuery(
				          "SELECT count(distinct CF.id) "
					    + "FROM CallFailure CF, UE u "
					    + "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
						+ "AND CF.ueType = u.tac and u.marketingName = :model");

		query1.setParameter("formattedFromDate", formattedFromDate);
		query1.setParameter("formattedToDate", formattedToDate);
		query1.setParameter("model", model);
		return (long) query1.getSingleResult();
		
	}
	
	public String dateFormatter(String date, String time) {
		String day = date.substring(0, 2);
		String month = date.substring(3, 5);
		String year = date.substring(8);
		return year + month + day + time.substring(0, 2) + time.substring(3, 5);
	}

}
