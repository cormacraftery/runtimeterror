package com.project.repository;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.project.model.Users;

@Stateless
public class UsersRepository {

	@PersistenceContext
	public EntityManager em;
	
	public List<Users> getAllUsers() {
		Query query = em.createQuery("SELECT w FROM Users w");
		List<Users> totalList = query.getResultList();
		return totalList;
	}
	
	public Users getUsers(int id) {
		return em.find(Users.class, id);
	}
	
	public Users save(Users user){
		user.setUserPassword(generatePasswordHash(user.getUserPassword()));
		em.persist(user);
		return user;
	}
	
	public List<Users> getSpecificUser(String userName, String password) {
		String hashedPassword = generatePasswordHash(password);
    	Query query=em.createQuery("SELECT w FROM Users AS w "+
    								"WHERE w.userName LIKE ?1 and w.userPassword LIKE ?2");
    	query.setParameter(1, userName);
    	query.setParameter(2, hashedPassword);
        return query.getResultList();
    }
	
	public String generatePasswordHash(String password) {
		try {
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
			return Base64.getEncoder().encodeToString(hash);
		} catch(NoSuchAlgorithmException e) {
			return null;
		}
	}
	
}