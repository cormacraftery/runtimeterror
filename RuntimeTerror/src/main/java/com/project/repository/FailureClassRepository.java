package com.project.repository;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import com.project.model.FailureClass;
import javax.persistence.Query;

@Stateless
public class FailureClassRepository {

	@PersistenceContext
	public EntityManager em;

	public FailureClass save(FailureClass failureClass) {
		em.persist(failureClass);
		return failureClass;
	}

	public List<FailureClass> getAllFailureClasses() {
		return em.createQuery("select f from FailureClass as f").getResultList();
	}

	public List<FailureClass> getIMSICountAndDurationCallFailuresWithinTimePeriod(String fromDate, String fromTime,
			String toDate, String toTime) {
		
		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);

		Query query1 = em.createQuery(
				          "SELECT CF.imsi, CF.failureClass, count(distinct CF.id), FCT.description, sum(CF.duration) "
					    + "FROM CallFailure CF ,FailureClass FCT "
					    + "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
						+ "AND CF.failureClass = FCT.failureClass "
						+ "GROUP BY CF.imsi,CF.failureClass,FCT.description");

		query1.setParameter("formattedFromDate", formattedFromDate);
		query1.setParameter("formattedToDate", formattedToDate);
		return query1.getResultList();
		
	}

	public List<FailureClass> getIMSICountAndDurationCallFailuresWithinTimePeriodGrouped(String fromDate,
			String fromTime, String toDate, String toTime) {
		
		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);

		Query query1 = em.createQuery(
				          "SELECT CF.cellId, count(distinct CF.id) " 
					    + "FROM CallFailure CF "
					    + "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
						+ "GROUP BY CF.cellId");

		query1.setParameter("formattedFromDate", formattedFromDate);
		query1.setParameter("formattedToDate", formattedToDate);
		return query1.getResultList();
	}

	public List<FailureClass> cellDrillDown(String fromDate, String fromTime, String toDate, String toTime,
			int cellId) {

		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);

		Query query = em.createQuery("SELECT FC.description, count(distinct CF.id) "
				+ "FROM FailureClass FC, CallFailure CF  " 
				+ "WHERE CF.failureClass = FC.failureClass "
				+ "AND CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate " 
				+ "AND CF.cellId = :cellId group by FC.description");

		query.setParameter("formattedFromDate", formattedFromDate);
		query.setParameter("formattedToDate", formattedToDate);
		query.setParameter("cellId", cellId);
		List<FailureClass> failureClasses = query.getResultList();
		return failureClasses;
	}

	public List<FailureClass> failureClassDrillDown(String fromDate, String fromTime, String toDate, String toTime,
			int cellId, String failureClassDescription) {

		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);

		Query query = em.createQuery("SELECT CF.causeCode, CF.eventId, count(distinct CF.id), EC.description "
				+ "FROM CallFailure CF, FailureClass FCT, EventCause EC "
				+ "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate " 
				+ "AND CF.failureClass = FCT.failureClass "
				+ "AND FCT.description = :failureClassDescription "
				+ "AND CF.cellId = :cellId "
				+ "and CF.causeCode = EC.causeCode "
				+ "and CF.eventId = EC.eventId "
				+ "group by CF.causeCode, CF.eventId, EC.description");

		query.setParameter("formattedFromDate", formattedFromDate);
		query.setParameter("formattedToDate", formattedToDate);
		query.setParameter("failureClassDescription", failureClassDescription);
		query.setParameter("cellId", cellId);
		return query.getResultList();
	}
	
	public List<FailureClass> imsiDrillDown(String fromDate, String fromTime, String toDate, String toTime,
			int cellId, String failureClassDescription, String eventCauseDescription) {

		String formattedFromDate = dateFormatter(fromDate, fromTime);
		String formattedToDate = dateFormatter(toDate, toTime);

		Query query = em.createQuery("select CF.imsi, count(distinct CF.id), sum(CF.duration) from CallFailure CF, FailureClass FC, EventCause EC "
				                   + "WHERE CF.formattedDateTime BETWEEN :formattedFromDate AND :formattedToDate "
				                   + "and FC.description = :failureClassDescription "
				                   + "and EC.description = :eventCauseDescription and CF.cellId = :cellId and CF.failureClass = FC.failureClass "
				                   + "and CF.causeCode = EC.causeCode and CF.eventId = EC.eventId group by CF.imsi");

		query.setParameter("formattedFromDate", formattedFromDate);
		query.setParameter("formattedToDate", formattedToDate);
		query.setParameter("eventCauseDescription", eventCauseDescription);
		query.setParameter("failureClassDescription", failureClassDescription);
		query.setParameter("cellId", cellId);
		return query.getResultList();
	}
	
	public String dateFormatter(String date, String time) {
		String day = date.substring(0, 2);
		String month = date.substring(3, 5);
		String year = date.substring(8);
		return year + month + day + time.substring(0, 2) + time.substring(3, 5);
	}

}
