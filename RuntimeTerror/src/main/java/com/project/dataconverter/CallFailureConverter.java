package com.project.dataconverter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.project.model.CallFailure;

public class CallFailureConverter {

	public List<CallFailure> convertCallFailuresToList() {
		try {
			List<CallFailure> callFailures = new ArrayList<CallFailure>();
			FileManager fileManager = new FileManager();
			String[] fileNames = fileManager.getFilenames();
			for (String fileName : fileNames) {
				FileInputStream inputStream = fileManager.getFile(fileName);
				Workbook workbook = new HSSFWorkbook(inputStream);
				Sheet firstSheet = workbook.getSheetAt(0);
				Iterator<Row> rowIterator = firstSheet.iterator();
				rowIterator.next();
				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();
					callFailures.add(convertRowToCallFailure(row));
				}
				fileManager.moveFile(fileName, "data", "imported-data");
			}
			fileManager.moveFile("dummy.txt", "imported-data", "data");
			return callFailures;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	public CallFailure convertRowToCallFailure(Row row) {
		DataFormatter formatter = new DataFormatter();
		CallFailure callFailure = new CallFailure();
		callFailure.setDateTime(formatter.formatCellValue(row.getCell(0)));
		if (formatter.formatCellValue(row.getCell(0)).length() == 13) {
			callFailure.setFormattedDateTime(formatter.formatCellValue(row.getCell(0)).substring(5, 7) + "0"
					+ formatter.formatCellValue(row.getCell(0)).substring(0, 1)
					+ formatter.formatCellValue(row.getCell(0)).substring(2, 4)
					+ formatter.formatCellValue(row.getCell(0)).substring(8, 10)
					+ formatter.formatCellValue(row.getCell(0)).substring(11));
		} else {
			callFailure.setFormattedDateTime(formatter.formatCellValue(row.getCell(0)).substring(5, 7) + "0"
					+ formatter.formatCellValue(row.getCell(0)).substring(0, 1)
					+ formatter.formatCellValue(row.getCell(0)).substring(2, 4) + "0"
					+ formatter.formatCellValue(row.getCell(0)).substring(8, 9)
					+ formatter.formatCellValue(row.getCell(0)).substring(10));
		}

		callFailure.setEventId((int) row.getCell(1).getNumericCellValue());
		callFailure.setFailureClass(formatter.formatCellValue(row.getCell(2)));
		callFailure.setUeType((int) row.getCell(3).getNumericCellValue());
		callFailure.setMarket((int) row.getCell(4).getNumericCellValue());
		callFailure.setOperator((int) row.getCell(5).getNumericCellValue());
		callFailure.setCellId((int) row.getCell(6).getNumericCellValue());
		callFailure.setDuration((int) row.getCell(7).getNumericCellValue());
		callFailure.setCauseCode(formatter.formatCellValue(row.getCell(8)));
		callFailure.setNeVersion(formatter.formatCellValue(row.getCell(9)));
		callFailure.setImsi(formatter.formatCellValue(row.getCell(10)));
		callFailure.setHier3Id(formatter.formatCellValue(row.getCell(11)));
		callFailure.setHier32Id(formatter.formatCellValue(row.getCell(12)));
		callFailure.setHier321Id(formatter.formatCellValue(row.getCell(13)));
		return callFailure;
	}

}
