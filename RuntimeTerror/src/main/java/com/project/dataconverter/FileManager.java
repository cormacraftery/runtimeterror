package com.project.dataconverter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FileManager {

	private String webAppFilePath;

	public FileManager() {
		checkFilePaths(filePaths);
	}

	private String[] filePaths = {
			"C:\\Users\\Cormac\\OneDrive\\Documents\\College\\AIT\\Sem2\\GroupProject\\junits\\master\\RuntimeTerror\\src\\main\\webapp\\",
			"C:\\Users\\35385\\Downloads\\SprintTwoRepo2\\runtimeterror\\RuntimeTerror\\src\\main\\webapp\\",
			"D:\\MASE\\Group Project\\Git6\\runtimeterror\\RuntimeTerror\\src\\main\\webapp\\",
			// *** OTHERS TO ADD FILEPATHS TO \\webapp\\ folders in their projects *** //
	};

	private void checkFilePaths(String[] filePaths) {
		for (String filePath : filePaths) {
			if (Files.exists(Paths.get(filePath))) {
				this.webAppFilePath = filePath;
				break;
			}
		}
	}

	public String[] getFilenames() {
		File dataFileDirectory = new File(webAppFilePath + "data\\");
		String[] filenames = dataFileDirectory.list();
		return filenames;
	}

	public FileInputStream getFile(String fileName) {
		FileInputStream fileInputStream = null;
		try {
			fileInputStream = new FileInputStream(webAppFilePath + "data\\" + fileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return fileInputStream;
	}

	public void moveFile(String fileName, String sourceFolder, String targetFolder) {
		Path sourcePath = Paths.get(webAppFilePath + sourceFolder + "\\" + fileName);
		Path targetPath = Paths.get(webAppFilePath + targetFolder + "\\" + fileName);
		try {
			Files.move(sourcePath, targetPath);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
