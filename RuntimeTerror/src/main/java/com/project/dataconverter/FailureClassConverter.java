package com.project.dataconverter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.project.model.FailureClass;

public class FailureClassConverter {
	
	public List<FailureClass> convertFailureClassesToList() {
		try {
			List<FailureClass> failureClasses = new ArrayList<>();
			FileManager fileManager = new FileManager();
			String[] fileNames = fileManager.getFilenames();
			for (String fileName : fileNames) {
				FileInputStream inputStream= fileManager.getFile(fileName);
				Workbook workbook = new HSSFWorkbook(inputStream);
				Sheet firstSheet = workbook.getSheetAt(2);
				Iterator<Row> rowIterator = firstSheet.iterator();
				rowIterator.next();
				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();
					failureClasses.add(convertRowToFailureClass(row));
				}
			}
			return failureClasses;
		} catch(Exception e) {
			return null;
		}
	}
	
	public FailureClass convertRowToFailureClass(Row row) {
		DataFormatter formatter = new DataFormatter();
		FailureClass failureClass = new FailureClass();
		failureClass.setFailureClass(formatter.formatCellValue(row.getCell(0)));
		failureClass.setDescription(formatter.formatCellValue(row.getCell(1)));
		return failureClass;
	}

}
