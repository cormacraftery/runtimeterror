package com.project.dataconverter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.project.model.EventCause;
import com.project.model.UE;

public class UEConverter {

	public List<UE> convertUEToList() {
		try {
			List<UE> ues = new ArrayList<>();
			FileManager fileManager = new FileManager();
			String[] fileNames = fileManager.getFilenames();
			for (String fileName : fileNames) {
				FileInputStream inputStream= fileManager.getFile(fileName);
				Workbook workbook = new HSSFWorkbook(inputStream);
				Sheet firstSheet = workbook.getSheetAt(3);
				Iterator<Row> rowIterator = firstSheet.iterator();
				rowIterator.next();
				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();
					ues.add(convertRowToUE(row));
				}
			}
			return ues;
		} catch (Exception e) {
			return null;
		}
	}

	public UE convertRowToUE(Row row) {
		DataFormatter formatter = new DataFormatter();
		UE ue = new UE();
		ue.setTac((int) row.getCell(0).getNumericCellValue());
		ue.setMarketingName(formatter.formatCellValue(row.getCell(1)));
		ue.setManufacturer(formatter.formatCellValue(row.getCell(2)));
		ue.setAccessCapability(formatter.formatCellValue(row.getCell(3)));
		return ue;
	}

}
