package com.project.dataconverter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.project.model.EventCause;

public class EventCauseConverter {
	
	public List<EventCause> convertEventCauseToList() {
		try {
			List<EventCause> eventCauses = new ArrayList<>();
			FileManager fileManager = new FileManager();
			fileManager.moveFile("dummy.txt", "data", "imported-data");
			String[] fileNames = fileManager.getFilenames();
			for (String fileName : fileNames) {
				FileInputStream inputStream= fileManager.getFile(fileName);
				Workbook workbook = new HSSFWorkbook(inputStream);
				Sheet firstSheet = workbook.getSheetAt(1);
				Iterator<Row> rowIterator = firstSheet.iterator();
				rowIterator.next();
				while (rowIterator.hasNext()) {
					Row row = rowIterator.next();
					eventCauses.add(convertRowToEventCause(row));
				}
			}
			return eventCauses;
		} catch (Exception e) {
			return null;
		}
	}
	
	public EventCause convertRowToEventCause(Row row) {
		DataFormatter formatter = new DataFormatter();
		EventCause eventCause = new EventCause();
		eventCause.setCauseCode(formatter.formatCellValue(row.getCell(0)));
		eventCause.setEventId((int) row.getCell(1).getNumericCellValue());
		eventCause.setDescription(formatter.formatCellValue(row.getCell(2)));
		return eventCause;
	}

}
