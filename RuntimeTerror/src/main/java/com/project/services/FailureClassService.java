package com.project.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.ejb.Local;
import javax.inject.Inject;

import com.project.model.FailureClass;
import com.project.repository.CallFailureRepository;
import com.project.repository.FailureClassRepository;

@Local
public interface FailureClassService {
	
	public List<FailureClass> upload();
	
	public List<FailureClass> findIMSICallFailuresCountAndDurationWithinTimePeriod(String fromDate, String fromTime, String toDate,  String toTime);
	
	public List<FailureClass> getIMSICountAndDurationCallFailuresWithinTimePeriodGrouped(String fromDate, String fromTime, String toDate, String toTime);
	
	public List<FailureClass> cellDrillDown(String fromDate, String fromTime, String toDate, String toTime, int cellId);
	
	public List<FailureClass> failureClassDrillDown(String fromDate, String fromTime, String toDate, String toTime, int cellId, String description);
	
	public List<FailureClass> imsiDrillDown(String fromDate, String fromTime, String toDate, String toTime,
			int cellId, String failureClassDescription, String eventCauseDescription);

}
