package com.project.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.ejb.Local;


import com.project.model.CallFailure;
import com.project.model.InvalidCallFailure;

@Local
public interface CallFailureService {
	
    public List<CallFailure> getAllCallFailures();
	
	public List<InvalidCallFailure> getInvalidCallFailures();
	
	public List<CallFailure> findIMSICallFailuresWithinTimePeriod(String fromDate, String fromTime, String toDate, String toTime);
	
	public List<CallFailure> findTop10ModelOperatorCellId(String fromDate, String fromTime, String toDate, String toTime);
	
	public List<CallFailure> uploadValid();
	
	public List<InvalidCallFailure> uploadInvalid(List<CallFailure> callFailures);
	
	public InvalidCallFailure convertValidToInvalid(CallFailure callFailure);
	
	public String determineInvalidReason(CallFailure callFailure);
	
	public boolean verifyCallFailure(CallFailure callFailure);
	
	public List<CallFailure> getImsiCountWithinDate(String startDate, String startTime, String endDate, String endTime, String imsi);
	
	public List<CallFailure> getModelCountWithinDate(String startDate, String startTime, String endDate, String endTime, String model);

	public List<CallFailure> getTop10ImsiCallfailuresWithinDate(String startDate, String startTime, String endDate, String endTime);
	
	public List<CallFailure> top10ModelDrillDown(String fromDate, String fromTime, String toDate, String toTime, int market, int operator, int cellId);
	
	public List<CallFailure> top10ImsiDrillDown(String fromDate, String fromTime, String toDate, String toTime, String imsi);
	
	public List<CallFailure> top10ModelSecondDrillDown(String fromDate, String fromTime, String toDate, String toTime, int market, int operator, int cellId, String description);
	
	public List<CallFailure> getIMSIFailureCause(String failureClass);
	
	public List<CallFailure> top10ImsiSecondDrillDown(String fromDate, String fromTime, String toDate, String toTime, String imsi, int cellId);

}

