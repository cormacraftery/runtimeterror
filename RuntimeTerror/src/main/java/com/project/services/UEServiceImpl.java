package com.project.services;

import javax.ejb.Stateless;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import com.project.dataconverter.UEConverter;
import com.project.model.UE;
import com.project.repository.UERepository;
@Stateless
public class UEServiceImpl implements UEService{

	UEConverter ueConverter = new UEConverter();
	@Inject
	public UERepository ueRepository;
	
	@Override
	public List<UE> getAllUE() {
		return ueRepository.getAllUE();
	}
	
	@Override
	public List<UE> upload(){
		List<UE> ues = ueConverter.convertUEToList();
		List<UE> ueList = new ArrayList<>();
		for(UE ue:ues) {
			ueRepository.save(ue);
			ueList.add(ue);
		}
		return ueList;
	}

	
	@Override
	public List<UE> findPhoneModel(String phoneModel) {
		return ueRepository.getPhoneModel(phoneModel);
	}
	
	public List<UE> getCellValuesForPhoneModel(String phoneModel, String causeCode, int eventId){
		return ueRepository.getCellValuesForPhoneModel(phoneModel, causeCode, eventId);
	}
	
	public List<UE> getImsisForCellIds(String phoneModel, String causeCode, int eventId, int cellId){
		return ueRepository.getImsisForCellIds(phoneModel, causeCode, eventId, cellId);
	}

}