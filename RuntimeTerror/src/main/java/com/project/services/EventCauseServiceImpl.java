package com.project.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.project.dataconverter.EventCauseConverter;
import com.project.model.EventCause;
import com.project.repository.EventCauseRepository;

@Stateless
public class EventCauseServiceImpl implements EventCauseService {
	
	EventCauseConverter eventCauseConverter = new EventCauseConverter();
	
	@Inject
	public EventCauseRepository eventCauseRepository;
	
	@Override
	public List<EventCause> findImsiEvents(String imsi) {
		return eventCauseRepository.getCallFailure_IMSI(imsi);
	}
	
	@Override
	public List<EventCause> upload() {
		List<EventCause> eventCauses = eventCauseConverter.convertEventCauseToList();
		List<EventCause> eventCausesList = new ArrayList<>();
		for (EventCause eventCause : eventCauses) {
			eventCausesList.add(eventCause);
			eventCauseRepository.save(eventCause);
		}
		return eventCausesList;
	}
	
	@Override
	public List<EventCause> findImsiEventsUnique(String imsi){
		return eventCauseRepository.getCallFailure_IMSI_Unique(imsi);
	}

}
