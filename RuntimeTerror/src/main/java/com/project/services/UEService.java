package com.project.services;

import java.util.List;

import javax.ejb.Local;

import com.project.model.UE;

@Local
public interface UEService {
	
    public List<UE> getAllUE();
	
    public List<UE> upload();
    
    public List<UE> findPhoneModel(String phoneModel);
    
    public List<UE> getCellValuesForPhoneModel(String phoneModel, String causeCode, int eventId);
    
    public List<UE> getImsisForCellIds(String phoneModel, String causeCode, int eventId, int cellId);

}