package com.project.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;


import com.project.dataconverter.CallFailureConverter;
import com.project.model.CallFailure;
import com.project.model.InvalidCallFailure;
import com.project.repository.CallFailureRepository;

@Stateless
public class CallFailureServiceImpl implements CallFailureService {
	
	CallFailureConverter callFailureConverter = new CallFailureConverter();

	@Inject
	public CallFailureRepository callFailureRepository;

	@Override
	public List<CallFailure> getAllCallFailures() {
		return callFailureRepository.getAllCallFailures();
	}

	@Override
	public List<InvalidCallFailure> getInvalidCallFailures() {
		return callFailureRepository.getInvalidCallFailures();
	}

	@Override
	public List<CallFailure> findIMSICallFailuresWithinTimePeriod(String fromDate, String fromTime ,String toDate, String toTime) {
		return callFailureRepository.getIMSICallFailuresWithinTimePeriod(fromDate, fromTime, toDate , toTime);
	}
	
	@Override
	public List<CallFailure> findTop10ModelOperatorCellId(String fromDate, String fromTime ,String toDate, String toTime) {
		return callFailureRepository.getTop10ModelOperatorCellId(fromDate, fromTime, toDate, toTime);
	}
	
	@Override
	public List<CallFailure> getTop10ImsiCallfailuresWithinDate(String startDate, String startTime, String endDate,
			String endTime) {
		return callFailureRepository.getTop10ImsiCallfailuresWithinDate(startDate, startTime, endDate, endTime);
	}
	
	
	@Override
	public List<CallFailure> uploadValid() {
		List<CallFailure> callFailures = callFailureConverter.convertCallFailuresToList();
		List<CallFailure> validCallFailures = new ArrayList<>();
		List<CallFailure> invalidCallFailures = new ArrayList<>();
		for(CallFailure callFailure:callFailures) {
			if(verifyCallFailure(callFailure)) {
				validCallFailures.add(callFailure);
				callFailureRepository.save(callFailure);
			} else {
				invalidCallFailures.add(callFailure);
			}
		}
		uploadInvalid(invalidCallFailures);
		return validCallFailures;
	}
	
	@Override
	public List<InvalidCallFailure> uploadInvalid(List<CallFailure> callFailures) {
		List<InvalidCallFailure> invalidCallFailures = new ArrayList<>();
		for(CallFailure callFailure:callFailures) {
			InvalidCallFailure invalidCallFailure = convertValidToInvalid(callFailure);
			invalidCallFailures.add(invalidCallFailure);
			invalidCallFailure.setInvalidReason(determineInvalidReason(callFailure));
			callFailureRepository.save(invalidCallFailure);
		}
		return invalidCallFailures;
	}
	
	@Override
	public boolean verifyCallFailure(CallFailure callFailure) {
		boolean callFailureIsValid = true;
		if (callFailure.getEventId() == 4099 || callFailure.getCauseCode() == "(null)"
				|| callFailure.getFailureClass().equals("(null)") || Integer.parseInt(callFailure.getFailureClass()) > 4
				|| callFailure.getUeType() == 21060810 || callFailure.getUeType() == 33000255
				|| callFailure.getUeType() == 33000257
				|| (callFailure.getMarket() == 344 && callFailure.getOperator() == 935)
				|| (callFailure.getMarket() == 345 && callFailure.getOperator() == 930)
				|| (callFailure.getMarket() == 355 && callFailure.getOperator() == 930)) {
			callFailureIsValid = false;
		}
		return callFailureIsValid;
	}
	
	@Override
	public InvalidCallFailure convertValidToInvalid(CallFailure callFailure) {
		InvalidCallFailure invalidCallFailure = new InvalidCallFailure();
		invalidCallFailure.setDateTime(callFailure.getDateTime());
		invalidCallFailure.setEventId(callFailure.getEventId());
		invalidCallFailure.setFailureClass(callFailure.getFailureClass());
		invalidCallFailure.setUeType(callFailure.getUeType());
		invalidCallFailure.setMarket(callFailure.getMarket());
		invalidCallFailure.setOperator(callFailure.getOperator());
		invalidCallFailure.setCellId(callFailure.getCellId());
		invalidCallFailure.setDuration(callFailure.getDuration());
		invalidCallFailure.setCauseCode(callFailure.getCauseCode());
		invalidCallFailure.setNeVersion(callFailure.getNeVersion());
		invalidCallFailure.setImsi(callFailure.getImsi());
		invalidCallFailure.setHier3Id(callFailure.getHier3Id());
		invalidCallFailure.setHier32Id(callFailure.getHier32Id());
		invalidCallFailure.setHier321Id(callFailure.getHier321Id());
		return invalidCallFailure;
	}
	
	@Override
	public String determineInvalidReason(CallFailure callFailure) {
		if(callFailure.getEventId() == 4099) {
			return "Invalid event ID";
		} else if(callFailure.getFailureClass() == "(null)") {
			return "Null failure class";
		} else if (callFailure.getCauseCode() == "(null)") {
			return "Null cause code";
		} else if(callFailure.getUeType() == 21060810 || callFailure.getUeType() == 33000255
			|| callFailure.getUeType() == 33000257) {
			return "Invalid UE type";
		} else {
			return "Invalid market and operator values";
		}
	}
	
	@Override
	public List<CallFailure> getImsiCountWithinDate(String startDate, String startTime, String endDate, String endTime, String imsi){
		return callFailureRepository.getImsiCountWithinDate(startDate, startTime, endDate, endTime, imsi);
	}
	
	@Override
	public List<CallFailure> getModelCountWithinDate(String startDate, String startTime, String endDate, String endTime, String model){
		return callFailureRepository.getModelCountWithinDate(startDate, startTime, endDate, endTime, model);
	}
	
	@Override
	public List<CallFailure> top10ModelDrillDown(String fromDate, String fromTime, String toDate, String toTime, int market, int operator, int cellId){
		return callFailureRepository.top10ModelDrillDown(fromDate, fromTime, toDate, toTime, market, operator, cellId);
	}
	
	@Override
	public List<CallFailure> top10ModelSecondDrillDown(String fromDate, String fromTime, String toDate, String toTime, int market, int operator, int cellId, String description){
		return callFailureRepository.top10ModelSecondDrillDown(fromDate, fromTime, toDate, toTime, market, operator, cellId, description);
	}
	
	@Override
	public List<CallFailure> top10ImsiDrillDown(String fromDate, String fromTime, String toDate, String toTime, String imsi){
		return callFailureRepository.top10ImsiDrillDown(fromDate, fromTime, toDate, toTime, imsi);
	}
	
	public List<CallFailure> top10ImsiSecondDrillDown(String fromDate, String fromTime, String toDate, String toTime, String imsi, int cellId){
		return callFailureRepository.top10ImsiSecondDrillDown(fromDate, fromTime, toDate, toTime, imsi, cellId);
	}
	
	@Override
	public List<CallFailure> getIMSIFailureCause(String failureClass) {
		return callFailureRepository.getIMSIFailureCause(failureClass);
	}
	
	

}
