package com.project.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.project.dataconverter.FailureClassConverter;
import com.project.model.FailureClass;
import com.project.repository.FailureClassRepository;

@Stateless
public class FailureClassServiceImpl implements FailureClassService {
	
	FailureClassConverter failureClassConverter = new FailureClassConverter();
	
	@Inject
	public FailureClassRepository failureClassRepository;
	
	public List<FailureClass> upload(){
		List<FailureClass> failureClasses = failureClassConverter.convertFailureClassesToList();
		List<FailureClass> failureClassList = new ArrayList<>();
		for(FailureClass failureClass:failureClasses) {
			failureClassRepository.save(failureClass);
			failureClassList.add(failureClass);
		}
		return failureClassList;
	}
	
	@Override
	public List<FailureClass> findIMSICallFailuresCountAndDurationWithinTimePeriod(String fromDate, String fromTime, String toDate, String toTime){
		return failureClassRepository.getIMSICountAndDurationCallFailuresWithinTimePeriod(fromDate, fromTime, toDate, toTime);
	}
	
	@Override
	public List<FailureClass> getIMSICountAndDurationCallFailuresWithinTimePeriodGrouped(String fromDate, String fromTime, String toDate, String toTime){
		return failureClassRepository.getIMSICountAndDurationCallFailuresWithinTimePeriodGrouped(fromDate, fromTime, toDate, toTime);
	}
	
	@Override
	public List<FailureClass> cellDrillDown(String fromDate, String fromTime, String toDate, String toTime, int cellId){
		return failureClassRepository.cellDrillDown(fromDate, fromTime, toDate, toTime, cellId);
	}
	
	@Override
	public List<FailureClass> failureClassDrillDown(String fromDate, String fromTime, String toDate, String toTime, int cellId, String description){
		return failureClassRepository.failureClassDrillDown(fromDate, fromTime, toDate, toTime, cellId, description);
	}
	
	@Override
	public List<FailureClass> imsiDrillDown(String fromDate, String fromTime, String toDate, String toTime,
			int cellId, String failureClassDescription, String eventCauseDescription){
		return failureClassRepository.imsiDrillDown(fromDate, fromTime, toDate, toTime, cellId, failureClassDescription, eventCauseDescription);
	}

}
