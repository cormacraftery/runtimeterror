package com.project.services;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ejb.Local;

import com.project.model.Users;

@Local
public interface UsersService {
	
	public List<Users> getAllUsers();
	public Users getUsers(int id);
	public Users save(Users user);
	public List<Users> getSpecificUser(String userName, String password);
	public String generatePasswordHash(String password);

}
