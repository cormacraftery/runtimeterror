package com.project.services;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import com.project.model.Users;
import com.project.repository.UsersRepository;

@Stateless
public class UsersServiceImpl implements UsersService{

	@Inject
	public UsersRepository usersRepository;
	
	@Override
	public List<Users> getAllUsers() {
		return usersRepository.getAllUsers();
	}

	@Override
	public Users getUsers(int id) {
		return usersRepository.getUsers(id);
	}

	@Override
	public Users save(Users user) {
		return usersRepository.save(user);
	}

	@Override
	public List<Users> getSpecificUser(String userName, String password) {
		return usersRepository.getSpecificUser(userName, password);
	}
	
	@Override
	public String generatePasswordHash(String password) {
		return usersRepository.generatePasswordHash(password);
	}

}

