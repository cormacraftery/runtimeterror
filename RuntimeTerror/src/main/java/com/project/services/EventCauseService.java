package com.project.services;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

import javax.ejb.Local;

import com.project.model.EventCause;

@Local
public interface EventCauseService {
	
	public List<EventCause> findImsiEvents(String imsi);
	
	public List<EventCause> findImsiEventsUnique(String imsi);
	
	public List<EventCause> upload();

}
