
/* ************************* GLOBAL VARIABLES ************************* */

var userUrl = "http://localhost:8080/RuntimeTerror/rest/users/";
var callFailureUrl = "http://localhost:8080/RuntimeTerror/rest/callfailures/";
var eventCauseUrl = "http://localhost:8080/RuntimeTerror/rest/eventcause";
var UeUrl = "http://localhost:8080/RuntimeTerror/rest/ues/";
var failureClassUrl = "http://localhost:8080/RuntimeTerror/rest/failureclass/"

var uploadUrls = [
	"http://localhost:8080/RuntimeTerror/rest/callfailures/upload",
	"http://localhost:8080/RuntimeTerror/rest/eventcause/upload",
	"http://localhost:8080/RuntimeTerror/rest/failureclass/upload",
	"http://localhost:8080/RuntimeTerror/rest/ues/upload"
];

var loggedIn = false;
var userType = "";
var dataImported = false;
var autoImportTimer;

/* ************************* IMPORTING DATA ************************* */

var importDataButtonClicked = function() {
	openModal('#importDataModal');
	$('#importStatus')
	$('#importStatus').css('color','black');
	$('#importStatus').css('font-size','20px');
	$('#importStatus').text("Importing...");
	clearTimeout(autoImportTimer);
	importData();
}

var importData = function() {
	$('#notificationModal h3').text('Updating database...');
	$('#notificationModal').animate({
		right: '-5%'
	});
	autoImportTimer = 0;
	uploadEventCauses();
}

var uploadEventCauses = function() {
	$.ajax({
		type: 'POST',
		url: eventCauseUrl + "/upload",
		success: uploadFailureClasses
	});
}

var uploadFailureClasses = function() {
	$.ajax({
		type: 'POST',
		url: failureClassUrl + "upload",
		success: uploadUes
	})
}

var uploadUes = function() {
	$.ajax({
		type: 'POST',
		url: UeUrl + "upload",
		success: uploadCallFailures
	})
}

var uploadCallFailures = function() {
	$.ajax({
		type: 'POST',
		url: callFailureUrl + "upload",
		success: function() {
			$('#notificationModal h3').text('Updating database...')
			setTimeout(function() {
				$('#notificationModal h3').text('Database updated!');
				$('#importStatus').text("Import complete!");
				setTimeout(function() {
					$('#notificationModal').animate({
						right: '-25%'
					});
				}, 1500);
				autoImportTimer = setTimeout(importData, 60000);
			}, 1000);
			getInvalidRecordsCount();
		}
	})
}

/* ************************* INVALID RECORDS ************************* */

var getInvalidRecordsCount = function() {
	var invalidRecordsCount = 0;
	$.ajax({
		type: 'GET',
		url: callFailureUrl + "invalid",
		dataType: "json",
		success: function(data) {
			$.each(data, function(index, callfailure) {
				invalidRecordsCount++;
			});
			$('#invalidRecordsMessage').text('Invalid records found: ' + invalidRecordsCount);
			$('#invalidRecordsMessage').css('font-size', '20px');
			$('#invalidRecordsMessage').css('color', 'red');
		}
	})
}

var invalidRecordsButtonClicked = function() {
	openModal('#invalidRecordsModal');
	getInvalidRecords();
}

var getInvalidRecords = function() {
	$.ajax({
		type: 'GET',
		url: callFailureUrl + "invalid",
		dataType: "json",
		success: function(data) {
			renderInvalidRecordsList(data);
		}
	});
};

var renderInvalidRecordsList = function(list) {
	$.each(list, function(index, callFailure) {
		$('#invalidRecordsTableBody').append('<tr><td>' + callFailure.dateTime +
			'</td><td>' + callFailure.eventId + '</td><td>' + callFailure.failureClass + '</td><td>' +
			callFailure.imsi + '</td><td>' +
			callFailure.invalidReason + '</td></tr>');
	});
	$('#invalidRecordsTable').DataTable({
		'bDestroy': true
	});
	$('td').show();
	$('invalidRecordsTable').show();
	$('invalidRecordsTableBody').show();
}

/* ************************* LOG IN ************************* */

var logInModalLogInButtonClicked = function() {
	var task = 'logIn';
	$('#logInUsernameInput').css('border-style', 'hidden');
	$('#logInUsernameInputErrorMessage').text('');
	$('#logInPasswordInput').css('border-style', 'hidden');
	$('#logInPasswordInputErrorMessage').text('');
	if (areFieldsBlank(task) == false) {
		encryptPasswordInput();
	}
}

var encryptPasswordInput = function() {
	$.ajax({
		type: 'GET',
		url: userUrl + $('#logInPasswordInput').val(),
		dataType: 'text',
		success: function(encryptedPassword) {
			tryLogInUser(encryptedPassword);
		}
	})
}

var tryLogInUser = function(encryptedPassword) {
	var userFound = false;
	$.ajax({
		type: 'GET',
		url: userUrl + "valid",
		dataType: 'json',
		success: function(data) {
			$.each(data, function(index, user) {
				if (user.userName == $('#logInUsernameInput').val()) {
					userFound = true;
					if (user.userPassword != encryptedPassword) {
						incorrectCredentialsError();
						return false;
					} else {
						logInUser(user.userName);
						return false;
					}
				}
			})
			if (userFound == false) {
				incorrectCredentialsError();
			}
		}
	})
}

var logInUser = function(username) {
	closeModal('#logInModal');
	$('#navBarLogInButton p').text(username);
	getUserType(username);
	$('#homeLogInButton').hide();
	$('#homeLogOutButton').show();
	clearModalFields('logIn');
	loggedIn = true;
	importData();
}

var getUserType = function(username) {
	var result = "";
	$.ajax({
		type: 'GET',
		url: userUrl + "valid",
		dataType: 'json',
		success: function(data) {
			$.each(data, function(index, user) {
				if (user.userName == username) {
					userType = user.userType;
					updateDisplay(user.userType)
				}
			})
		}

	})
}

/* ************************* CREATE USER ************************* */

var createUserModalCreateUserButtonClicked = function() {
	var task = 'createUser';
	$('#createUserUsernameInput').css('border-style', 'hidden');
	$('#createUserUsernameInputErrorMessage').text('');
	$('#createUserPasswordInput').css('border-style', 'hidden');
	$('#createUserPasswordInputErrorMessage').text('');
	$('#createUserConfirmPasswordInput').css('border-style', 'hidden');
	$('#createUserConfirmPasswordInputErrorMessage').text('');
	$('#createUserSelectErrorMessage').text('');
	if (areFieldsBlank(task) == false) {
		if (doPasswordsMatch() == true) {
			if (areFieldsCorrectLength() == true) {
				if (isPasswordTheCorrectFormat() == true) {
					tryCreateUser();
				}
			}
		}
	}
}

var tryCreateUser = function() {
	var userFound = false;
	$.ajax({
		type: 'GET',
		url: userUrl + 'valid',
		dataType: 'json',
		success: function(users) {
			$.each(users, function(index, user) {
				if (user.userName == $('#createUserUsernameInput').val()) {
					userFound = true;
					userFoundError();
					return false;
				}
			});
			if (userFound == false) {
				createUser()
			}
		}
	});
}

var createUser = function() {
	$.ajax({
		type: 'POST',
		contentType: 'application/json',
		url: userUrl + 'valid',
		dataType: "json",
		data: formToJSON(),
		success: function() {
			clearModalFields('createUser');
			createUserSuccess();
		},
		failure: function() {
			createUserFailure();
		}
	});
}

var formToJSON = function() {
	return JSON.stringify({
		"userType": $('#createUserSelect').val(),
		"userName": $('#createUserUsernameInput').val(),
		"userPassword": $('#createUserPasswordInput').val()
	});
}

/* ************************* LOG OUT ************************* */

var logOutUser = function() {
	closeModal('#logOutConfirmationModal');
	$('#navBarLogInButton p').text('Log In');
	userType = "";
	updateDisplay(userType);
	$('#homeLogInButton').show();
	$('#homeLogOutButton').hide();
	$("#homeModule").get(0).scrollIntoView();
	loggedIn = false;
	clearTimeout(autoImportTimer);
}

/* ************************* EVENT ID, CAUSE CODE ETC BY IMSI ************************* */

var imsiSearchModalRunButtonClicked = function() {
	var task = "imsiSearch";
	var imsi = $('#imsiSearchModalImsiInput').val();
	$('#imsiSearchModalImsiInputErrorMessage').text('');
	$('#imsiSearchModalImsiInput').css('border-style', 'hidden');
	if (areFieldsBlank(task) == false) {
		if ((doesImsiContainOnlyNumbers(imsi, task) == true)) {
			if (isImsiCorrectLength(imsi, task) == true) {
				getImsiSearchResults($('#imsiSearchModalImsiInput').val());
			}
		}
	}
}

var getImsiSearchResults = function(imsi) {
	$.ajax({
		type: 'GET',
		url: eventCauseUrl + "/imsi/" + imsi,
		dataType: "json",
		success: renderImsiSearchResults
	});
}

var renderImsiSearchResults = function(results) {
	$('#imsiSearchResultsModal h2').text('Event ID, Cause Code & Description for IMSI ' + $('#imsiSearchModalImsiInput').val())
	$('#imsiSearchResultsModalTable').DataTable().destroy();
	$('#imsiSearchResultsModalTableBody').empty();
	$.each(results, function(index, imsi) {
		$('#imsiSearchResultsModalTableBody').append(
			'<tr>' +
			'<td>' + imsi[0] + '</td>' +
			'<td>' + imsi[1] + '</td>' +
			'<td>' + imsi[2] + '</td>' +
			'<td>' + imsi[3] + '</td>' +
			'</tr>'
		);
	});
	$('#imsiSearchResultsModalTable').DataTable({
		lengthChange: false,
		pageLength: 8
	});
	openModal('#imsiSearchResultsModal');
}

var imsiSearchUniqueModalRunButtonClicked = function() {
	var task = "imsiSearchUnique";
	var imsi = $('#imsiSearchUniqueModalImsiInput').val();
	$('#imsiSearchUniqueModalImsiInputErrorMessage').text('');
	$('#imsiSearchUniqueModalImsiInput').css('border-style', 'hidden');
	if (areFieldsBlank(task) == false) {
		if ((doesImsiContainOnlyNumbers(imsi, task) == true)) {
			if (isImsiCorrectLength(imsi, task) == true) {
				getImsiSearchUniqueResults($('#imsiSearchUniqueModalImsiInput').val());
			}
		}
	}
}

var getImsiSearchUniqueResults = function(imsi) {
	$.ajax({
		type: 'GET',
		url: eventCauseUrl + "/imsiUnique/" + imsi,
		dataType: "json",
		success: renderImsiSearchUniqueResults
	});
}

var renderImsiSearchUniqueResults = function(results) {
	$('#imsiSearchUniqueResultsModal h2').text('Event ID, Cause Code & Description for IMSI' + $('#imsiSearchUniqueModalImsiInput').val())
	$('#imsiSearchUniqueResultsModalTable').DataTable().destroy();
	$('#imsiSearchUniqueResultsModalTableBody').empty();
	$.each(results, function(index, imsi) {
		$('#imsiSearchUniqueResultsModalTableBody').append(
			'<tr>' +
			'<td>' + imsi[0] + '</td>' +
			'<td>' + imsi[1] + '</td>' +
			'<td>' + imsi[2] + '</td>' +
			'</tr>'
		);
	});
	$('#imsiSearchUniqueResultsModalTable').DataTable({
		lengthChange: false,
		pageLength: 8
	});
	openModal('#imsiSearchUniqueResultsModal');
}

/* ************************* CALL FAILURE COUNT BY IMSI ************************* */

var callFailureCountByImsiModalRunButtonClicked = function() {
	var task = "callFailureCountByImsi";
	var imsi = $('#callFailureCountByImsiModalImsiInput').val();
	var startTime = $('#callFailureCountByImsiModalStartTime').val();
	var endTime = $('#callFailureCountByImsiModalEndTime').val();
	var invalidInput = false;
	$('#callFailureCountByImsiModalImsiInputErrorMessage').text('');
	$('#callFailureCountByImsiModalImsiInput').css('border-style', 'hidden');
	$('#callFailureCountByImsiModalStartDateErrorMessage').text('');
	$('#callFailureCountByImsiModalStartDate').css('border-style', 'hidden');
	$('#callFailureCountByImsiModalStartTimeErrorMessage').text('');
	$('#callFailureCountByImsiModalStartTime').css('border-style', 'hidden');
	$('#callFailureCountByImsiModalEndDateErrorMessage').text('');
	$('#callFailureCountByImsiModalEndDate').css('border-style', 'hidden');
	$('#callFailureCountByImsiModalEndTimeErrorMessage').text('');
	$('#callFailureCountByImsiModalEndTime').css('border-style', 'hidden');
	if (areFieldsBlank(task) == false) {
		if (doesImsiContainOnlyNumbers(imsi, task) == false) {
			invalidInput = true;
		} else if (isImsiCorrectLength(imsi, task) == false) {
			invalidInput = true;
		}
		if (isTimeCorrectFormat(startTime, endTime, task) == false) {
			invalidInput = true;
		}
	} else {
		invalidInput = true;
	}
	if (invalidInput == false) {
		getCallFailureCountByImsi(
			$('#callFailureCountByImsiModalImsiInput').val(),
			formatDate($('#callFailureCountByImsiModalStartDate').val()),
			$('#callFailureCountByImsiModalStartTime').val(),
			formatDate($('#callFailureCountByImsiModalEndDate').val()),
			$('#callFailureCountByImsiModalEndTime').val()
		);
	}
}

var getCallFailureCountByImsi = function(imsi, startDate, startTime, endDate, endTime) {
	var callFailureCountByImsi = 0;
	$.ajax({
		type: 'GET',
		url: callFailureUrl + 'imsiDate/' + startDate + "/" + startTime + "/" + endDate + "/" + endTime + "/" + imsi,
		dataType: "json",
		success: function(data) {
			$.each(data, function(index, callfailure) {
				callFailureCountByImsi++;
			});
			if (callFailureCountByImsi == 1) {
				$('#callFailureCountByImsiResultModalResult').text("Between " + startDate + " at " + startTime + ", and " + endDate + " at " + endTime + ", there was " + callFailureCountByImsi + " Call Failure for IMSI " + imsi + ".");
			} else {
				$('#callFailureCountByImsiResultModalResult').text("Between " + startDate + " at " + startTime + ", and " + endDate + " at " + endTime + ", there were " + callFailureCountByImsi + " Call Failures for IMSI " + imsi + ".");
			}
			$('#callFailureCountByImsiResultModalResult').css('color', 'black');
			$('#callFailureCountByImsiResultModalResult').css('font-size', '20px');
			$('#callFailureCountByImsiResultModalResult').css('font-weight', 'bold');
			openModal('#callFailureCountByImsiResultModal');
		}
	})
}
/* ************************* CALL FAILURE Market Model Id by Date ************************* */

var callFailureModelOperatorIdByDateModalRunButtonClicked = function() {
	var task = "callFailureModelOperatorIdByDate";
	var startTime = $('#callFailureModelOperatorIdByDateModalStartTime').val();
	var endTime = $('#callFailureModelOperatorIdByDateModalEndTime').val();
	var invalidInput = false;
	$('#callFailureModelOperatorIdByDateModalStartDateErrorMessage').text('');
	$('#callFailureModelOperatorIdByDateModalStartDate').css('border-style', 'hidden');
	$('#callFailureModelOperatorIdByDateModalStartTimeErrorMessage').text('');
	$('#callFailureModelOperatorIdByDateModalStartTime').css('border-style', 'hidden');
	$('#callFailureModelOperatorIdByDateModalEndDateErrorMessage').text('');
	$('#callFailureModelOperatorIdByDateModalEndDate').css('border-style', 'hidden');
	$('#callFailureModelOperatorIdByDateModalEndTimeErrorMessage').text('');
	$('#callFailureModelOperatorIdByDateModalEndTime').css('border-style', 'hidden');

	if (areFieldsBlank(task) == false) {
		if (isTimeCorrectFormat(startTime, endTime, task) == false) {
			invalidInput = true;
		}
	} else {
		invalidInput = true;
	}

	if (invalidInput == false) {
		getCallFailureModelOperatorIdByDate(

			formatDate($('#callFailureModelOperatorIdByDateModalStartDate').val()),
			$('#callFailureModelOperatorIdByDateModalStartTime').val(),
			formatDate($('#callFailureModelOperatorIdByDateModalEndDate').val()),
			$('#callFailureModelOperatorIdByDateModalEndTime').val()
		);
	}
}

var getCallFailureModelOperatorIdByDate = function(startDate, startTime, endDate, endTime) {
	$.ajax({
		type: 'GET',
		url: "http://localhost:8080/RuntimeTerror/rest/callfailures/Top10Model/" + startDate + "/" + startTime + "/" + endDate + "/" + endTime,
		dataType: "json",
		success: renderCallFailureModelOperatorResults
	});
}

var renderCallFailureModelOperatorResults = function(callFailurePhoneModelList) {
	
	callFailurePhoneModelList.sort(function(a, b) {
		return b[0] - a[0];
	});
	
	$('#CallFailureModelOperatorIdSearchResultsModalTable').DataTable().destroy();
	$('#CallFailureModelOperatorIdSearchResultsModalTableBody').empty();
	$.each(callFailurePhoneModelList, function(index, callFailure) {
		$('#CallFailureModelOperatorIdSearchResultsModalTableBody').append(
			'<tr>' +
			'<td>' + callFailure[1] + '</td>' +
			'<td>' + callFailure[2] + '</td>' +
			'<td>' + callFailure[3] + '</td>' +
			'<td>' + callFailure[0] + '</td>' +
			'</tr>'
		);
	});
	$('#CallFailureModelOperatorIdResultsModal .dataTable-modal-box').css('width', '50%');
	$('#CallFailureModelOperatorIdSearchResultsModalTable').DataTable({
		lengthChange: false,
		pageLength: 10
	});
	openModal('#CallFailureModelOperatorIdResultsModal');
}
/* ************************* IMSI LIST BY TIME PERIOD ************************* */

var imsiListByTimePeriodModalRunButtonClicked = function() {
	var invalidInput = false;
	var task = "imsiListByTimePeriod";
	var startTime = $('#imsiListByTimePeriodModalStartTime').val();
	var endTime = $('#imsiListByTimePeriodModalEndTime').val();
	$('#imsiListByTimePeriodModalStartDateErrorMessage').text('');
	$('#imsiListByTimePeriodModalStartDate').css('border-style', 'hidden');
	$('#imsiListByTimePeriodModalStartTimeErrorMessage').text('');
	$('#imsiListByTimePeriodModalStartTime').css('border-style', 'hidden');
	$('#imsiListByTimePeriodModalEndDateErrorMessage').text('');
	$('#imsiListByTimePeriodModalEndDate').css('border-style', 'hidden');
	$('#imsiListByTimePeriodModalEndTimeErrorMessage').text('');
	$('#imsiListByTimePeriodModalEndTime').css('border-style', 'hidden');
	if (areFieldsBlank(task) == false) {
		if (isTimeCorrectFormat(startTime, endTime, task) == false) {
			invalidInput = true;
		}
	} else {
		invalidInput = true;
	}


	if (invalidInput == false) {
		getImsiListByTimePeriod(
			formatDate($('#imsiListByTimePeriodModalStartDate').val()),
			$('#imsiListByTimePeriodModalStartTime').val(),
			formatDate($('#imsiListByTimePeriodModalEndDate').val()),
			$('#imsiListByTimePeriodModalEndTime').val()
		);
	}
}

var getImsiListByTimePeriod = function(startDate, startTime, endDate, endTime) {
	$.ajax({
		type: 'GET',
		url: callFailureUrl + "callfailuredates/" + startDate + "/" + startTime + "/" + endDate + "/" + endTime,
		dataType: "json",
		success: renderImsiListByTimePeriodResults
	});
}

var renderImsiListByTimePeriodResults = function(imsiList) {
	$('#imsiListByTimePeriodResultsModalTable').DataTable().destroy();
	$('#imsiListByTimePeriodResultsModalTableBody').empty();
	$.each(imsiList, function(index, imsi) {
		$('#imsiListByTimePeriodResultsModalTableBody').append(
			'<tr>' +
			'<td>' + imsi[0] + '</td>' +
			'<td>' + imsi[1] + '</td>' +
			'<td>' + imsi[2] + '</td>' +
			'<td>' + imsi[3] + '</td>' +
			'</tr>'
		);
	});
	$('#imsiListByTimePeriodResultsModalTable').DataTable({
		lengthChange: false,
		pageLength: 8
	});
	openModal('#imsiListByTimePeriodResultsModal');
}

/* ************************* CALL FAILURE COUNT BY PHONE MODEL ************************* */

var callFailureCountByPhoneModelModalRunButtonClicked = function() {
	var invalidInput = false;
	var task = "callFailureCountByPhoneModel";
	var startTime = $('#callFailureCountByPhoneModelModalStartTime').val();
	var endTime = $('#callFailureCountByPhoneModelModalEndTime').val();
	$('#callFailureCountByPhoneModelModalPhoneModelInputErrorMessage').text('');
	$('#callFailureCountByPhoneModelModalStartDateErrorMessage').text('');
	$('#callFailureCountByPhoneModelModalStartTimeErrorMessage').text('');
	$('#callFailureCountByPhoneModelModalEndDateErrorMessage').text('');
	$('#callFailureCountByPhoneModelModalEndTimeErrorMessage').text('');
	$('#callFailureCountByPhoneModelModalPhoneModelInput').css('border-style', 'hidden');
	$('#callFailureCountByPhoneModelModalStartDate').css('border-style', 'hidden');
	$('#callFailureCountByPhoneModelModalStartTime').css('border-style', 'hidden');
	$('#callFailureCountByPhoneModelModalEndDate').css('border-style', 'hidden');
	$('#callFailureCountByPhoneModelModalEndTime').css('border-style', 'hidden');
	if (areFieldsBlank(task) == false) {
		if (isTimeCorrectFormat(startTime, endTime, task) == false) {
			invalidInput = true;
		}
	} else {
		invalidInput = true;
	}

	if (invalidInput == false) {
		getCallFailureCountByPhoneModel(
			formatDate($('#callFailureCountByPhoneModelModalStartDate').val()),
			$('#callFailureCountByPhoneModelModalStartTime').val(),
			formatDate($('#callFailureCountByPhoneModelModalEndDate').val()),
			$('#callFailureCountByPhoneModelModalEndTime').val(),
			$('#callFailureCountByPhoneModelModalPhoneModelInput').val()
		);
	}
}

var getCallFailureCountByPhoneModel = function(startDate, startTime, endDate, endTime, model) {
	var callFailureCountByPhoneModel = 0;
	$.ajax({
		type: 'GET',
		url: callFailureUrl + 'modelcount/' + startDate + "/" + startTime + "/" + endDate + "/" + endTime + "/" + model,
		dataType: "json",
		success: function(callFailures) {
			$.each(callFailures, function(index, callfailure) {
				callFailureCountByPhoneModel++;
			});
			if (callFailureCountByPhoneModel == 1) {
				$('#callFailureCountByPhoneModelResultModalResult').text('Between ' + startDate + ' at ' + startTime + ', and ' + endDate + ' at ' + endTime + ', there was ' + callFailureCountByPhoneModel + " Call Failure for the Phone Model " + model + ".");
			} else {
				$('#callFailureCountByPhoneModelResultModalResult').text('Between ' + startDate + ' at ' + startTime + ', and ' + endDate + ' at ' + endTime + ', there were ' + callFailureCountByPhoneModel + " Call Failures for the Phone Model " + model + ".");
			}
			$('#callFailureCountByPhoneModelResultModalResult').css('color', 'black');
			$('#callFailureCountByPhoneModelResultModalResult').css('font-size', '20px');
			$('#callFailureCountByPhoneModelResultModalResult').css('font-weight', 'bold');
			openModal('#callFailureCountByPhoneModelResultModal');
		}
	})
}

/* ************************* EVENT ID, CAUSE CODE ETC BY PHONE MODEL ************************* */

var PhoneModelSearchModalRunButtonClicked = function() {
	var invalidInput = false;
	var task = 'phoneModelSearch';
	$('#PhoneModelSearchModalPhoneModelInputErrorMessage').text('');
	$('#PhoneModelSearchModalPhoneModelInput').css('border-style', 'hidden');
	if (areFieldsBlank(task) == true) {
		invalidInput = true;
	}
	if (invalidInput == false) {
		getPhoneModelSearchResults($('#PhoneModelSearchModalPhoneModelInput').val());
	}
}

var getPhoneModelSearchResults = function(phoneModel) {
	$.ajax({
		type: 'GET',
		url: UeUrl + "phonemodel/" + phoneModel,
		dataType: "json",
		success: function(events) {
			renderPhoneModelSearchResults(events, phoneModel)
		}
	});
}

var renderPhoneModelSearchResults = function(events, phoneModel) {
	$('#PhoneModelSearchResultsModalTable').DataTable().destroy();
	$('#PhoneModelSearchResultsModalTableBody').empty();
	$.each(events, function(index, event) {
		$('#PhoneModelSearchResultsModalTableBody').append(
			'<tr>' +
			'<td>' + event[0] + '</td>' +
			'<td>' + event[1] + '</td>' +
			'<td>' + event[2] + '</td>' +
			'<td>' + event[3] + '</td>' +
			'<td>' + event[4] + '</td>' +
			'</tr>'
		);
	});
	$('#PhoneModelSearchResultsModalTable').DataTable({
		lengthChange: false,
		pageLength: 5
	});
	$('#PhoneModelSearchResultsModal h2').text('EVENT ID\'S, CAUSE CODES & NO. OF OCCURRENCES FOR PHONE MODEL ' + $('#PhoneModelSearchModalPhoneModelInput').val());
	openModal('#PhoneModelSearchResultsModal');
}

/* ************************* AFFECTED IMSI BY GIVEN FAILURE CLASS ************************* */

var imsiFailureClassModalRunButtonClicked = function() {
	var invalidInput = false;
	var task = 'imsiFailureClass';
	if (areFieldsBlank(task) == true) {
		invalidInput = true;
	}
	
	if (invalidInput == false) {
		getImsiFailureClassSearchResults($('#imsiFailureClassModalInput').val());	
	}
}

var getImsiFailureClassSearchResults = function(failureClassCode) {
	$.ajax({
		type: 'GET',
		url: callFailureUrl + "failureclass/"  + failureClassCode,
		dataType: "json",
		success: renderImsiFailureClassSearchResults
	});
}

var renderImsiFailureClassSearchResults = function(events) {
    $('#imsiFailureClassResultsModalTable').DataTable().destroy();
    $('#imsiFailureClassResultsModalTableBody').empty();
	$.each(events,function(index, event){
		$('#imsiFailureClassResultsModalTableBody').append(
			'<tr>' + 
				'<td>' + event + '</td>' +
			'</tr>'
		);
	});
	$('#imsiFailureClassResultsModalTable').DataTable({
		lengthChange: false,
		pageLength: 8
	});
	$('#imsiFailureClassResultsModal .dataTable-modal-box').css('width', '45%');
	$('#imsiFailureClassResultsModal h2').text('IMSI\'s affected by this Failure Class');
	openModal('#imsiFailureClassResultsModal');
}



/* ************************* CALL FAILURE PLUS DURATION BY IMSI ************************* */

var callFailureCountPlusDurationByImsiModalRunButtonClicked = function() {
	var invalidInput = false;
	var task = 'callFailureCountPlusDurationByImsi';
	var startTime = $('#callFailureCountPlusDurationByImsiModalStartTime').val();
	var endTime = $('#callFailureCountPlusDurationByImsiModalEndTime').val();
	$('#callFailureCountPlusDurationByImsiModalStartDateErrorMessage').text('');
	$('#callFailureCountPlusDurationByImsiModalStartTimeErrorMessage').text('');
	$('#callFailureCountPlusDurationByImsiModalEndDateErrorMessage').text('');
	$('#callFailureCountPlusDurationByImsiModalEndTimeErrorMessage').text('');
	$('#callFailureCountPlusDurationByImsiModalStartDate').css('border-style', 'hidden');
	$('#callFailureCountPlusDurationByImsiModalStartTime').css('border-style', 'hidden');
	$('#callFailureCountPlusDurationByImsiModalEndDate').css('border-style', 'hidden');
	$('#callFailureCountPlusDurationByImsiModalEndTime').css('border-style', 'hidden');

	if (areFieldsBlank(task) == true) {
		invalidInput = true;
	} else {
		if (isTimeCorrectFormat(startTime, endTime, task) == false) {
			invalidInput = true;
		}
	}
	if (invalidInput == false) {
		getCallFailureCountPlusDurationByImsi(
			formatDate($('#callFailureCountPlusDurationByImsiModalStartDate').val()),
			$('#callFailureCountPlusDurationByImsiModalStartTime').val(),
			formatDate($('#callFailureCountPlusDurationByImsiModalEndDate').val()),
			$('#callFailureCountPlusDurationByImsiModalEndTime').val()
		);
	}
}

var getCallFailureCountPlusDurationByImsi = function(startDate, startTime, endDate, endTime) {
	$.ajax({
		type: 'GET',
		url: failureClassUrl + "imsicallfailurescount/" + startDate + "/" + startTime + "/" + endDate + "/" + endTime,
		dataType: "json",
		success: renderCallFailureCountPlusDurationByImsi
	});
}

var renderCallFailureCountPlusDurationByImsi = function(results) {
	$('#callFailureCountPlusDurationByImsiResultsModalTable').DataTable().destroy();
	$('#callFailureCountPlusDurationByImsiResultsModalTableBody').empty();
	$.each(results, function(index, result) {
		$('#callFailureCountPlusDurationByImsiResultsModalTableBody').append(
			'<tr>' +
			'<td>' + result[0] + '</td>' +
			'<td>' + result[1] + '</td>' +
			'<td>' + result[2] + '</td>' +
			'<td>' + result[3] + '</td>' +
			'<td>' + formatTotalDuration(result[4]) + '</td>' +
			'</tr>'
		);
	});
	$('#callFailureCountPlusDurationByImsiResultsModalTable').DataTable({
		lengthChange: false,
		pageLength: 8
	});
	openModal('#callFailureCountPlusDurationByImsiResultsModal');

}

/* ************************* TOP 10 IMSIs that had Callfailures Within TimePeriod ************************* */

var top10ImsiCallfailuresWithinTimeperiodModalRunButtonClicked = function() {
	var invalidInput = false;
	var task = 'top10ImsiCallfailuresWithinTimeperiod';
	var startTime = $('#top10ImsiCallfailuresWithinTimeperiodModalStartTime').val();
	var endTime = $('#top10ImsiCallfailuresWithinTimeperiodModalEndTime').val();
	$('#top10ImsiCallfailuresWithinTimeperiodModalStartDateErrorMessage').text('');
	$('#top10ImsiCallfailuresWithinTimeperiodModalStartTimeErrorMessage').text('');
	$('#top10ImsiCallfailuresWithinTimeperiodModalEndDateErrorMessage').text('');
	$('#top10ImsiCallfailuresWithinTimeperiodModalEndTimeErrorMessage').text('');
	$('#top10ImsiCallfailuresWithinTimeperiodModalStartDate').css('border-style', 'hidden');
	$('#top10ImsiCallfailuresWithinTimeperiodModalStartTime').css('border-style', 'hidden');
	$('#top10ImsiCallfailuresWithinTimeperiodModalEndDate').css('border-style', 'hidden');
	$('#top10ImsiCallfailuresWithinTimeperiodModalEndTime').css('border-style', 'hidden');
	
	if (areFieldsBlank(task) == true) {
		invalidInput = true;
	} else {
		if (isTimeCorrectFormat(startTime, endTime, task) == false) {
			invalidInput = true;
		}
	}
	if (invalidInput == false) {
		gettop10ImsiCallfailuresWithinTimeperiod(
			formatDate($('#top10ImsiCallfailuresWithinTimeperiodModalStartDate').val()),
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartTime').val(),
			formatDate($('#top10ImsiCallfailuresWithinTimeperiodModalEndDate').val()),
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndTime').val()
		);
	}
}

var gettop10ImsiCallfailuresWithinTimeperiod = function(startDate, startTime, endDate, endTime) {
	$.ajax({
		type: 'GET',
		url: callFailureUrl + "top10imsicallfailures/" + startDate + "/" + startTime + "/" + endDate + "/" + endTime ,
		dataType: "json",
		success: rendertop10ImsiCallfailuresWithinTimeperiod
	});
}

var rendertop10ImsiCallfailuresWithinTimeperiod = function(top10Imsis) {
	
	$('#top10ImsiCallfailuresWithinTimeperiodResultsModalTable').DataTable().destroy();
	$('#top10ImsiCallfailuresWithinTimeperiodResultsModalTableBody').empty();		
	$.each(top10Imsis, function(index, value){
		$('#top10ImsiCallfailuresWithinTimeperiodResultsModalTableBody').append(
			'<tr>' + 
				'<td>' + value[0] + '</td>' +
				'<td>' + value[1] + '</td>' +
			'</tr>'
		);
	});
	$('#top10ImsiCallfailuresWithinTimeperiodResultsModal .dataTable-modal-box').css('width', '45%');
	$('#top10ImsiCallfailuresWithinTimeperiodResultsModal .dataTable-modal-box').css('margin-top', '0.5%');
	$('#top10ImsiCallfailuresWithinTimeperiodResultsModalTable').DataTable({
		lengthChange: false,
		pageLength: 10
	});
	openModal('#top10ImsiCallfailuresWithinTimeperiodResultsModal'); 
	
}


/* ************************* FORMAT DURATION ************************* */

var formatTotalDuration = function(numberOfSeconds) {
	var numberOfHours = Math.floor(numberOfSeconds / 3600);
	remainingSeconds = numberOfSeconds - (numberOfHours * 3600);
	var numberOfMins = Math.floor(remainingSeconds / 60);
	remainingSeconds -= numberOfMins * 60;
	var formattedTotalDuration = numberOfHours + "h : " + numberOfMins + "m : " + remainingSeconds + "s";
	return formattedTotalDuration
}

/* ************************* FORMAT DATE ************************* */

var formatDate = function(unformattedDate) {
	var day = unformattedDate.substring(8, 10);
	var month = unformattedDate.substring(5, 7);
	var year = unformattedDate.substring(0, 4);
	var formattedDate = day + "-" + month + "-" + year;
	return formattedDate;
}

/* ************************* DISPLAY UPDATES ************************* */

var updateDisplay = function(userType) {
	resetDisplay();
	if (userType == "Customer Service Representative") {
		$('#customerServiceLink').show();
		$('#customerServiceRepModule').show();
	} else if (userType == "Support Engineer") {
		$('#supportEngineerLink').show();
		$('#supportEngineerModule').show();
		$('#customerServiceLink').show();
		$('#customerServiceRepModule').show();
	} else if (userType == "Network Management Engineer") {
		$('#networkManagementLink').show();
		$('#networkManagementModule').show();
		$('#supportEngineerLink').show();
		$('#supportEngineerModule').show();
		$('#customerServiceLink').show();
		$('#customerServiceRepModule').show();
	} else if (userType == "Administrator") {
		$('#systemAdministratorLink').show();
		$('#systemAdministratorModule').show();
		$('#networkManagementLink').show();
		$('#networkManagementModule').show();
		$('#supportEngineerLink').show();
		$('#supportEngineerModule').show();
		$('#customerServiceLink').show();
		$('#customerServiceRepModule').show();
	} else if (userType == "") {
		$('#systemAdministratorLink').hide();
		$('#systemAdministratorModule').hide();
		$('#networkManagementLink').hide();
		$('#networkManagementModule').hide();
		$('#supportEngineerLink').hide();
		$('#supportEngineerModule').hide();
		$('#customerServiceLink').hide();
		$('#customerServiceRepModule').hide();
	}
}

var clearModalFields = function(modal) {

	if (modal == 'logIn') {

		$('#logInUsernameInput').val('');
		$('#logInUsernameInput').css('border-style', 'hidden');
		$('#logInUsernameInputErrorMessage').text('');

		$('#logInPasswordInput').val('');
		$('#logInPasswordInput').css('border-style', 'hidden');
		$('#logInPasswordInputErrorMessage').text('');

	} else if (modal == 'createUser') {

		$('#createUserUsernameInput').val('');
		$('#createUserUsernameInput').css('border-style', 'hidden');
		$('#createUserUsernameInputErrorMessage').text('');

		$('#createUserPasswordInput').val('');
		$('#createUserPasswordInput').css('border-style', 'hidden');
		$('#createUserPasswordInputErrorMessage').text('');

		$('#createUserConfirmPasswordInput').val('');
		$('#createUserConfirmPasswordInput').css('border-style', 'hidden');
		$('#createUserConfirmPasswordInputErrorMessage').text('');

		$('#createUserSelectErrorMessage').text('');

	} else if (modal == 'imsiSearch') {

		$('#imsiSearchModalImsiInput').val('');
		$('#imsiSearchModalImsiInput').css('border-style', 'hidden');
		$('#imsiSearchModalImsiInputErrorMessage').text('');
		$('#imsiSearchModalImsiInputwrapper').removeClass('active');


	} else if (modal == 'callFailureCountByImsi') {

		$('#callFailureCountByImsiModalImsiInput').val('');
		$('#callFailureCountByImsiModalImsiInput').css('border-style', 'hidden');
		$('#callFailureCountByImsiModalImsiInputErrorMessage').text('');
		$('#callFailureCountByImsiInputwrapper').removeClass('active');

		$('#callFailureCountByImsiModalStartDate').val('');
		$('#callFailureCountByImsiModalStartDate').css('border-style', 'hidden');
		$('#callFailureCountByImsiModalStartDateErrorMessage').text('');

		$('#callFailureCountByImsiModalStartTime').val('');
		$('#callFailureCountByImsiModalStartTime').css('border-style', 'hidden');
		$('#callFailureCountByImsiModalStartTimeErrorMessage').text('');

		$('#callFailureCountByImsiModalEndDate').val('');
		$('#callFailureCountByImsiModalEndDate').css('border-style', 'hidden');
		$('#callFailureCountByImsiModalEndDateErrorMessage').text('');

		$('#callFailureCountByImsiModalEndTime').val('');
		$('#callFailureCountByImsiModalEndTime').css('border-style', 'hidden');
		$('#callFailureCountByImsiModalEndTimeErrorMessage').text('');

	} else if (modal == 'imsiListByTimePeriod') {

		$('#imsiListByTimePeriodModalStartDateErrorMessage').text('');
		$('#imsiListByTimePeriodModalStartTimeErrorMessage').text('');
		$('#imsiListByTimePeriodModalEndDateErrorMessage').text('');
		$('#imsiListByTimePeriodModalEndTimeErrorMessage').text('');

		$('#imsiListByTimePeriodModalStartDate').val('');
		$('#imsiListByTimePeriodModalStartTime').val('');
		$('#imsiListByTimePeriodModalEndDate').val('');
		$('#imsiListByTimePeriodModalEndTime').val('');

		$('#imsiListByTimePeriodModalStartDate').css('border-style', 'hidden');
		$('#imsiListByTimePeriodModalStartTime').css('border-style', 'hidden');
		$('#imsiListByTimePeriodModalEndDate').css('border-style', 'hidden');
		$('#imsiListByTimePeriodModalEndTime').css('border-style', 'hidden');

	} else if (modal == 'callFailureCountByPhoneModel') {

		$('#callFailureCountByPhoneModelModalPhoneModelInputErrorMessage').text('');
		$('#callFailureCountByPhoneModelModalStartDateErrorMessage').text('');
		$('#callFailureCountByPhoneModelModalStartTimeErrorMessage').text('');
		$('#callFailureCountByPhoneModelModalEndDateErrorMessage').text('');
		$('#callFailureCountByPhoneModelModalEndTimeErrorMessage').text('');

		$('#callFailureCountByPhoneModelModalPhoneModelInput').val('');
		$('#callFailureCountByPhoneModelModalStartDate').val('');
		$('#callFailureCountByPhoneModelModalStartTime').val('');
		$('#callFailureCountByPhoneModelModalEndDate').val('');
		$('#callFailureCountByPhoneModelModalEndTime').val('');

		$('#callFailureCountByPhoneModelModalPhoneModelInput').val('');

		$('#callFailureCountByPhoneModelModalPhoneModelInput').css('border-style', 'hidden');
		$('#callFailureCountByPhoneModelModalStartDate').css('border-style', 'hidden');
		$('#callFailureCountByPhoneModelModalStartTime').css('border-style', 'hidden');
		$('#callFailureCountByPhoneModelModalEndDate').css('border-style', 'hidden');
		$('#callFailureCountByPhoneModelModalEndTime').css('border-style', 'hidden');

	} else if (modal == 'imsiSearchUnique') {

		$('#imsiSearchUniqueModalImsiInput').val('');
		$('#imsiSearchUniqueModalImsiInput').css('border-style', 'hidden');
		$('#imsiSearchUniqueModalImsiInputErrorMessage').text('');
		$('#imsiSearchUniqueModalImsiInputwrapper').removeClass('active');

	} else if (modal == 'phoneModelSearch') {

		$('#PhoneModelSearchModalPhoneModelInput').val('');
		$('#PhoneModelSearchModalPhoneModelInput').css('border-style', 'hidden');
		$('#PhoneModelSearchModalPhoneModelInputErrorMessage').text('');

	} else if (modal == 'callFailureCountPlusDurationByImsi') {

		$('#callFailureCountPlusDurationByImsiModalStartDateErrorMessage').text('');
		$('#callFailureCountPlusDurationByImsiModalEndDateErrorMessage').text('');
		$('#callFailureCountPlusDurationByImsiModalStartTimeErrorMessage').text('');
		$('#callFailureCountPlusDurationByImsiModalEndTimeErrorMessage').text('');

		$('#callFailureCountPlusDurationByImsiModalStartDate').val('');
		$('#callFailureCountPlusDurationByImsiModalEndDate').val('');
		$('#callFailureCountPlusDurationByImsiModalStartTime').val('');
		$('#callFailureCountPlusDurationByImsiModalEndTime').val('');

		$('#callFailureCountPlusDurationByImsiModalStartDate').css('border-style', 'hidden');
		$('#callFailureCountPlusDurationByImsiModalEndDate').css('border-style', 'hidden');
		$('#callFailureCountPlusDurationByImsiModalStartTime').css('border-style', 'hidden');
		$('#callFailureCountPlusDurationByImsiModalEndTime').css('border-style', 'hidden');

	} else if (modal == 'top10ImsiCallfailuresWithinTimeperiod') {
		
		$('#top10ImsiCallfailuresWithinTimeperiodModalStartDateErrorMessage').text('');
		$('#top10ImsiCallfailuresWithinTimeperiodModalEndDateErrorMessage').text('');
		$('#top10ImsiCallfailuresWithinTimeperiodModalStartTimeErrorMessage').text('');
		$('#top10ImsiCallfailuresWithinTimeperiodModalEndTimeErrorMessage').text('');
		
		$('#top10ImsiCallfailuresWithinTimeperiodModalStartDate').val('');
		$('#top10ImsiCallfailuresWithinTimeperiodModalEndDate').val('');
		$('#top10ImsiCallfailuresWithinTimeperiodModalStartTime').val('');
		$('#top10ImsiCallfailuresWithinTimeperiodModalEndTime').val('');
		
		$('#top10ImsiCallfailuresWithinTimeperiodModalStartDate').css('border-style', 'hidden');
		$('#top10ImsiCallfailuresWithinTimeperiodModalEndDate').css('border-style', 'hidden');
		$('#top10ImsiCallfailuresWithinTimeperiodModalStartTime').css('border-style', 'hidden');
		$('#top10ImsiCallfailuresWithinTimeperiodModalEndTime').css('border-style', 'hidden');
		
	} else if (modal == 'imsiFailureClass') {

		$('#imsiFailureClassModalInputErrorMessage').text('');
		
	} else if (modal == 'callFailureModelByOperatorId') {
		
		$('#callFailureModelOperatorIdByDateModalStartDate').val('');
		$('#callFailureModelOperatorIdByDateModalStartTime').val('');
		$('#callFailureModelOperatorIdByDateModalEndDate').val('');
		$('#callFailureModelOperatorIdByDateModalEndTime').val('');
		
		$('#callFailureModelOperatorIdByDateModalStartDate').css('border-style', 'hidden');
		$('#callFailureModelOperatorIdByDateModalStartTime').css('border-style', 'hidden');
		$('#callFailureModelOperatorIdByDateModalEndDate').css('border-style', 'hidden');
		$('#callFailureModelOperatorIdByDateModalEndTime').css('border-style', 'hidden');
		
		$('#callFailureModelOperatorIdByDateModalStartDateErrorMessage').text('');
		$('#callFailureModelOperatorIdByDateModalStartTimeErrorMessage').text('');
		$('#callFailureModelOperatorIdByDateModalEndDateErrorMessage').text('');
		$('#callFailureModelOperatorIdByDateModalEndTimeErrorMessage').text('');
		
	}
}

var createUserSuccess = function() {
	$('#createUserUsernameInput').css('border-style', 'solid');
	$('#createUserUsernameInput').css('border-color', 'green');
	$('#createUserUsernameInputErrorMessage').text('User created.');
	$('#createUserUsernameInputErrorMessage').css('color', 'green');

}

var createUserFailure = function() {
	$('#createUserUsernameInput').css('border-style', 'solid');
	$('#createUserUsernameInput').css('border-color', 'red');
	$('#createUserUsernameInputErrorMessage').text('User failed to be created.');
	$('#createUserUsernameInputErrorMessage').css('color', 'red');
}

var userFoundError = function() {
	$('#createUserUsernameInput').css('border-style', 'solid');
	$('#createUserUsernameInput').css('border-color', 'red');
	$('#createUserUsernameInputErrorMessage').text('User already exists.');
	$('#createUserUsernameInputErrorMessage').css('color', 'red');
}

var incorrectCredentialsError = function() {
	$('#logInUsernameInput').css('border-style', 'solid');
	$('#logInUsernameInput').css('border-color', 'red');
	$('#logInUsernameInputErrorMessage').text("Incorrect credentials.");
	$('#logInUsernameInputErrorMessage').css('color', 'red');
	$('#logInPasswordInput').css('border-style', 'solid');
	$('#logInPasswordInput').css('border-color', 'red');
	$('#logInPasswordInputErrorMessage').text("Incorrect credentials.");
	$('#logInPasswordInputErrorMessage').css('color', 'red');
}

var resetDisplay = function() {
	$('#customerServiceLink').hide();
	$('#supportEngineerLink').hide();
	$('#networkManagementLink').hide();
	$('#systemAdministratorLink').hide();
	$('#customerServiceRepModule').hide();
	$('#supportEngineerModule').hide();
	$('#networkManagementModule').hide();
	$('#systemAdministratorModule').hide();
	if (loggedIn == false) {
		$('#homeLogInButton').show();
		$('#homeLogOutButton').hide();
	} else {
		$('#homeLogInButton').hide();
		$('#homeLogOutButton').show();
	}
}

var openModal = function(modalId) {
	$(modalId).show();
}

var closeModal = function(modalId) {
	$(modalId).hide();
}

/* ************************* MODAL FIELD VALIDATION ************************* */

var areFieldsBlank = function(modal) {
	var isBlank = false;
	if (modal == 'logIn') {
		if ($('#logInUsernameInput').val() == "") {
			$('#logInUsernameInput').css('border-style', 'solid');
			$('#logInUsernameInput').css('border-color', 'red');
			$('#logInUsernameInputErrorMessage').text("Please enter a username.");
			$('#logInUsernameInputErrorMessage').css('color', 'red');
			isBlank = true
		}
		if ($('#logInPasswordInput').val() == "") {
			$('#logInPasswordInput').css('border-style', 'solid');
			$('#logInPasswordInput').css('border-color', 'red');
			$('#logInPasswordInputErrorMessage').text("Please enter a password.");
			$('#logInPasswordInputErrorMessage').css('color', 'red');
			isBlank = true;
		}
	} else if (modal == 'createUser') {
		if ($('#createUserUsernameInput').val() == "") {
			$('#createUserUsernameInput').css('border-style', 'solid');
			$('#createUserUsernameInput').css('border-color', 'red');
			$('#createUserUsernameInputErrorMessage').text("Please enter a username.");
			$('#createUserUsernameInputErrorMessage').css('color', 'red');
			isBlank = true;
		}
		if ($('#createUserPasswordInput').val() == "") {
			$('#createUserPasswordInput').css('border-style', 'solid');
			$('#createUserPasswordInput').css('border-color', 'red');
			$('#createUserPasswordInputErrorMessage').text("Please enter a password.");
			$('#createUserPasswordInputErrorMessage').css('color', 'red');
			isBlank = true;
		}
		if ($('#createUserConfirmPasswordInput').val() == "") {
			$('#createUserConfirmPasswordInput').css('border-style', 'solid');
			$('#createUserConfirmPasswordInput').css('border-color', 'red');
			$('#createUserConfirmPasswordInputErrorMessage').text("Please confirm your password.");
			$('#createUserConfirmPasswordInputErrorMessage').css('color', 'red');
			isBlank = true;
		}
		if ($('#createUserSelect').find(":selected").text() == "Select user type") {
			$('#createUserSelectErrorMessage').text("Please select a user type.");
			$('#createUserSelectErrorMessage').css('color', 'red');
			isBlank = true;
		}
	} else if (modal == "imsiSearch") {
		if ($('#imsiSearchModalImsiInput').val() == "") {
			$('#imsiSearchModalImsiInputErrorMessage').text('Please enter an IMSI.');
			$('#imsiSearchModalImsiInputErrorMessage').css('color', 'red');
			$('#imsiSearchModalImsiInput').css('border-style', 'solid');
			$('#imsiSearchModalImsiInput').css('border-color', 'red');
			isBlank = true;
		}
	} else if (modal == "callFailureCountByImsi") {
		if ($('#callFailureCountByImsiModalImsiInput').val() == "") {
			$('#callFailureCountByImsiModalImsiInputErrorMessage').text('Please enter an IMSI.');
			$('#callFailureCountByImsiModalImsiInputErrorMessage').css('color', 'red');
			$('#callFailureCountByImsiModalImsiInput').css('border-style', 'solid');
			$('#callFailureCountByImsiModalImsiInput').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureCountByImsiModalStartDate').val() == "") {
			$('#callFailureCountByImsiModalStartDateErrorMessage').text('Please enter a valid date.');
			$('#callFailureCountByImsiModalStartDateErrorMessage').css('color', 'red');
			$('#callFailureCountByImsiModalStartDate').css('border-style', 'solid');
			$('#callFailureCountByImsiModalStartDate').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureCountByImsiModalStartTime').val() == "") {
			$('#callFailureCountByImsiModalStartTimeErrorMessage').text('Please enter a time.');
			$('#callFailureCountByImsiModalStartTimeErrorMessage').css('color', 'red');
			$('#callFailureCountByImsiModalStartTime').css('border-style', 'solid');
			$('#callFailureCountByImsiModalStartTime').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureCountByImsiModalEndDate').val() == "") {
			$('#callFailureCountByImsiModalEndDateErrorMessage').text('Please enter a valid date.');
			$('#callFailureCountByImsiModalEndDateErrorMessage').css('color', 'red');
			$('#callFailureCountByImsiModalEndDate').css('border-style', 'solid');
			$('#callFailureCountByImsiModalEndDate').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureCountByImsiModalEndTime').val() == "") {
			$('#callFailureCountByImsiModalEndTimeErrorMessage').text('Please enter a time.');
			$('#callFailureCountByImsiModalEndTimeErrorMessage').css('color', 'red');
			$('#callFailureCountByImsiModalEndTime').css('border-style', 'solid');
			$('#callFailureCountByImsiModalEndTime').css('border-color', 'red');
			isBlank = true;
		}
	} else if (modal == 'imsiListByTimePeriod') {
		if ($('#imsiListByTimePeriodModalStartDate').val() == "") {
			$('#imsiListByTimePeriodModalStartDateErrorMessage').text('Please enter a valid date.')
			$('#imsiListByTimePeriodModalStartDateErrorMessage').css('color', 'red');
			$('#imsiListByTimePeriodModalStartDate').css('border-style', 'solid');
			$('#imsiListByTimePeriodModalStartDate').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#imsiListByTimePeriodModalStartTime').val() == "") {
			$('#imsiListByTimePeriodModalStartTimeErrorMessage').text('Please enter a time.')
			$('#imsiListByTimePeriodModalStartTimeErrorMessage').css('color', 'red');
			$('#imsiListByTimePeriodModalStartTime').css('border-style', 'solid');
			$('#imsiListByTimePeriodModalStartTime').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#imsiListByTimePeriodModalEndDate').val() == "") {
			$('#imsiListByTimePeriodModalEndDateErrorMessage').text('Please enter a valid date.')
			$('#imsiListByTimePeriodModalEndDateErrorMessage').css('color', 'red');
			$('#imsiListByTimePeriodModalEndDate').css('border-style', 'solid');
			$('#imsiListByTimePeriodModalEndDate').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#imsiListByTimePeriodModalEndTime').val() == "") {
			$('#imsiListByTimePeriodModalEndTimeErrorMessage').text('Please enter a time.')
			$('#imsiListByTimePeriodModalEndTimeErrorMessage').css('color', 'red');
			$('#imsiListByTimePeriodModalEndTime').css('border-style', 'solid');
			$('#imsiListByTimePeriodModalEndTime').css('border-color', 'red');
			isBlank = true;
		}
	} else if (modal == 'callFailureCountByPhoneModel') {
		if ($('#callFailureCountByPhoneModelModalPhoneModelInput').val() == "") {
			$('#callFailureCountByPhoneModelModalPhoneModelInputErrorMessage').text('Please enter a phone model.')
			$('#callFailureCountByPhoneModelModalPhoneModelInputErrorMessage').css('color', 'red');
			$('#callFailureCountByPhoneModelModalPhoneModelInput').css('border-style', 'solid');
			$('#callFailureCountByPhoneModelModalPhoneModelInput').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureCountByPhoneModelModalStartDate').val() == "") {
			$('#callFailureCountByPhoneModelModalStartDateErrorMessage').text('Please enter a valid date.');
			$('#callFailureCountByPhoneModelModalStartDateErrorMessage').css('color', 'red');
			$('#callFailureCountByPhoneModelModalStartDate').css('border-style', 'solid');
			$('#callFailureCountByPhoneModelModalStartDate').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureCountByPhoneModelModalStartTime').val() == "") {
			$('#callFailureCountByPhoneModelModalStartTimeErrorMessage').text('Please enter a time.');
			$('#callFailureCountByPhoneModelModalStartTimeErrorMessage').css('color', 'red');
			$('#callFailureCountByPhoneModelModalStartTime').css('border-style', 'solid');
			$('#callFailureCountByPhoneModelModalStartTime').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureCountByPhoneModelModalEndDate').val() == "") {
			$('#callFailureCountByPhoneModelModalEndDateErrorMessage').text('Please enter a valid date.');
			$('#callFailureCountByPhoneModelModalEndDateErrorMessage').css('color', 'red');
			$('#callFailureCountByPhoneModelModalEndDate').css('border-style', 'solid');
			$('#callFailureCountByPhoneModelModalEndDate').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureCountByPhoneModelModalEndTime').val() == "") {
			$('#callFailureCountByPhoneModelModalEndTimeErrorMessage').text('Please enter a time.');
			$('#callFailureCountByPhoneModelModalEndTimeErrorMessage').css('color', 'red');
			$('#callFailureCountByPhoneModelModalEndTime').css('border-style', 'solid');
			$('#callFailureCountByPhoneModelModalEndTime').css('border-color', 'red');
			isBlank = true;
		}
	} else if (modal == 'imsiSearchUnique') {
		if ($('#imsiSearchUniqueModalImsiInput').val() == "") {
			$('#imsiSearchUniqueModalImsiInputErrorMessage').text('Please enter an IMSI.');
			$('#imsiSearchUniqueModalImsiInputErrorMessage').css('color', 'red');
			$('#imsiSearchUniqueModalImsiInput').css('border-style', 'solid');
			$('#imsiSearchUniqueModalImsiInput').css('border-color', 'red');
			isBlank = true;
		}
	} else if (modal == 'phoneModelSearch') {
		if ($('#PhoneModelSearchModalPhoneModelInput').val() == "") {
			$('#PhoneModelSearchModalPhoneModelInputErrorMessage').text('Please enter a phone model');
			$('#PhoneModelSearchModalPhoneModelInputErrorMessage').css('color', 'red');
			$('#PhoneModelSearchModalPhoneModelInput').css('border-style', 'solid');
			$('#PhoneModelSearchModalPhoneModelInput').css('border-color', 'red');
			isBlank = true;
		}
	} else if (modal == 'callFailureModelOperatorIdByDate') {
		if ($('#callFailureModelOperatorIdByDateModalStartDate').val() == "") {
			$('#callFailureModelOperatorIdByDateModalStartDateErrorMessage').text('Please enter a valid date.');
			$('#callFailureModelOperatorIdByDateModalStartDateErrorMessage').css('color', 'red');
			$('#callFailureModelOperatorIdByDateModalStartDate').css('border-style', 'solid');
			$('#callFailureModelOperatorIdByDateModalStartDate').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureModelOperatorIdByDateModalStartTime').val() == "") {
			$('#callFailureModelOperatorIdByDateModalStartTimeErrorMessage').text('Please enter a time.');
			$('#callFailureModelOperatorIdByDateModalStartTimeErrorMessage').css('color', 'red');
			$('#callFailureModelOperatorIdByDateModalStartTime').css('border-style', 'solid');
			$('#callFailureModelOperatorIdByDateModalStartTime').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureModelOperatorIdByDateModalEndDate').val() == "") {
			$('#callFailureModelOperatorIdByDateModalEndDateErrorMessage').text('Please enter a valid date.');
			$('#callFailureModelOperatorIdByDateModalEndDateErrorMessage').css('color', 'red');
			$('#callFailureModelOperatorIdByDateModalEndDate').css('border-style', 'solid');
			$('#callFailureModelOperatorIdByDateModalEndDate').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureModelOperatorIdByDateModalEndTime').val() == "") {
			$('#callFailureModelOperatorIdByDateModalEndTimeErrorMessage').text('Please enter a time.');
			$('#callFailureModelOperatorIdByDateModalEndTimeErrorMessage').css('color', 'red');
			$('#callFailureModelOperatorIdByDateModalEndTime').css('border-style', 'solid');
			$('#callFailureModelOperatorIdByDateModalEndTime').css('border-color', 'red');
			isBlank = true;
		}
	} else if (modal == 'callFailureCountPlusDurationByImsi') {
		if ($('#callFailureCountPlusDurationByImsiModalStartDate').val() == "") {
			$('#callFailureCountPlusDurationByImsiModalStartDateErrorMessage').text('Please enter a valid date.');
			$('#callFailureCountPlusDurationByImsiModalStartDateErrorMessage').css('color', 'red');
			$('#callFailureCountPlusDurationByImsiModalStartDate').css('border-style', 'solid');
			$('#callFailureCountPlusDurationByImsiModalStartDate').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureCountPlusDurationByImsiModalStartTime').val() == "") {
			$('#callFailureCountPlusDurationByImsiModalStartTimeErrorMessage').text('Please enter a time.');
			$('#callFailureCountPlusDurationByImsiModalStartTimeErrorMessage').css('color', 'red');
			$('#callFailureCountPlusDurationByImsiModalStartTime').css('border-style', 'solid');
			$('#callFailureCountPlusDurationByImsiModalStartTime').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureCountPlusDurationByImsiModalEndDate').val() == "") {
			$('#callFailureCountPlusDurationByImsiModalEndDateErrorMessage').text('Please enter a valid date.');
			$('#callFailureCountPlusDurationByImsiModalEndDateErrorMessage').css('color', 'red');
			$('#callFailureCountPlusDurationByImsiModalEndDate').css('border-style', 'solid');
			$('#callFailureCountPlusDurationByImsiModalEndDate').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#callFailureCountPlusDurationByImsiModalEndTime').val() == "") {
			$('#callFailureCountPlusDurationByImsiModalEndTimeErrorMessage').text('Please enter a time.');
			$('#callFailureCountPlusDurationByImsiModalEndTimeErrorMessage').css('color', 'red');
			$('#callFailureCountPlusDurationByImsiModalEndTime').css('border-style', 'solid');
			$('#callFailureCountPlusDurationByImsiModalEndTime').css('border-color', 'red');
			isBlank = true;
		}
	} else if (modal == 'top10ImsiCallfailuresWithinTimeperiod') {
		if ($('#top10ImsiCallfailuresWithinTimeperiodModalStartDate').val() == "") {
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartDateErrorMessage').text('Please enter a valid date.');
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartDateErrorMessage').css('color', 'red');
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartDate').css('border-style', 'solid');
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartDate').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#top10ImsiCallfailuresWithinTimeperiodModalStartTime').val() == "") {
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartTimeErrorMessage').text('Please enter a time.');
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartTimeErrorMessage').css('color', 'red');
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartTime').css('border-style', 'solid');
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartTime').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#top10ImsiCallfailuresWithinTimeperiodModalEndDate').val() == "") {
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndDateErrorMessage').text('Please enter a valid date.');
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndDateErrorMessage').css('color', 'red');
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndDate').css('border-style', 'solid');
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndDate').css('border-color', 'red');
			isBlank = true;
		}
		if ($('#top10ImsiCallfailuresWithinTimeperiodModalEndTime').val() == "") {
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndTimeErrorMessage').text('Please enter a time.');
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndTimeErrorMessage').css('color', 'red');
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndTime').css('border-style', 'solid');
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndTime').css('border-color', 'red');
			isBlank = true;
		}
	} else if (modal == 'imsiFailureClass') {
		if ($('#imsiFailureClassModalInput').find(":selected").text() == "Select Failure Class") {
			$('#imsiFailureClassModalInputErrorMessage').text('Please enter a failure class.');
			$('#imsiFailureClassModalInputErrorMessage').css('color', 'red');
			isBlank = true;
		}
	}

	return isBlank;
}

var doPasswordsMatch = function() {
	var match = false;
	if ($('#createUserPasswordInput').val() == $('#createUserConfirmPasswordInput').val()) {
		match = true;
	} else {
		$('#createUserPasswordInput').css('border-style', 'solid');
		$('#createUserPasswordInput').css('border-color', 'red');
		$('#createUserPasswordInputErrorMessage').text("Passwords do not match");
		$('#createUserPasswordInputErrorMessage').css('color', 'red');
		$('#createUserConfirmPasswordInput').css('border-style', 'solid');
		$('#createUserConfirmPasswordInput').css('border-color', 'red');
		$('#createUserConfirmPasswordInputErrorMessage').text("Passwords do not match");
		$('#createUserConfirmPasswordInputErrorMessage').css('color', 'red');
	}
	return match;
}

var areFieldsCorrectLength = function() {
	var correctLength = true;
	if ($('#createUserPasswordInput').val().length < 6 || $('#createUserPasswordInput').val().length > 16) {
		$('#createUserPasswordInput').css('border-style', 'solid');
		$('#createUserPasswordInput').css('border-color', 'red');
		$('#createUserPasswordInputErrorMessage').text("Password must be between 6 and 16 characters.");
		$('#createUserPasswordInputErrorMessage').css('color', 'red');
		$('#createUserConfirmPasswordInput').css('border-style', 'solid');
		$('#createUserConfirmPasswordInput').css('border-color', 'red');
		$('#createUserConfirmPasswordInputErrorMessage').text("Passwords must be between 6 and 16 characters.");
		$('#createUserConfirmPasswordInputErrorMessage').css('color', 'red');
		correctLength = false;
	}
	if ($('#createUserUsernameInput').val().length < 10) {
		$('#createUserUsernameInput').css('border-style', 'solid');
		$('#createUserUsernameInput').css('border-color', 'red');
		$('#createUserUsernameInputErrorMessage').text('Username must be at least 10 characters.');
		$('#createUserUsernameInputErrorMessage').css('color', 'red');
		correctLength = false;
	}
	return correctLength;
}

var isPasswordTheCorrectFormat = function() {
	var format = /^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z])/;
	var correctFormat = false;
	if (!(format.test($('#createUserPasswordInput').val()))) {
		$('#createUserPasswordInput').css('border-style', 'solid');
		$('#createUserPasswordInput').css('border-color', 'red');
		$('#createUserPasswordInputErrorMessage').text("Password must contain a digit, upper & lowercase letter, and a special character.");
		$('#createUserPasswordInputErrorMessage').css('color', 'red');
		$('#createUserConfirmPasswordInput').css('border-style', 'solid');
		$('#createUserConfirmPasswordInput').css('border-color', 'red');
		$('#createUserConfirmPasswordInputErrorMessage').text("Password must contain a digit, upper & lowercase letter, and a special character.");
		$('#createUserConfirmPasswordInputErrorMessage').css('color', 'red');
	} else {
		correctFormat = true;
	}
	return correctFormat;
}

var doesImsiContainOnlyNumbers = function(imsi, modal) {
	var containsOnlyNumbers = true;
	if (/^[0-9]+$/.test(imsi) == false) {
		if (modal == "imsiSearch") {
			$('#imsiSearchModalImsiInput').css('border-style', 'solid');
			$('#imsiSearchModalImsiInput').css('border-color', 'red');
			$('#imsiSearchModalImsiInputErrorMessage').text("IMSI must only contain numbers.");
			$('#imsiSearchModalImsiInputErrorMessage').css('color', 'red');
		}
		if (modal == "callFailureCountByImsi") {
			$('#callFailureCountByImsiModalImsiInput').css('border-style', 'solid');
			$('#callFailureCountByImsiModalImsiInput').css('border-color', 'red');
			$('#callFailureCountByImsiModalImsiInputErrorMessage').text('IMSI must only contain numbers.');
			$('#callFailureCountByImsiModalImsiInputErrorMessage').css('color', 'red');
		}
		if (modal == "imsiSearchUnique") {
			$('#imsiSearchUniqueModalImsiInput').css('border-style', 'solid');
			$('#imsiSearchUniqueModalImsiInput').css('border-color', 'red');
			$('#imsiSearchUniqueModalImsiInputErrorMessage').text("IMSI must only contain numbers.");
			$('#imsiSearchUniqueModalImsiInputErrorMessage').css('color', 'red');
		}
		containsOnlyNumbers = false;
	}
	return containsOnlyNumbers;
}

var isImsiCorrectLength = function(imsi, modal) {
	var correctLength = true;
	if (imsi.length < 15 || imsi.length > 15) {
		if (modal == 'imsiSearch') {
			$('#imsiSearchModalImsiInput').css('border-style', 'solid');
			$('#imsiSearchModalImsiInput').css('border-color', 'red');
			$('#imsiSearchModalImsiInputErrorMessage').text("IMSI must be 15 digits long.");
			$('#imsiSearchModalImsiInputErrorMessage').css('color', 'red');
		} else if (modal == 'callFailureCountByImsi') {
			$('#callFailureCountByImsiModalImsiInput').css('border-style', 'solid');
			$('#callFailureCountByImsiModalImsiInput').css('border-color', 'red');
			$('#callFailureCountByImsiModalImsiInputErrorMessage').text('IMSI must be 15 digits long.');
			$('#callFailureCountByImsiModalImsiInputErrorMessage').css('color', 'red');
		} else if (modal == 'imsiSearchUnique') {
			$('#imsiSearchUniqueModalImsiInput').css('border-style', 'solid');
			$('#imsiSearchUniqueModalImsiInput').css('border-color', 'red');
			$('#imsiSearchUniqueModalImsiInputErrorMessage').text("IMSI must be 15 digits long.");
			$('#imsiSearchUniqueModalImsiInputErrorMessage').css('color', 'red');
		}
		correctLength = false;
	}
	return correctLength
}

var isTimeCorrectFormat = function(startTime, endTime, modal) {
	var correctFormat = true;
	if (!(/(2[0-3]|[01][0-9]):[0-5][0-9]/.test(startTime))) {
		if (modal == 'callFailureCountByImsi') {
			$('#callFailureCountByImsiModalStartTime').css('border-style', 'solid');
			$('#callFailureCountByImsiModalStartTime').css('border-color', 'red');
			$('#callFailureCountByImsiModalStartTimeErrorMessage').text('Time must be in 24hr \'hh:mm\' format');
			$('#callFailureCountByImsiModalStartTimeErrorMessage').css('color', 'red');
		} else if (modal == 'imsiListByTimePeriod') {
			$('#imsiListByTimePeriodModalStartTime').css('border-style', 'solid');
			$('#imsiListByTimePeriodModalStartTime').css('border-color', 'red');
			$('#imsiListByTimePeriodModalStartTimeErrorMessage').text('Time must be in 24hr \'hh:mm\' format');
			$('#imsiListByTimePeriodModalStartTimeErrorMessage').css('color', 'red');
		} else if (modal == 'callFailureCountByPhoneModel') {
			$('#callFailureCountByPhoneModelModalStartTime').css('border-style', 'solid');
			$('#callFailureCountByPhoneModelModalStartTime').css('border-color', 'red');
			$('#callFailureCountByPhoneModelModalStartTimeErrorMessage').text('Time must be in 24hr \'hh:mm\' format');
			$('#callFailureCountByPhoneModelModalStartTimeErrorMessage').css('color', 'red');
		} else if (modal == 'callFailureCountPlusDurationByImsi') {
			$('#callFailureCountPlusDurationByImsiModalStartTime').css('border-style', 'solid');
			$('#callFailureCountPlusDurationByImsiModalStartTime').css('border-color', 'red');
			$('#callFailureCountPlusDurationByImsiModalStartTimeErrorMessage').text('Time must be in 24hr \'hh:mm\' format');
			$('#callFailureCountPlusDurationByImsiModalStartTimeErrorMessage').css('color', 'red');
		} else if (modal == 'callFailureModelOperatorIdByDate') {
			$('#callFailureModelOperatorIdByDateModalStartTime').css('border-style', 'solid');
			$('#callFailureModelOperatorIdByDateModalStartTime').css('border-color', 'red');
			$('#callFailureModelOperatorIdByDateModalStartTimeErrorMessage').text('Time must be in 24hr \'hh:mm\' format');
			$('#callFailureModelOperatorIdByDateModalStartTimeErrorMessage').css('color', 'red');
		} else if (modal == 'top10ImsiCallfailuresWithinTimeperiod') {
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartTime').css('border-style', 'solid');
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartTime').css('border-color', 'red');
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartTimeErrorMessage').text('Time must be in 24hr \'hh:mm\' format');
			$('#top10ImsiCallfailuresWithinTimeperiodModalStartTimeErrorMessage').css('color', 'red');
		}
		correctFormat = false;
	}
	if (!(/(2[0-3]|[01][0-9]):[0-5][0-9]/.test(endTime))) {
		if (modal == 'callFailureCountByImsi') {
			$('#callFailureCountByImsiModalEndTime').css('border-style', 'solid');
			$('#callFailureCountByImsiModalEndTime').css('border-color', 'red');
			$('#callFailureCountByImsiModalEndTimeErrorMessage').text('Time must be in 24hr \'hh:mm\' format');
			$('#callFailureCountByImsiModalEndTimeErrorMessage').css('color', 'red');
		} else if (modal == 'imsiListByTimePeriod') {
			$('#imsiListByTimePeriodModalEndTime').css('border-style', 'solid');
			$('#imsiListByTimePeriodModalEndTime').css('border-color', 'red');
			$('#imsiListByTimePeriodModalEndTimeErrorMessage').text('Time must be in 24hr \'hh:mm\' format');
			$('#imsiListByTimePeriodModalEndTimeErrorMessage').css('color', 'red');
		} else if (modal == 'callFailureCountByPhoneModel') {
			$('#callFailureCountByPhoneModelModalEndTime').css('border-style', 'solid');
			$('#callFailureCountByPhoneModelModalEndTime').css('border-color', 'red');
			$('#callFailureCountByPhoneModelModalEndTimeErrorMessage').text('Time must be in 24hr \'hh:mm\' format');
			$('#callFailureCountByPhoneModelModalEndTimeErrorMessage').css('color', 'red');
		} else if (modal == 'callFailureCountPlusDurationByImsi') {
			$('#callFailureCountPlusDurationByImsiModalEndTime').css('border-style', 'solid');
			$('#callFailureCountPlusDurationByImsiModalEndTime').css('border-color', 'red');
			$('#callFailureCountPlusDurationByImsiModalEndTimeErrorMessage').text('Time must be in 24hr \'hh:mm\' format');
			$('#callFailureCountPlusDurationByImsiModalEndTimeErrorMessage').css('color', 'red');
		} else if (modal == 'callFailureModelOperatorIdByDate') {
			$('#callFailureModelOperatorIdByDateModalEndTime').css('border-style', 'solid');
			$('#callFailureModelOperatorIdByDateModalEndTime').css('border-color', 'red');
			$('#callFailureModelOperatorIdByDateModalEndTimeErrorMessage').text('Time must be in 24hr \'hh:mm\' format');
			$('#callFailureModelOperatorIdByDateModalEndTimeErrorMessage').css('color', 'red');
		} else if (modal == 'top10ImsiCallfailuresWithinTimeperiod') {
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndTime').css('border-style', 'solid');
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndTime').css('border-color', 'red');
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndTimeErrorMessage').text('Time must be in 24hr \'hh:mm\' format');
			$('#top10ImsiCallfailuresWithinTimeperiodModalEndTimeErrorMessage').css('color', 'red');
		}
		correctFormat = false;
	}
	return correctFormat;
}

/* ************************* PHONE MODEL GRAPH FUNCTIONS ************************* */

var phoneModelChart;
var phoneModelGraphData;
var canvas;
var ctv;
var phoneModel;
var causeCode;
var eventId;
var description;
var cellId;
var cellIdData;

function createPhoneModelGraph() {
	phoneModel = document.getElementById('PhoneModelSearchModalPhoneModelInput').value;
	document.getElementById('phoneModelGraphHeading').innerHTML = "Call Failures for: " + phoneModel;
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/RuntimeTerror/rest/ues/phonemodel/' + phoneModel,
		dataType: "json",
		success: generatePhoneModelGraphData
	});
}

function generatePhoneModelGraphData(list) {
	phoneModelGraphData = [];

	$.each(list, function(index, value) {
		phoneModelGraphData.push({ label: value[2] + "/" + value[3], value: value[4], description: value[5] })
	})

	phoneModelGraphData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = phoneModelGraphData.map(function(e) {
		return e.label;
	});

	var values = phoneModelGraphData.map(function(e) {
		return e.value;
	});

	var descriptions = phoneModelGraphData.map(function(e) {
		return e.description;
	});

	renderPhoneModelGraph(labels, values, descriptions);
}

function renderPhoneModelGraph(labels, values, descriptions) {

	canvas = document.getElementById('phoneModelChart');
	ctv = canvas.getContext('2d');
	canvas.onclick = causeCodeEventIdDrillDown;
	phoneModelChart = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'Cause Code/Event ID',
						font: {
							size: 20
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Number of Call Failures',
						font: {
							size: 20
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'Phone Model: ' + phoneModel;
						},
						afterTitle: function(context) {
							return '# Call Failures: ' + values[context[0].dataIndex] + '\n' +
								'Cause Code: ' + labels[context[0].dataIndex].split("/")[0] + '\n' +
								'Event ID: ' + labels[context[0].dataIndex].split("/")[1] + '\n' +
								'Description: ' + descriptions[context[0].dataIndex];
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});
	openModal('#phoneModelGraphResultsModal');
}

function causeCodeEventIdDrillDown(click) {
	const points = phoneModelChart.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
	if (points.length) {
		const firstPoint = points[0];
		const index = firstPoint.index;
		causeCode = phoneModelGraphData[index].label.split("/")[0];
		eventId = phoneModelGraphData[index].label.split("/")[1];
		phoneModel = document.getElementById('PhoneModelSearchModalPhoneModelInput').value;
		description = phoneModelGraphData[index].description;
		console.log(causeCode + " " + eventId + " " + phoneModel);
		drillDownChartForCells(phoneModel, causeCode, eventId);
	}
}

function drillDownChartForCells(phoneModel, causeCode, eventId) {
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/RuntimeTerror/rest/ues/phonemodel/' + phoneModel + '/' + causeCode + '/' + eventId,
		dataType: "json",
		success: generateCellIdDrillDown
	});
}

function generateCellIdDrillDown(list) {

	cellIdData = [];

	$.each(list, function(index, value) {
		cellIdData.push({ label: value[0], value: value[1] })
	})

	cellIdData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = cellIdData.map(function(e) {
		return e.label;
	});

	var values = cellIdData.map(function(e) {
		return e.value;
	});

	renderCellIdGraph(labels, values);

}

function renderCellIdGraph(labels, values) {

	phoneModelChart.destroy();
	canvas.onclick = cellIdDrillDown;
	phoneModelChart = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'Cell ID',
						font: {
							size: 20
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Number of Call Failures',
						font: {
							size: 20
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'Cell ID: ' + labels[context[0].dataIndex];
						},
						afterTitle: function(context) {
							return '# Call Failures: ' + values[context[0].dataIndex] + '\n' +
								'Phone Model: ' + phoneModel + '\n' +
								'Cause Code: ' + causeCode + '\n' +
								'Event ID: ' + eventId + '\n' +
								'Description: ' + description;
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});
}

function cellIdDrillDown(click) {
	const points = phoneModelChart.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
	if (points.length) {
		const firstPoint = points[0];
		const index = firstPoint.index;
		cellId = cellIdData[index].label;
		console.log(causeCode + " " + eventId + " " + phoneModel + " " + cellId)
		drillDownChartForImsis(phoneModel, causeCode, eventId, cellId)
	}
}

function drillDownChartForImsis(phoneModel1, causeCode1, eventId1, cellId) {
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/RuntimeTerror/rest/ues/phonemodel/' + phoneModel1 + '/' + causeCode1 + '/' + eventId1 + '/' + cellId,
		dataType: "json",
		success: generateImsiDrillDown
	});
}

function generateImsiDrillDown(list) {

	var imsiData = [];

	$.each(list, function(index, value) {
		imsiData.push({ label: value[0], value: value[1] })
	})

	imsiData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = imsiData.map(function(e) {
		return e.label;
	});

	var values = imsiData.map(function(e) {
		return e.value;
	});

	renderImsiGraph(labels, values);

}

function renderImsiGraph(labels, values) {

	phoneModelChart.destroy();
	canvas.onclick = "return false;"
	phoneModelChart = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'IMSI Value',
						font: {
							size: 20
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Total Call Failure Duration',
						font: {
							size: 20
						}
					},
					ticks: {
						callback: function(value, index, ticks) {
							return Math.round(value / 3600) + " hours";
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'IMSI: ' + labels[context[0].dataIndex];
						},
						afterTitle: function(context) {
							return 'Total Duration: ' + formatTotalDuration(values[context[0].dataIndex]) + '\n' +
								'Cell ID: ' + cellId + '\n' +
								'Phone Model: ' + phoneModel + '\n' +
								'Cause Code: ' + causeCode + '\n' +
								'Event ID: ' + eventId + '\n' +
								'Description: ' + description;
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});
}

/* ************************* DATE IMSI GRAPH FUNCTIONS ************************* */
var dateImsiGraph;
var imsiDateGraphData;
var fromDate;
var formattedFromDate;
var fromTime;
var toDate;
var formattedToDate;
var toTime;
var imsiDateCellIdData;

var failureClassDescription;
var eventCauseDescription;

function createImsiDateGraph() {
	fromDate = $('#callFailureCountPlusDurationByImsiModalStartDate').val();
	formattedFromDate = formatDate($('#callFailureCountPlusDurationByImsiModalStartDate').val());
	fromTime = $('#callFailureCountPlusDurationByImsiModalStartTime').val();
	toDate = $('#callFailureCountPlusDurationByImsiModalEndDate').val();
	formattedToDate = formatDate($('#callFailureCountPlusDurationByImsiModalEndDate').val());
	toTime = $('#callFailureCountPlusDurationByImsiModalEndTime').val();
	getCallFailureCountPlusDurationByImsiGraph(formattedFromDate, fromTime, formattedToDate, toTime);
}

var getCallFailureCountPlusDurationByImsiGraph = function(startDate, startTime, endDate, endTime) {
	$.ajax({
		type: 'GET',
		url: "http://localhost:8080/RuntimeTerror/rest/failureclass/imsicallfailuresgrouped/" + startDate + "/" + startTime + "/" + endDate + "/" + endTime,
		dataType: "json",
		success: generateDataImsiGraphData
	});
}


function generateDataImsiGraphData(list) {

	imsiDateGraphData = [];

	$.each(list, function(index, value) {
		imsiDateGraphData.push({ label: value[0], value: value[1] })
	})

	imsiDateGraphData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = imsiDateGraphData.map(function(e) {
		return e.label;
	});

	var values = imsiDateGraphData.map(function(e) {
		return e.value;
	});

	renderImsiDateGraph(labels, values);

}

function renderImsiDateGraph(labels, values) {

	canvas = document.getElementById('dataImsiModelChart');
	canvas.onclick = CellIdDrillDown;
	ctv = canvas.getContext('2d');
	dateImsiGraph = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'Cell ID',
						font: {
							size: 20
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Number of Call Failures',
						font: {
							size: 20
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'Cell ID: ' + labels[context[0].dataIndex];
						},
						afterTitle: function(context) {
							return 'Start Date: ' + fromDate + ' (' + fromTime + ')\n' + 
							'End Date: ' + toDate + ' (' + toTime + ')\n' + 
							'# Call Failures: ' + values[context[0].dataIndex];
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});
	document.getElementById('dateImsiGraphHeading').innerHTML = 'Call failures between ' +
		formatDate($('#callFailureCountPlusDurationByImsiModalStartDate').val()) + " " +
		$('#callFailureCountPlusDurationByImsiModalStartTime').val() + " and " +
		formatDate($('#callFailureCountPlusDurationByImsiModalEndDate').val()) + " " +
		$('#callFailureCountPlusDurationByImsiModalEndTime').val()

	openModal('#dateImsiGraphResultsModal');
}

function CellIdDrillDown(click) {
	const points = dateImsiGraph.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
	if (points.length) {
		const firstPoint = points[0];
		const index = firstPoint.index;
		cellId = imsiDateGraphData[index].label;
		drillDownImsiDateGraphToCellId(formattedFromDate, fromTime, formattedToDate, toTime, description);
	}
}

function drillDownImsiDateGraphToCellId(fromDate, fromTime, toDate, toTime, description) {
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/RuntimeTerror/rest/failureclass/imsicallfailuresgrouped/' +
			fromDate + '/' + fromTime + '/' + toDate + '/' + toTime + '/' + cellId,
		dataType: "json",
		success: generateImsiDateCellIdData
	});
}

function generateImsiDateCellIdData(list) {

	imsiDateCellIdData = [];

	$.each(list, function(index, value) {
		imsiDateCellIdData.push({ label: value[0], value: value[1] })
	})

	imsiDateCellIdData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = imsiDateCellIdData.map(function(e) {
		return e.label;
	});

	var values = imsiDateCellIdData.map(function(e) {
		return e.value;
	});

	renderImsiDateCellIdGraph(labels, values);

}

function renderImsiDateCellIdGraph(labels, values) {
	dateImsiGraph.destroy();
	canvas.onclick = failureClassDrillDown;
	dateImsiGraph = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'Failure Class',
						font: {
							size: 20
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Number of Call Failures',
						font: {
							size: 20
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'Failure Class: ' + labels[context[0].dataIndex];
						},
						afterTitle: function(context) {
							return 'Cell ID: ' + cellId + '\n' +
							       'Start Date: ' + fromDate + ' (' + fromTime + ')\n' + 
							       'End Date: ' + toDate + ' (' + toTime + ')\n' + 
							       '# Call Failures: ' + values[context[0].dataIndex];
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});

}

function failureClassDrillDown(click) {
	const points = dateImsiGraph.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
	if (points.length) {
		const firstPoint = points[0];
		const index = firstPoint.index;
		failureClassDescription = imsiDateCellIdData[index].label;
		drillDownImsiDateGraphToFailureClass(formattedFromDate, fromTime, formattedToDate, toTime, description, cellId);
	}
}

function drillDownImsiDateGraphToFailureClass(formattedFromDate, fromTime, formattedToDate, toTime, description, cellId){
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/RuntimeTerror/rest/failureclass/imsicallfailuresgrouped/' +
			formattedFromDate + '/' + fromTime + '/' + formattedToDate + '/' + toTime + '/' + cellId + '/' + failureClassDescription,
		dataType: "json",
		success: generateImsiDateDrilldownDataFailureClass
	});
}

var descriptions;

function generateImsiDateDrilldownDataFailureClass(list){
	
	imsiDateCellIdData = [];
	descriptions = [];

	$.each(list, function(index, value) {
		imsiDateCellIdData.push({ label: value[0] + "/" + value[1], value: value[2], description: value[3] })
	})

	imsiDateCellIdData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = imsiDateCellIdData.map(function(e) {
		return e.label;
	});

	var values = imsiDateCellIdData.map(function(e) {
		return e.value;
	});
	
	descriptions = imsiDateCellIdData.map(function(e) {
		return e.description;
	});

	renderFailureClassDateImsiGraph(labels, values);
	
}

function renderFailureClassDateImsiGraph(labels, values){
	
	dateImsiGraph.destroy();
	canvas.onclick = eventCauseDrillDown;
	dateImsiGraph = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'Cause Code/Event ID',
						font: {
							size: 20
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Number of Call Failures',
						font: {
							size: 20
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'Cause Code: ' + imsiDateCellIdData[context[0].dataIndex].label.split("/")[0] + '\n' +
							       'Event ID: ' + imsiDateCellIdData[context[0].dataIndex].label.split("/")[1] + '\n' +
                                   'Description: ' + descriptions[context[0].dataIndex]
						},
						afterTitle: function(context) {
							return 'Failure Class: ' + failureClassDescription + '\n' +
							       'Cell ID: ' + cellId + '\n' +
							       'Start Date: ' + fromDate + ' (' + fromTime + ')\n' + 
							       'End Date: ' + toDate + ' (' + toTime + ')\n' + 
							       '# Call Failures: ' + values[context[0].dataIndex];
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});
	
}

function eventCauseDrillDown(click) {
	const points = dateImsiGraph.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
	if (points.length) {
		const firstPoint = points[0];
		const index = firstPoint.index;
		eventCauseDescription = descriptions[index];
		drillDownImsiDateGraphToEventCause();
	}
}

function drillDownImsiDateGraphToEventCause(){
	$.ajax({
		type: 'GET',
		url: 'http://localhost:8080/RuntimeTerror/rest/failureclass/imsicallfailuresgrouped/' +
			formattedFromDate + '/' + fromTime + '/' + formattedToDate + '/' + toTime + '/' + cellId + '/' +
			failureClassDescription + "/" + eventCauseDescription,
		dataType: "json",
		success: generateImsiDateDrilldownDataEventCause
	});
	console.log('http://localhost:8080/RuntimeTerror/rest/failureclass/imsicallfailuresgrouped/' +
			formattedFromDate + '/' + fromTime + '/' + formattedToDate + '/' + toTime + '/' + cellId + '/' +
			failureClassDescription + "/" + eventCauseDescription)
}

function generateImsiDateDrilldownDataEventCause(list){
	
	var imsiDateCellIdData = [];

	$.each(list, function(index, value) {
		imsiDateCellIdData.push({ label: value[0], value: value[2] })
	})

	imsiDateCellIdData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = imsiDateCellIdData.map(function(e) {
		return e.label;
	});

	var values = imsiDateCellIdData.map(function(e) {
		return e.value;
	});

	renderEventCauseDateImsiGraph(labels, values);
	
}

function renderEventCauseDateImsiGraph(labels, values){
	
	dateImsiGraph.destroy();
	canvas.onclick = "return false;";
	dateImsiGraph = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'IMSI',
						font: {
							size: 20
						},
						ticks: {
							font: {
								size: 10
							}
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Call Failure Duration',
						font: {
							size: 20
						}
					},
					ticks: {
						callback: function(value, index, ticks) {
							if(Math.round(value)/3600<1){
								return Math.round(value / 60) + " minutes";
							} else{
								return Math.round(value / 3600) + " hours";
							}
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'IMSI: ' + labels[context[0].dataIndex];
						},
						afterTitle: function(context) {
							return 'Description: ' + eventCauseDescription + '\n' +
							       'Failure Class: ' + failureClassDescription + '\n' +
							       'Cell ID: ' + cellId + '\n' +
							       'Start Date: ' + fromDate + ' (' + fromTime + ')\n' + 
							       'End Date: ' + toDate + ' (' + toTime + ')\n' + 
							       'Total Duration: ' + formatTotalDuration(values[context[0].dataIndex]);
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});
	
}

/* ************************* TOP 10 CALL FAILURES GRAPH FUNCTIONS ************************* */
var top10CallFailureGraph;
var top10CallFailureData;
var startDate;
var startTime;
var endDate;
var endTime;
var market;
var operator;
var top10CallFailureDrillDownData;

function createTop10CallFailureGraph(){
	startDate = formatDate($('#callFailureModelOperatorIdByDateModalStartDate').val());
	startTime = $('#callFailureModelOperatorIdByDateModalStartTime').val();
	endDate = formatDate($('#callFailureModelOperatorIdByDateModalEndDate').val());
	endTime = $('#callFailureModelOperatorIdByDateModalEndTime').val();
	
	$.ajax({
		type: 'GET',
		url: "http://localhost:8080/RuntimeTerror/rest/callfailures/Top10Model/" + startDate + "/" + startTime + "/" + endDate + "/" + endTime,
		dataType: "json",
		success: renderCallFailureModelOperatorGraph
	});
	
	document.getElementById('top10CallFailuresGraphHeading').innerHTML = 'Top 10 Call Failures Between ' + startDate + " and " + endDate;
}

function renderCallFailureModelOperatorGraph(list){
	
	top10CallFailureData = [];

	$.each(list, function(index, value) {
		top10CallFailureData.push({ label: value[1] + "/" + value[2] + "/" + value[3], value: value[0] })
	})

	top10CallFailureData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = top10CallFailureData.map(function(e) {
		return e.label;
	});

	var values = top10CallFailureData.map(function(e) {
		return e.value;
	});

	renderTop10CallFailureGraph(labels, values);
}

function renderTop10CallFailureGraph(labels, values){
	canvas = document.getElementById('top10CallFailuresModelChart');
	canvas.onclick = top10CallFailureDrillDown;
	ctv = canvas.getContext('2d');
	var top10 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
	top10CallFailureGraph = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'Market/Operator/Cell ID',
						font: {
							size: 20
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Number of Call Failures',
						font: {
							size: 20
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'Market: ' + labels[context[0].dataIndex].split("/")[0] + '\n' +
							       'Operator: ' + labels[context[0].dataIndex].split("/")[1] + '\n' +
                                   'Cell ID: ' + labels[context[0].dataIndex].split("/")[2]
						},
						afterTitle: function(context) {
							return '# Call Failures: ' + values[context[0].dataIndex] + '\n' + 
							       'Position in top 10: ' + top10[context[0].dataIndex];
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});
	
	openModal('#top10CallFailuresGraphResultsModal');
	
}

function top10CallFailureDrillDown(click) {
	const points = top10CallFailureGraph.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
	if (points.length) {
		const firstPoint = points[0];
		const index = firstPoint.index;
		market = top10CallFailureData[index].label.split("/")[0];
		operator = top10CallFailureData[index].label.split("/")[1];
		cellId = top10CallFailureData[index].label.split("/")[2];
		drillDownTop10CallFailureGraph()
	}
}

function drillDownTop10CallFailureGraph(){
	$.ajax({
		type: 'GET',
		url: "http://localhost:8080/RuntimeTerror/rest/callfailures/Top10Model/" + 
		startDate + "/" + startTime + "/" + endDate + "/" + endTime 
		+ "/" + market + "/" + operator + "/" + cellId,
		dataType: "json",
		success: renderTop10CallFailureDrillDown
	});
}

function renderTop10CallFailureDrillDown(list){
	
	top10CallFailureDrillDownData = [];

	$.each(list, function(index, value) {
		top10CallFailureDrillDownData.push({ label: value[0], value: value[1] })
	})

	top10CallFailureDrillDownData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = top10CallFailureDrillDownData.map(function(e) {
		return e.label;
	});

	var values = top10CallFailureDrillDownData.map(function(e) {
		return e.value;
	});

	generateTop10CallFailureDrillDown(labels, values);
}

function generateTop10CallFailureDrillDown(labels, values){
	top10CallFailureGraph.destroy();
	canvas.onclick = top10CallFailureSecondDrillDown;
	top10CallFailureGraph = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'Failure Class',
						font: {
							size: 20
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Number of Call Failures',
						font: {
							size: 20
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'Market: ' + market + '\n' +
							       'Operator: ' + operator + '\n' +
                                   'Cell ID: ' + cellId + '\n' +
                                   'Failure Class: ' + labels[context[0].dataIndex]
						},
						afterTitle: function(context) {
							return '# Call Failures: ' + values[context[0].dataIndex];
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});
}

function top10CallFailureSecondDrillDown(click) {
	const points = top10CallFailureGraph.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
	if (points.length) {
		const firstPoint = points[0];
		const index = firstPoint.index;
		failureClass = top10CallFailureDrillDownData[index].label;
		secondDrillDownTop10CallFailureGraph()
	}
}

function secondDrillDownTop10CallFailureGraph(){
	$.ajax({
		type: 'GET',
		url: "http://localhost:8080/RuntimeTerror/rest/callfailures/Top10Model/" + 
		startDate + "/" + startTime + "/" + endDate + "/" + endTime 
		+ "/" + market + "/" + operator + "/" + cellId + "/" + failureClass,
		dataType: "json",
		success: renderTop10CallFailureSecondDrillDown
	});
}

function renderTop10CallFailureSecondDrillDown(list){
	top10CallFailureDrillDownData = [];
	descriptions = []

	$.each(list, function(index, value) {
		top10CallFailureDrillDownData.push({ label: value[0] + "/" + value[1], value: value[2], description: value[3]})
	})

	top10CallFailureDrillDownData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = top10CallFailureDrillDownData.map(function(e) {
		return e.label;
	});

	var values = top10CallFailureDrillDownData.map(function(e) {
		return e.value;
	});
	
	descriptions = top10CallFailureDrillDownData.map(function(e) {
		return e.description;
	});

	generateTop10CallFailureSecondDrillDown(labels, values);
}

function generateTop10CallFailureSecondDrillDown(labels, values){
	top10CallFailureGraph.destroy();
	canvas.onclick = "return false;";
	top10CallFailureGraph = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'Cause Code/Event ID',
						font: {
							size: 20
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Number of Call Failures',
						font: {
							size: 20
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'Cause Code: ' + labels[context[0].dataIndex].split("/")[0] + '\n' +
							       'Event ID: ' + labels[context[0].dataIndex].split("/")[1] + '\n' +
                                   'Description: ' + descriptions[context[0].dataIndex]
						},
						afterTitle: function(context) {
							return 'Market: ' + market + '\n' +
							       'Operator: ' + operator + '\n' +
                                   'Cell ID: ' + cellId + '\n' +
                                   'FailureClass: ' + failureClass + '\n' +
							       '# Call Failures: ' + values[context[0].dataIndex];
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});
}



/* ************************* TOP 10 IMSI GRAPH FUNCTIONS ************************* */
var top10ImsiGraph;
var top10ImsiDateData;
var imsi;

function createTop10ImsiGraph(){
	
	startDate = formatDate($('#top10ImsiCallfailuresWithinTimeperiodModalStartDate').val());
	startTime = $('#top10ImsiCallfailuresWithinTimeperiodModalStartTime').val();
	endDate = formatDate($('#top10ImsiCallfailuresWithinTimeperiodModalEndDate').val());
	endTime = $('#top10ImsiCallfailuresWithinTimeperiodModalEndTime').val();
	
	$.ajax({
		type: 'GET',
		url: callFailureUrl + "top10imsicallfailures/" + startDate + "/" + startTime + "/" + endDate + "/" + endTime ,
		dataType: "json",
		success: rendertop10ImsiCallfailuresWithinTimeperiodGraph
	});
	
	document.getElementById('top10ImsiCallFailuresGraphHeading').innerHTML = 'Top 10 IMSIs between ' + startDate + " and " + endDate;
}

function rendertop10ImsiCallfailuresWithinTimeperiodGraph(list){
	
	top10ImsiDateData = [];

	$.each(list, function(index, value) {
		top10ImsiDateData.push({ label: value[0], value: value[1] })
	})

	top10ImsiDateData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = top10ImsiDateData.map(function(e) {
		return e.label;
	});

	var values = top10ImsiDateData.map(function(e) {
		return e.value;
	});

	renderTop10ImsiGraph(labels, values);
}

function renderTop10ImsiGraph(labels, values){
	
	canvas = document.getElementById('top10ImsiCallFailuresModelChart');
	ctv = canvas.getContext('2d');
	canvas.onclick = top10ImsiDrillDown;
	top10ImsiGraph = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'IMSI',
						font: {
							size: 20
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Number of Call Failures',
						font: {
							size: 20
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'IMSI: ' + labels[context[0].dataIndex];
						},
						afterTitle: function(context) {
							return '# Call Failures: ' + values[context[0].dataIndex];
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});
	
	openModal('#top10ImsiCallfailuresWithinTimeperiodGraphResultsModal');
}

function top10ImsiDrillDown(click) {
	const points = top10ImsiGraph.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
	if (points.length) {
		const firstPoint = points[0];
		const index = firstPoint.index;
		imsi = top10ImsiDateData[index].label;
		drillDownTop10ImsiGraph();
	}
}

function drillDownTop10ImsiGraph(){
	$.ajax({
		type: 'GET',
		url: callFailureUrl + "top10imsicallfailures/" + startDate + "/" + startTime + "/" + endDate + "/" + endTime + "/" + imsi ,
		dataType: "json",
		success: renderTop10ImsiDrillDown
	});
}

function renderTop10ImsiDrillDown(list){
	top10ImsiDateData = [];

	$.each(list, function(index, value) {
		top10ImsiDateData.push({ label: value[0], value: value[1] })
	})

	top10ImsiDateData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = top10ImsiDateData.map(function(e) {
		return e.label;
	});

	var values = top10ImsiDateData.map(function(e) {
		return e.value;
	});

	generateTop10ImsiDrillDown(labels, values);
}

function generateTop10ImsiDrillDown(labels, values){
	top10ImsiGraph.destroy();
	canvas.onclick = top10ImsiSecondDrillDown;
	top10ImsiGraph = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'Cell ID',
						font: {
							size: 20
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Number of Call Failures',
						font: {
							size: 20
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'IMSI: ' + imsi;
						},
						afterTitle: function(context) {
							return 'Cell ID: ' + labels[context[0].dataIndex] + '\n' +
							       '# Call Failures: ' + values[context[0].dataIndex];
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});
}

function top10ImsiSecondDrillDown(click) {
	const points = top10ImsiGraph.getElementsAtEventForMode(click, 'nearest', { intersect: true }, true);
	if (points.length) {
		const firstPoint = points[0];
		const index = firstPoint.index;
		cellId = top10ImsiDateData[index].label;
		secondDrillDownTop10ImsiGraph();
	}
}

function secondDrillDownTop10ImsiGraph(){
	$.ajax({
		type: 'GET',
		url: callFailureUrl + "top10imsicallfailures/" + startDate + "/" + startTime + "/" + endDate + "/" + endTime + "/" + imsi + "/" + cellId ,
		dataType: "json",
		success: renderTop10ImsiSecondDrillDown
	});
}

function renderTop10ImsiSecondDrillDown(list){
	top10ImsiDateData = [];
	descriptions = [];

	$.each(list, function(index, value) {
		top10ImsiDateData.push({ label: value[0] + "/" + value[1], value: value[2],  description: value[3]})
	})

	top10ImsiDateData.sort(function(a, b) {
		return b.value - a.value
	})

	var labels = top10ImsiDateData.map(function(e) {
		return e.label;
	});

	var values = top10ImsiDateData.map(function(e) {
		return e.value;
	});
	
	descriptions = top10ImsiDateData.map(function(e) {
		return e.description;
	});

	generateTop10ImsiSecondDrillDown(labels, values);
}

function generateTop10ImsiSecondDrillDown(labels, values){
	top10ImsiGraph.destroy();
	canvas.onclick = "return false;"
	top10ImsiGraph = new Chart(ctv, {
		type: 'bar',
		data: {
			labels: labels,
			datasets: [{
				data: values,
				backgroundColor: ['navy'],
				borderColor: ['black'],
				borderWidth: 1
			}]
		},
		options: {
			indexAxis: 'y',
			scales: {
				yAxes: {
					title: {
						display: true,
						text: 'Cause Code/Event ID',
						font: {
							size: 20
						}
					}
				},
				xAxes: {
					title: {
						display: true,
						text: 'Number of Call Failures',
						font: {
							size: 20
						}
					}
				}
			},
			plugins: {
				legend: {
					display: false
				},
				tooltip: {
					callbacks: {
						title: function(context) {
							return 'IMSI: ' + imsi;
						},
						afterTitle: function(context) {
							return 'Cell ID: ' + cellId + '\n' +
							       'Cause Code: ' + labels[context[0].dataIndex].split("/")[0] + '\n' +
							       'Event ID: ' + labels[context[0].dataIndex].split("/")[1] + '\n' +
                                   'Description: ' + descriptions[context[0].dataIndex].split("/")[0] + '\n' +
							       '# Call Failures: ' + values[context[0].dataIndex];
						},
						label: function(context) {
							return '';
						}
					},
					displayColors: false
				}
			}
		}
	});
}


$('#imsiSearchModalImsiInput').keyup(function (event) { 
    let imsiInput = event.target.value;
    let imsiSuggestionArray = [];
    if (imsiInput) {
        imsiSuggestionArray = imsiSuggestions.filter((input) =>{
            return input.toString().startsWith(imsiInput.toString());
        });
        imsiSuggestionArray = imsiSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#imsiSearchModalImsiInputwrapper').addClass("active");
        showSuggestions(imsiSuggestionArray, '#imsiSearchModalImsiInput', '#imsiSearchModalImsiInputwrapper', '#imsiSearchModalImsiInputAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#imsiSearchModalImsiInput', '#imsiSearchModalImsiInputwrapper');
        });
    } else {
        $('#imsiSearchModalImsiInputwrapper').removeClass("active");
    }
});

$('#callFailureCountByImsiModalImsiInput').keyup(function (event) { 
    let imsiInput = event.target.value;
    let imsiSuggestionArray = [];
    if (imsiInput) {
        imsiSuggestionArray = imsiSuggestions.filter((input) =>{
            return input.toString().startsWith(imsiInput.toString());
        });
        imsiSuggestionArray = imsiSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#callFailureCountByImsiModalImsiInputwrapper').addClass("active");
        showSuggestions(imsiSuggestionArray, '#callFailureCountByImsiModalImsiInput', '#callFailureCountByImsiModalImsiInputwrapper', '#callFailureCountByImsiModalImsiInputAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#callFailureCountByImsiModalImsiInput', '#callFailureCountByImsiModalImsiInputwrapper');
        });
    } else {
        $('#callFailureCountByImsiModalImsiInputwrapper').removeClass("active");
    }
});

$('#callFailureCountByImsiModalStartTime').keyup(function (event) { 
    let timeInput = event.target.value;
    let timeSuggestionArray = [];
    if (timeInput) {
        timeSuggestionArray = timeSuggestions.filter((input) =>{
            return input.toString().startsWith(timeInput.toString());
        });
        timeSuggestionArray = timeSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#callFailureCountByImsiModalStartTimewrapper').addClass("active");
        showSuggestions(timeSuggestionArray, '#callFailureCountByImsiModalStartTime', '#callFailureCountByImsiModalStartTimewrapper', '#callFailureCountByImsiModalStartTimeAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#callFailureCountByImsiModalStartTime', '#callFailureCountByImsiModalStartTimewrapper');
        });
    } else {
        $('#callFailureCountByImsiModalStartTimewrapper').removeClass("active");
    }
});

$('#callFailureCountByImsiModalEndTime').keyup(function (event) { 
    let timeInput = event.target.value;
    let timeSuggestionArray = [];
    if (timeInput) {
        timeSuggestionArray = timeSuggestions.filter((input) =>{
            return input.toString().startsWith(timeInput.toString());
        });
        timeSuggestionArray = timeSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#callFailureCountByImsiModalEndTimewrapper').addClass("active");
        showSuggestions(timeSuggestionArray, '#callFailureCountByImsiModalEndTime', '#callFailureCountByImsiModalEndTimewrapper', '#callFailureCountByImsiModalEndTimeAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#callFailureCountByImsiModalEndTime', '#callFailureCountByImsiModalEndTimewrapper');
        });
    } else {
        $('#callFailureCountByImsiModalEndTimewrapper').removeClass("active");
    }
});

$('#callFailureCountByPhoneModelModalPhoneModelInput').keyup(function (event) { 
    let modelInput = event.target.value;
    let modelSuggestionArray = [];
    if (modelInput) {
        modelSuggestionArray = phoneModelSuggestions.filter((input) =>{
            return input.toString().startsWith(modelInput.toString());
        });
        modelSuggestionArray = modelSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#callFailureCountByPhoneModelModalPhoneModelInputwrapper').addClass("active");
        showSuggestions(modelSuggestionArray, '#callFailureCountByPhoneModelModalPhoneModelInput', '#callFailureCountByPhoneModelModalPhoneModelInputwrapper', '#callFailureCountByPhoneModelModalPhoneModelAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#callFailureCountByPhoneModelModalPhoneModelInput', '#callFailureCountByPhoneModelModalPhoneModelInputwrapper');
        });
    } else {
        $('#callFailureCountByPhoneModelModalPhoneModelInputwrapper').removeClass("active");
    }
});

$('#callFailureCountByPhoneModelModalStartTime').keyup(function (event) { 
    let timeInput = event.target.value;
    let timeSuggestionArray = [];
    if (timeInput) {
        timeSuggestionArray = timeSuggestions.filter((input) =>{
            return input.toString().startsWith(timeInput.toString());
        });
        timeSuggestionArray = timeSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#callFailureCountByPhoneModelModalStartTimewrapper').addClass("active");
        showSuggestions(timeSuggestionArray, '#callFailureCountByPhoneModelModalStartTime', '#callFailureCountByPhoneModelModalStartTimewrapper', '#callFailureCountByPhoneModelModalStartTimeAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#callFailureCountByPhoneModelModalStartTime', '#callFailureCountByPhoneModelModalStartTimewrapper');
        });
    } else {
        $('#callFailureCountByPhoneModelModalStartTimewrapper').removeClass("active");
    }
});

$('#callFailureCountByPhoneModelModalEndTime').keyup(function (event) { 
    let timeInput = event.target.value;
    let timeSuggestionArray = [];
    if (timeInput) {
        timeSuggestionArray = timeSuggestions.filter((input) =>{
            return input.toString().startsWith(timeInput.toString());
        });
        timeSuggestionArray = timeSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#callFailureCountByPhoneModelModalEndTimewrapper').addClass("active");
        showSuggestions(timeSuggestionArray, '#callFailureCountByPhoneModelModalEndTime', '#callFailureCountByPhoneModelModalEndTimewrapper', '#callFailureCountByPhoneModelModalEndTimeAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#callFailureCountByPhoneModelModalEndTime', '#callFailureCountByPhoneModelModalEndTimewrapper');
        });
    } else {
        $('#callFailureCountByPhoneModelModalEndTimewrapper').removeClass("active");
    }
});

$('#imsiSearchUniqueModalImsiInput').keyup(function (event) { 
    let imsiInput = event.target.value;
    let imsiSuggestionArray = [];
    if (imsiInput) {
        imsiSuggestionArray = imsiSuggestions.filter((input) =>{
            return input.toString().startsWith(imsiInput.toString());
        });
        imsiSuggestionArray = imsiSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#imsiSearchUniqueModalImsiInputwrapper').addClass("active");
        showSuggestions(imsiSuggestionArray, '#imsiSearchUniqueModalImsiInput', '#imsiSearchUniqueModalImsiInputwrapper', '#imsiSearchUniqueModalImsiInputAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#imsiSearchUniqueModalImsiInput', '#imsiSearchUniqueModalImsiInputwrapper');
        });
    } else {
        $('#imsiSearchUniqueModalImsiInputwrapper').removeClass("active");
    }
});

$('#imsiListByTimePeriodModalStartTime').keyup(function (event) { 
    let timeInput = event.target.value;
    let timeSuggestionArray = [];
    if (timeInput) {
        timeSuggestionArray = timeSuggestions.filter((input) =>{
            return input.toString().startsWith(timeInput.toString());
        });
        timeSuggestionArray = timeSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#imsiListByTimePeriodModalStartTimewrapper').addClass("active");
        showSuggestions(timeSuggestionArray, '#imsiListByTimePeriodModalStartTime', '#imsiListByTimePeriodModalStartTimewrapper', '#imsiListByTimePeriodModalStartTimeAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#imsiListByTimePeriodModalStartTime', '#imsiListByTimePeriodModalStartTimewrapper');
        });
    } else {
        $('#imsiListByTimePeriodModalStartTimewrapper').removeClass("active");
    }
});

$('#imsiListByTimePeriodModalEndTime').keyup(function (event) { 
    let timeInput = event.target.value;
    let timeSuggestionArray = [];
    if (timeInput) {
        timeSuggestionArray = timeSuggestions.filter((input) =>{
            return input.toString().startsWith(timeInput.toString());
        });
        timeSuggestionArray = timeSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#imsiListByTimePeriodModalEndTimewrapper').addClass("active");
        showSuggestions(timeSuggestionArray, '#imsiListByTimePeriodModalEndTime', '#imsiListByTimePeriodModalEndTimewrapper', '#imsiListByTimePeriodModalEndTimeAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#imsiListByTimePeriodModalEndTime', '#imsiListByTimePeriodModalEndTimewrapper');
        });
    } else {
        $('#imsiListByTimePeriodModalEndTimewrapper').removeClass("active");
    }
});

$('#PhoneModelSearchModalPhoneModelInput').keyup(function (event) { 
    let modelInput = event.target.value;
    let modelSuggestionArray = [];
    if (modelInput) {
        modelSuggestionArray = phoneModelSuggestions.filter((input) =>{
            return input.toString().startsWith(modelInput.toString());
        });
        modelSuggestionArray = modelSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#PhoneModelSearchModalPhoneModelInputwrapper').addClass("active");
        showSuggestions(modelSuggestionArray, '#PhoneModelSearchModalPhoneModelInput', '#PhoneModelSearchModalPhoneModelInputwrapper', '#PhoneModelSearchModalPhoneModelAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#PhoneModelSearchModalPhoneModelInput', '#PhoneModelSearchModalPhoneModelInputwrapper');
        });
    } else {
        $('#PhoneModelSearchModalPhoneModelInputwrapper').removeClass("active");
    }
});

$('#callFailureCountPlusDurationByImsiModalStartTime').keyup(function (event) { 
    let timeInput = event.target.value;
    let timeSuggestionArray = [];
    if (timeInput) {
        timeSuggestionArray = timeSuggestions.filter((input) =>{
            return input.toString().startsWith(timeInput.toString());
        });
        timeSuggestionArray = timeSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#callFailureCountPlusDurationByImsiModalStartTimewrapper').addClass("active");
        showSuggestions(timeSuggestionArray, '#callFailureCountPlusDurationByImsiModalStartTime', '#callFailureCountPlusDurationByImsiModalStartTimewrapper', '#callFailureCountPlusDurationByImsiModalStartTimeAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#callFailureCountPlusDurationByImsiModalStartTime', '#callFailureCountPlusDurationByImsiModalStartTimewrapper');
        });
    } else {
        $('#callFailureCountPlusDurationByImsiModalStartTimewrapper').removeClass("active");
    }
});

$('#callFailureCountPlusDurationByImsiModalEndTime').keyup(function (event) { 
    let timeInput = event.target.value;
    let timeSuggestionArray = [];
    if (timeInput) {
        timeSuggestionArray = timeSuggestions.filter((input) =>{
            return input.toString().startsWith(timeInput.toString());
        });
        timeSuggestionArray = timeSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#callFailureCountPlusDurationByImsiModalEndTimewrapper').addClass("active");
        showSuggestions(timeSuggestionArray, '#callFailureCountPlusDurationByImsiModalEndTime', '#callFailureCountPlusDurationByImsiModalEndTimewrapper', '#callFailureCountPlusDurationByImsiModalEndTimeAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#callFailureCountPlusDurationByImsiModalEndTime', '#callFailureCountPlusDurationByImsiModalEndTimewrapper');
        });
    } else {
        $('#callFailureCountPlusDurationByImsiModalEndTimewrapper').removeClass("active");
    }
});

$('#callFailureModelOperatorIdByDateModalStartTime').keyup(function (event) { 
    let timeInput = event.target.value;
    let timeSuggestionArray = [];
    if (timeInput) {
        timeSuggestionArray = timeSuggestions.filter((input) =>{
            return input.toString().startsWith(timeInput.toString());
        });
        timeSuggestionArray = timeSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#callFailureModelOperatorIdModalStartTimewrapper').addClass("active");
        showSuggestions(timeSuggestionArray, '#callFailureModelOperatorIdByDateModalStartTime', '#callFailureModelOperatorIdModalStartTimewrapper', '#callFailureModelOperatorIdModalStartTimeAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#callFailureModelOperatorIdByDateModalStartTime', '#callFailureModelOperatorIdModalStartTimewrapper');
        });
    } else {
        $('#callFailureModelOperatorIdModalStartTimewrapper').removeClass("active");
    }
});

$('#callFailureModelOperatorIdByDateModalEndTime').keyup(function (event) { 
    let timeInput = event.target.value;
    let timeSuggestionArray = [];
    if (timeInput) {
        timeSuggestionArray = timeSuggestions.filter((input) =>{
            return input.toString().startsWith(timeInput.toString());
        });
        timeSuggestionArray = timeSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#callFailureModelOperatorIdModalEndTimewrapper').addClass("active");
        showSuggestions(timeSuggestionArray, '#callFailureModelOperatorIdByDateModalEndTime', '#callFailureModelOperatorIdModalEndTimewrapper', '#callFailureModelOperatorIdModalEndTimeAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#callFailureModelOperatorIdByDateModalEndTime', '#callFailureModelOperatorIdModalEndTimewrapper');
        });
    } else {
        $('#callFailureModelOperatorIdModalEndTimewrapper').removeClass("active");
    }
});

$('#top10ImsiCallfailuresWithinTimeperiodModalStartTime').keyup(function (event) { 
    let timeInput = event.target.value;
    let timeSuggestionArray = [];
    if (timeInput) {
        timeSuggestionArray = timeSuggestions.filter((input) =>{
            return input.toString().startsWith(timeInput.toString());
        });
        timeSuggestionArray = timeSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#top10ImsiCallfailuresWithinTimeperiodModalStartTimewrapper').addClass("active");
        showSuggestions(timeSuggestionArray, '#top10ImsiCallfailuresWithinTimeperiodModalStartTime', '#top10ImsiCallfailuresWithinTimeperiodModalStartTimewrapper', '#top10ImsiCallfailuresWithinTimeperiodModalStartTimeAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#top10ImsiCallfailuresWithinTimeperiodModalStartTime', '#top10ImsiCallfailuresWithinTimeperiodModalStartTimewrapper');
        });
    } else {
        $('#top10ImsiCallfailuresWithinTimeperiodModalStartTimewrapper').removeClass("active");
    }
});

$('#top10ImsiCallfailuresWithinTimeperiodModalEndTime').keyup(function (event) { 
    let timeInput = event.target.value;
    let timeSuggestionArray = [];
    if (timeInput) {
        timeSuggestionArray = timeSuggestions.filter((input) =>{
            return input.toString().startsWith(timeInput.toString());
        });
        timeSuggestionArray = timeSuggestionArray.map((input) => {
            return input = '<li>' + input + '</li>';
        });
        $('#top10ImsiCallfailuresWithinTimeperiodModalEndTimewrapper').addClass("active");
        showSuggestions(timeSuggestionArray, '#top10ImsiCallfailuresWithinTimeperiodModalEndTime', '#top10ImsiCallfailuresWithinTimeperiodModalEndTimewrapper', '#top10ImsiCallfailuresWithinTimeperiodModalEndTimeAutofillSuggestions');
        $('.autofill-box li').click(function() {
            useSuggestion($(this).text(), '#top10ImsiCallfailuresWithinTimeperiodModalEndTime', '#top10ImsiCallfailuresWithinTimeperiodModalEndTimewrapper');
        });
    } else {
        $('#top10ImsiCallfailuresWithinTimeperiodModalEndTimewrapper').removeClass("active");
    }
});



function useSuggestion(suggestion, inputId, wrapperId) {
    $(inputId).val(suggestion);
    $(wrapperId).removeClass("active");
}

function showSuggestions(suggestionArray, inputId, wrapperId, autofillBoxId) {
    let suggestions;
	console.log(suggestionArray[0]);
	console.log(('<li>' + $(inputId).val() + '</li>'));
    if (!suggestionArray.length) {
        suggestions = '<li></li>';
		$(wrapperId).removeClass("active");
    } else if (suggestionArray[0] == ('<li>' + $(inputId).val() + '</li>')) {
		suggestions = '<li></li>';
		$(wrapperId).removeClass("active");
	}else {
        suggestions = suggestionArray.slice(0, 20).join('');
    }
    $(autofillBoxId).html(suggestions);
}

$('.modal-box').click(function() {
	$('.autofill-wrapper').removeClass('active');
});

$('.modal').click(function() {
	$('.autofill-wrapper').removeClass('active');
});

/* ************************* DOCUMENT READY ************************* */

$('document').ready(function() {

	resetDisplay();

	$('#navBarLogInButton').click(function() {
		if (loggedIn == false) {
			openModal('#logInModal');
		} else {
			$('#logOutConfirmation').css('font-size', '20px');
			$('#logOutConfirmation').css('color', 'black');
			$('#logOutConfirmation').css('font-weight', 'bold');
			openModal('#logOutConfirmationModal');
		}

	});

	$('#homeLogInButton').click(function() {
		openModal('#logInModal');
	});

	$('#homeLogOutButton').click(function() {
		$('#logOutConfirmation').css('font-size', '20px');
		$('#logOutConfirmation').css('color', 'black');
		$('#logOutConfirmation').css('font-weight', 'bold');
		openModal('#logOutConfirmationModal');
	});

	$('#logInModalCloseButton').click(function() {
		clearModalFields('logIn');
		closeModal('#logInModal');
	});

	$('#logInModalLogInButton').click(function() {
		logInModalLogInButtonClicked();
	});

	$('#logOutConfirmationModalConfirmButton').click(function() {
		logOutUser();
	});

	$('#logOutConfirmationModalCloseButton').click(function() {
		closeModal('#logOutConfirmationModal');
	});

	$('#createUserButton').click(function() {
		openModal('#createUserModal');
	});

	$('#createUserModalCreateUserButton').click(function() {
		createUserModalCreateUserButtonClicked();
	});

	$('#createUserModalCloseButton').click(function() {
		clearModalFields('createUser');
		closeModal('#createUserModal');
	});

	$('#importDataButton').click(function() {
		importDataButtonClicked();
	});

	$('#importDataModalCloseButton').click(function() {
		closeModal('#importDataModal');
	});

	$('#importDataModalInvalidRecordsButton').click(function() {
		invalidRecordsButtonClicked();
	});

	$('#invalidRecordsModalCloseButton').click(function() {
		closeModal('#invalidRecordsModal');
	});

	$('#imsiSearchButton').click(function() {
		openModal('#imsiSearchModal');
	});

	$('#imsiSearchModalCloseButton').click(function() {
		clearModalFields('imsiSearch');
		closeModal('#imsiSearchModal');
	});

	$('#imsiSearchModalRunButton').click(function() {
		imsiSearchModalRunButtonClicked();
	});

	$('#imsiSearchResultsModalCloseButton').click(function() {
		closeModal('#imsiSearchResultsModal');
	});

	$('#callFailureCountByImsiButton').click(function() {
		openModal('#callFailureCountByImsiModal');
	});

	$('#callFailureCountByImsiModalCloseButton').click(function() {
		clearModalFields('callFailureCountByImsi');
		closeModal('#callFailureCountByImsiModal');
	});

	$('#callFailureCountByImsiModalRunButton').click(function() {
		callFailureCountByImsiModalRunButtonClicked();
	});

	$('#callFailureCountByImsiResultModalCloseButton').click(function() {
		closeModal('#callFailureCountByImsiResultModal');
	});

	$('#imsiListByTimePeriodButton').click(function() {
		openModal('#imsiListByTimePeriodModal');
	});

	$('#imsiListByTimePeriodModalCloseButton').click(function() {
		clearModalFields('imsiListByTimePeriod');
		closeModal('#imsiListByTimePeriodModal');
	});

	$('#imsiListByTimePeriodModalRunButton').click(function() {
		imsiListByTimePeriodModalRunButtonClicked();
	});

	$('#imsiListByTimePeriodResultsModalCloseButton').click(function() {
		closeModal('#imsiListByTimePeriodResultsModal');
	});

	$('#callFailureCountByPhoneModelButton').click(function() {
		openModal('#callFailureCountByPhoneModelModal');
	});

	$('#callFailureCountByPhoneModelModalCloseButton').click(function() {
		clearModalFields('callFailureCountByPhoneModel');
		closeModal('#callFailureCountByPhoneModelModal');
	});

	$('#callFailureCountByPhoneModelModalRunButton').click(function() {
		callFailureCountByPhoneModelModalRunButtonClicked();
	});

	$('#callFailureCountByPhoneModelResultModalCloseButton').click(function() {
		closeModal('#callFailureCountByPhoneModelResultModal');
	});

	$('#PhoneModelSearchButton').click(function() {
		openModal('#PhoneModelSearchModal');
	});

	$('#PhoneModelSearchModalCloseButton').click(function() {
		clearModalFields('phoneModelSearch');
		closeModal('#PhoneModelSearchModal');
	});

	$('#PhoneModelSearchModalRunButton').click(function() {
		PhoneModelSearchModalRunButtonClicked();
	});

	$('#PhoneModelSearchResultsModalCloseButton').click(function() {
		closeModal('#PhoneModelSearchResultsModal');
	});

	$('#callFailureCountPlusDurationByImsiButton').click(function() {
		openModal('#callFailureCountPlusDurationByImsiModal');
	});

	$('#callFailureCountPlusDurationByImsiModalCloseButton').click(function() {
		clearModalFields('callFailureCountPlusDurationByImsi');
		closeModal('#callFailureCountPlusDurationByImsiModal');
	});

	$('#callFailureCountPlusDurationByImsiModalRunButton').click(function() {
		callFailureCountPlusDurationByImsiModalRunButtonClicked();
	});

	$('#callFailureCountPlusDurationByImsiResultsModalCloseButton').click(function() {
		closeModal('#callFailureCountPlusDurationByImsiResultsModal');
	});

	$('#imsiSearchUniqueButton').click(function() {
		openModal('#imsiSearchUniqueModal');
	});

	$('#imsiSearchUniqueModalRunButton').click(function() {
		imsiSearchUniqueModalRunButtonClicked();
	});

	$('#imsiSearchUniqueModalCloseButton').click(function() {
		clearModalFields('imsiSearchUnique')
		closeModal('#imsiSearchUniqueModal');
	});

	$('#imsiSearchUniqueResultsModalCloseButton').click(function() {
		closeModal('#imsiSearchUniqueResultsModal');
	});

	$('#callFailureMarketModelIdByDateButton').click(function() {
		openModal('#callFailureModelOperatorIdModal');
	});

	$('#callFailureModelOperatorIdByDateModalCloseButton').click(function() {
		clearModalFields('callFailureModelByOperatorId');
		closeModal('#callFailureModelOperatorIdModal');
	});

	$('#callFailureModelOperatorIdByDateModalRunButton').click(function() {
		callFailureModelOperatorIdByDateModalRunButtonClicked();
	});

	$('#CallFailureModelOperatorIdResultsModalCloseButton').click(function() {
		closeModal('#CallFailureModelOperatorIdResultsModal');
	});
	
/* ******* TOP 10 IMSIs that had CallFailures Within TimePeriod ******* */
	$('#top10ImsiCallfailuresWithinTimeperiodButton').click(function() {
		openModal('#top10ImsiCallfailuresWithinTimeperiodModal');
	});
	
	$('#top10ImsiCallfailuresWithinTimeperiodModalCloseButton').click(function() {
		clearModalFields('top10ImsiCallfailuresWithinTimeperiod');
		closeModal('#top10ImsiCallfailuresWithinTimeperiodModal');
	});
	
	$('#top10ImsiCallfailuresWithinTimeperiodModalRunButton').click(function() {
		top10ImsiCallfailuresWithinTimeperiodModalRunButtonClicked();
	});
	
	$('#top10ImsiCallfailuresWithinTimeperiodResultsModalCloseButton').click(function() {
		closeModal('#top10ImsiCallfailuresWithinTimeperiodResultsModal');
	});

	$('#PhoneModelSearchModalGraphButton').click(function() {
		createPhoneModelGraph();
	});

	$('#phoneModelGraphResultsModalCloseButton').click(function() {
		phoneModelChart.destroy();
		closeModal('#phoneModelGraphResultsModal');
	});

	$('#phoneModelGraphResetButton').click(function() {
		phoneModelChart.destroy();
		createPhoneModelGraph();
	});

	$('#callFailureCountPlusDurationByImsiModalGraphButton').click(function() {
		createImsiDateGraph();
	});

	$('#dateImsiGraphResultsModalCloseButton').click(function() {
		dateImsiGraph.destroy();
		closeModal('#dateImsiGraphResultsModal');
	});
	
	$('#dateImsiGraphResetButton').click(function() {
		dateImsiGraph.destroy();
		createImsiDateGraph();
	});
	
	$('#callFailureModelOperatorIdByDateModalGraphButton').click(function() {
		createTop10CallFailureGraph();
	});
	
	$('#top10CallFailuresGraphResultsModalCloseButton').click(function() {
		top10CallFailureGraph.destroy();
		closeModal('#top10CallFailuresGraphResultsModal');
	});
	
	$('#top10CallFailuresGraphResetButton').click(function() {
		top10CallFailureGraph.destroy();
		createTop10CallFailureGraph();
	});
	
	$('#top10ImsiCallfailuresWithinTimeperiodModalGraphButton').click(function() {
		createTop10ImsiGraph();
	});
	
	$('#top10ImsiCallFailuresGraphResultsModalCloseButton').click(function() {
		top10ImsiGraph.destroy();
		closeModal('#top10ImsiCallfailuresWithinTimeperiodGraphResultsModal');
	});
	
	$('#top10ImsiCallFailuresGraphResetButton').click(function() {
		top10ImsiGraph.destroy();
		createTop10ImsiGraph();
	});
	
	
	$('#imsiByFailureClassButton').click(function(){
		openModal('#imsiFailureClassModal');
	});
	
	$('#imsiFailureClassModalRunButton').click(function() {
		imsiFailureClassModalRunButtonClicked();
	});
	
	$('#imsiFailureClassResultsModalCloseButton').click(function() {
		closeModal('#imsiFailureClassResultsModal');
	});
	
	$('#imsiFailureClassModalCloseButton').click(function() {
		clearModalFields('imsiFailureClass');
		closeModal('#imsiFailureClassModal');
	});

});

