let phoneModelSuggestions = [
	'G410',
	'A53',
	'TBD (AAB-1880030-BV)',
	'RM-669',
	'M930 NA DB',
	'EBX700',
	'Test IMEI',
	'TCD718',
	'LMU',
	'GX-28',
	'ALCATEL OT-807A',
	'Wireless CPU Q2687',
	'WMO2-g1900',
	'Ovation MC547',
	'RAP40GW',
	'MX-5010',
	'Zoarmon',
	'SGH-t829',
	'Telguard 5 (TG5)',
	'Fizgig',
	'KMP6J1S1-6',
	'USB316',
	'U1',
	'Ferry',
	'700C',
	'K1',
	'7525 Workabout pro',
	'Benefon Track Box',
	'AD600',
	'GSM5108',
	'TM3000-C ATD',
	'G1000',
	'AGM 1100',
	'ITH155/MGH900',
	'CF-29/CF-18/CF-73/CF-P1',
	'H6xxx',
	'Artema Mobile Secure GPRS',
	'TS34',
	'PLD',
	'1000-1146',
	'GU-1000',
	'VK530',
	'BM3-891G GPRS OEM Modem',
	'P7',
	'PLD100 series',
	'Jembi',
	'U-300',
	'DM1000G',
	'SM5100B',
	'Debussy',
	'GX820',
	'WRTU54G',
	'whereQube201',
	'R100',
	'Dolphin 9900',
	'Nurit 8020',
	'SN-LSb-02',
	'IMM6071-M01',
	'247910',
	'Machine Gateway',
	'XP3300-AR1 (P25C005AA)',
	'TSN-1.1',
	'Dolphin 10K',
	'WWH9010',
	'Gobi3000',
	'VEA3',
	'9109 PA',
	'Dirland Miniphone',
	'9109PA',
	'Lisa 9030 Type 9109H',
	'Dirland Mobiphone',
	'Mitsubishi GSM MT-1000F02A',
	'Audiovox Type GSM 510',
	'Dirland Type Miniphone III',
	'GSM 510 Type Audiovox HB 160',
	'Vodafone VN 2121',
	'Sagem Type G 14',
	'RC410/430 Type G14-1',
	'Pocketline Tango Type NPTT HC 400',
	'Alcatel Airtel HC 600 Type Airtel HC 600',
	'Affinity/Affinity 930 Type G14-S',
	'Mitsubishi GSM MT 20 Type MT 1171FD2',
	'Mitsubishi GSM MT 10 Type MT 1176F02',
	'Alcatel 9100 Type 9109 HC 500',
	'Alcatel 9100 Type 9109 HC 800',
	'Detewe CP-ONE Type G14.1',
	'RM 833-/S/R Type G14-S',
	'Alcatel Movistar HC 100 Type Telefonica HC 1000',
	'SGH 200',
	'Mitsubishi GSM MT11 Type MT 1177 F02A',
	'Mitsubishi GSM MT20 D Type MT 1172 F02A'
]