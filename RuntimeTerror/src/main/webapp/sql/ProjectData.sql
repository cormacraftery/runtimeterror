drop database if exists networkdb;
create database if not exists networkdb;
use networkdb;

drop table if exists callFailure;

CREATE TABLE IF NOT EXISTS callFailure (
    `id` int auto_increment primary key,
    `dateTime` varchar(100),
    `formattedDateTime` varchar(100),
    `eventId` INT,
    `failureClass` VARCHAR(50),
    `ueType` INT,
    `market` INT,
    `operator` INT,
    `cellId` INT,
    `duration` INT,
    `causeCode` VARCHAR(50),
    `neVersion` VARCHAR(50),
    `imsi` VARCHAR(30),
    `hier3Id` VARCHAR(30),
    `hier32Id` VARCHAR(30),
    `hier321Id` VARCHAR(30)
);

select * from callFailure;

CREATE TABLE IF NOT EXISTS invalidCallFailure (
    `id` int auto_increment primary key,
    `dateTime` varchar(100),
    `eventId` INT,
    `failureClass` VARCHAR(50),
    `ueType` INT,
    `market` INT,
    `operator` INT,
    `cellId` INT,
    `duration` INT,
    `causeCode` VARCHAR(50),
    `neVersion` VARCHAR(50),
    `imsi` VARCHAR(30),
    `hier3Id` VARCHAR(30),
    `hier32Id` VARCHAR(30),
    `hier321Id` VARCHAR(30),
    `invalidReason` varchar(100)
);

select * from invalidCallFailure where failureClass="(null)";

CREATE TABLE IF NOT EXISTS failureClass (
    `id` int auto_increment primary key,
    `failureClass` varchar(100),
    `description` varchar(100)
);
CREATE TABLE IF NOT EXISTS ue (
	`id` int auto_increment primary key,
	 `tac` INT,
    `marketingName` VARCHAR(50),
    `manufacturer` VARCHAR(50),
    `accessCapability` VARCHAR(300)
);
select * from ue;
CREATE TABLE IF NOT EXISTS eventcause (
    `id` int auto_increment primary key,
    `causeCode` varchar(100),
    `eventId` varchar(100),
    `description` varchar(100)
);

select * from callFailure;
select count(*) from callFailure;
drop table if exists users;
create table if not exists users (
`id` int auto_increment primary key,
userType varchar(100),
userName varchar(100),
userPassword varchar(100)
);

insert into users values(null, "Administrator", "0000000001", "sD3fPKLnFKZUjnSV4qA/XoJOqsmDfNfxWcZ7kPtLc0I=");
select * from users;