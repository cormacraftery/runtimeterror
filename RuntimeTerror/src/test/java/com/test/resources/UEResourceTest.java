package com.test.resources;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project.model.CallFailure;
import com.project.model.InvalidCallFailure;
import com.project.model.UE;
import com.project.model.Users;
import com.project.resource.UEResource;
import com.project.services.UEService;


public class UEResourceTest {
	
	private UEResource ueResource;
	
	@Mock
	private UEService ueService;
	CallFailure callFailure;
	
	UE ue;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		ueResource = new UEResource();
		ueResource.setUEService(ueService);
		
		ue = new UE();
		ue.setId(0);
		ue.setTac(1);
		ue.setManufacturer("manu");
		ue.setMarketingName("mName");
		ue.setAccessCapability("access");
		
		callFailure = new CallFailure();
		callFailure.setDateTime("11/1/2020");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
    	callFailure.setOperator(930);
    	callFailure.setCellId(4);
    	callFailure.setDuration(1000);
    	callFailure.setCauseCode("24");
    	callFailure.setNeVersion("11B");
    	callFailure.setImsi("310560000000012");
    	callFailure.setHier3Id("4809532081614990300");
    	callFailure.setHier32Id("8226896360947470300");
    	callFailure.setHier321Id("1150444940909479940");
	}
	
	@Test
	public void testUpload() {
		Response response = ueResource.upload();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testFindUEs() {
		List<UE> ues = new ArrayList<UE>();
		when(ueService.getAllUE()).thenReturn(ues);
		Response response = ueResource.getAllUEs();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	@Test
	public void findPhoneModelTest() {
		List<UE> ueList = new ArrayList<>();
		ueList.add(ue);
		when(ueService.findPhoneModel("mName")).thenReturn(ueList);
		Response response = ueResource.findPhoneModel("mName");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	@Test
	public void findInvalidPhoneModelTest() {
		List<UE> ueList = new ArrayList<>();
		ueList.add(ue);
		Response response = ueResource.findPhoneModel("mName");
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.getStatus());
	}
	
	@Test
	public void getCellValuesForPhoneModelTest() {
		List<UE> ues = new ArrayList<UE>();
		ues.add(ue);
		when(ueService.getCellValuesForPhoneModel("mName","24",4098)).thenReturn(ues);
		Response response = ueResource.getCellValuesForPhoneModel("mName","24",4098);
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void getImsisForCellIdsTest() {
		List<UE> ues = new ArrayList<UE>();
		ues.add(ue);
		when(ueService.getImsisForCellIds("mName","24",4098, 4)).thenReturn(ues);
		Response response = ueResource.getImsisForCellIds("mName","24",4098, 4);
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}

}
