package com.test.resources;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project.model.CallFailure;
import com.project.model.EventCause;
import com.project.resource.EventCauseResource;
import com.project.services.EventCauseService;

public class EventCauseResourceTest {
	
    private EventCauseResource eventCauseResource;
	
	@Mock
	private EventCauseService eventCauseService;
	
	EventCause eventCause;
	
	CallFailure callFailure;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		eventCauseResource = new EventCauseResource();
		callFailure = new CallFailure();
		eventCauseResource.setEventCauseService(eventCauseService);
		
		EventCause eventCause = new EventCause();
		eventCause.setEventId(4098);
		eventCause.setCauseCode("24");
		eventCause.setDescription("Call failure");
		
		callFailure.setDateTime("11/1/2020");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
    	callFailure.setOperator(930);
    	callFailure.setCellId(4);
    	callFailure.setDuration(1000);
    	callFailure.setCauseCode("24");
    	callFailure.setNeVersion("11B");
    	callFailure.setImsi("310560000000012");
    	callFailure.setHier3Id("4809532081614990300");
    	callFailure.setHier32Id("8226896360947470300");
    	callFailure.setHier321Id("1150444940909479940");
	}
	
	@Test
	public void testFindImsiEvents() {
		List<EventCause> eventCauses = new ArrayList<>();
		eventCauses.add(eventCause);
		when(eventCauseService.findImsiEvents(callFailure.getImsi())).thenReturn(eventCauses);
		Response response = eventCauseResource.findImsiEvents(callFailure.getImsi());
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testUpload() {
		Response response = eventCauseResource.upload();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testfindImsiEventsUnique() {
		List<EventCause> eventCauses = new ArrayList<>();
		eventCauses.add(eventCause);
		when(eventCauseService.findImsiEventsUnique(callFailure.getImsi())).thenReturn(eventCauses);
		Response response = eventCauseResource.findImsiEventsUnique(callFailure.getImsi());
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}

}
