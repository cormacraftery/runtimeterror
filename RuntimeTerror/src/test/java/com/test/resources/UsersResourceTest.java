package com.test.resources;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project.model.Users;
import com.project.resource.UsersResource;
import com.project.services.UsersService;

public class UsersResourceTest {

	@Mock
	UsersService usersServices;
	
	UsersResource userResource;
	
	Users users;
	
	Users users2;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		userResource=new UsersResource();
		userResource.setUserService(usersServices);
		
		users = new Users();
		users.setId(0);
		users.setUserName("1234567899");
		users.setUserPassword("P@ssw0rd");
		users.setUserType("Admin");
		users2 = new Users();
		users2.setId(0);
		users2.setUserName("1234567891");
		users2.setUserPassword("P@ssw0rd");
		users2.setUserType("Admin");
		
	}

	@Test
	public void getAllUsersTest(){
		List<Users> usersList = new ArrayList<>();
		when(usersServices.getAllUsers()).thenReturn(usersList);
		Response response = userResource.findValidUsers();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void saveUsersTest(){
		when(usersServices.save(users)).thenReturn(users);
		Response response = userResource.saveUser(users);
		assertEquals(HttpStatus.SC_CREATED, response.getStatus());
	}
	
	@Test
	public void findUserTest(){
		List<Users> usersList = new ArrayList<>();
		usersList.add(users);
		when(usersServices.getSpecificUser("1234567899", "P@ssw0rd")).thenReturn(usersList);
		Response response = userResource.findUser("1234567899","P@ssw0rd");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void findInvalidUserTest(){
		List<Users> usersList = new ArrayList<>();
		usersList.add(users);
		when(usersServices.getSpecificUser("1234511111", "P@ssw0rd")).thenReturn(usersList);
		Response response = userResource.findUser("1234567899","P@ssw0rd");
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.getStatus());
	}
	
	@Test
	public void generateHashPasswordTest(){
		List<Users> usersList = new ArrayList<>();
		usersList.add(users);
		
		when(usersServices.generatePasswordHash("P@ssw0rd")).thenReturn("sD3fPKLnFKZUjnSV4qA/XoJOqsmDfNfxWcZ7kPtLc0I=");
		Response response = userResource.generatePasswordHash("P@ssw0rd");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}

}
