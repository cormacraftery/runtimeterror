package com.test.resources;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project.model.CallFailure;
import com.project.model.EventCause;
import com.project.model.FailureClass;
import com.project.resource.FailureClassResource;
import com.project.services.FailureClassService;



public class FailureClassResourceTest {

	
	private FailureClassResource failureClassResource;
	
	@Mock
	private FailureClassService failureClassService;
	CallFailure callFailure;
	FailureClass failureClass;
	EventCause eventCause;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		failureClassResource = new FailureClassResource();
		failureClassResource.setFailureClassService(failureClassService);
		callFailure = new CallFailure();
		FailureClass failureClass = new FailureClass();
		EventCause eventCause = new EventCause();
		eventCause.setEventId(4098);
		eventCause.setCauseCode("24");
		eventCause.setDescription("Call failure");
		
		failureClass.setId(0);
		failureClass.setFailureClass("1");
		failureClass.setDescription("Call failed");
		
		callFailure.setDateTime("11/1/2020");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
    	callFailure.setOperator(930);
    	callFailure.setCellId(4);
    	callFailure.setDuration(1000);
    	callFailure.setCauseCode("24");
    	callFailure.setNeVersion("11B");
    	callFailure.setImsi("310560000000012");
    	callFailure.setHier3Id("4809532081614990300");
    	callFailure.setHier32Id("8226896360947470300");
    	callFailure.setHier321Id("1150444940909479940");
	}

	@Test
	public void testUpload() {
		Response response = failureClassResource.upload();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testfindIMSICallFailuresCountAndDurationWithinTimePeriod() {
		List<FailureClass> failureClasses = new ArrayList<>();
		failureClasses.add(failureClass);
		when(failureClassService.findIMSICallFailuresCountAndDurationWithinTimePeriod("1/11/20", "11:11","1/11/20","11:11")).thenReturn(failureClasses);
		Response response = failureClassResource.findIMSICallFailuresCountAndDurationWithinTimePeriod("1/11/20", "11:11","1/11/20","11:11");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testInvalidfindIMSICallFailuresCountAndDurationWithinTimePeriod() {
		List<FailureClass> failureClasses = new ArrayList<>();
		failureClasses.add(failureClass);
		when(failureClassService.findIMSICallFailuresCountAndDurationWithinTimePeriod("1/11/20", "11:10","1/11/20","11:10")).thenReturn(failureClasses);
		Response response = failureClassResource.findIMSICallFailuresCountAndDurationWithinTimePeriod("1/11/20", "11:11","1/11/20","11:11");
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.getStatus());
	}
	
	@Test
	public void testfindIMSICallFailuresCountAndDurationWithinTimePeriodGrouped() {
		List<FailureClass> failureClasses = new ArrayList<>();
		failureClasses.add(failureClass);
		when(failureClassService.getIMSICountAndDurationCallFailuresWithinTimePeriodGrouped("1/11/20", "11:11","1/11/20","11:11")).thenReturn(failureClasses);
		Response response = failureClassResource.findIMSICallFailuresCountAndDurationWithinTimePeriodGrouped("1/11/20", "11:11","1/11/20","11:11");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testInvalidfindIMSICallFailuresCountAndDurationWithinTimePeriodGrouped() {
		List<FailureClass> failureClasses = new ArrayList<>();
		failureClasses.add(failureClass);
		when(failureClassService.getIMSICountAndDurationCallFailuresWithinTimePeriodGrouped("1/11/20", "11:10","1/11/20","11:10")).thenReturn(failureClasses);
		Response response = failureClassResource.findIMSICallFailuresCountAndDurationWithinTimePeriodGrouped("1/11/20", "11:11","1/11/20","11:11");
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.getStatus());
	}

	@Test
	public void testcellDrillDown() {
		List<FailureClass> failureClasses = new ArrayList<>();
		failureClasses.add(failureClass);
		when(failureClassService.cellDrillDown("1/11/20", "11:11","1/11/20","11:11",4)).thenReturn(failureClasses);
		Response response = failureClassResource.cellDrillDown("1/11/20", "11:11","1/11/20","11:11",4);
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testfailureClassDrillDown() {
		List<FailureClass> failureClasses = new ArrayList<>();
		failureClasses.add(failureClass);
		when(failureClassService.failureClassDrillDown("1/11/20", "11:11","1/11/20","11:11",4, "Call failed")).thenReturn(failureClasses);
		Response response = failureClassResource.failureClassDrillDown("1/11/20", "11:11","1/11/20","11:11",4, "Call failed");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testimsiDrillDown() {
		List<FailureClass> failureClasses = new ArrayList<>();
		failureClasses.add(failureClass);
		when(failureClassService.imsiDrillDown("1/11/20", "11:11","1/11/20","11:11",4, "Call failed","Call failure")).thenReturn(failureClasses);
		Response response = failureClassResource.imsiDrillDown("1/11/20", "11:11","1/11/20","11:11",4, "Call failed","Call failure");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
}
