package com.test.resources;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.Response;

import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.project.model.CallFailure;
import com.project.model.InvalidCallFailure;
import com.project.resource.CallFailureResource;
import com.project.services.CallFailureService;

public class CallFailureResourceTest {
	
	private CallFailureResource callFailureResource;
	
	@Mock
	private CallFailureService callFailureService;
	
	CallFailure callFailure;
	
	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		callFailureResource = new CallFailureResource();
		callFailureResource.setCallFailureService(callFailureService);
		
		callFailure = new CallFailure();
		callFailure.setDateTime("11/1/2020");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
    	callFailure.setOperator(930);
    	callFailure.setCellId(4);
    	callFailure.setDuration(1000);
    	callFailure.setCauseCode("24");
    	callFailure.setNeVersion("11B");
    	callFailure.setImsi("310560000000012");
    	callFailure.setHier3Id("4809532081614990300");
    	callFailure.setHier32Id("8226896360947470300");
    	callFailure.setHier321Id("1150444940909479940");
	}
	
	@Test
	public void testUpload() {
		Response response = callFailureResource.saveCallFailure();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testFindAllCallFailures() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		when(callFailureService.getAllCallFailures()).thenReturn(callFailures);
		Response response = callFailureResource.findAllCallFailures();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testFindInvalidCallFailures() {
		List<InvalidCallFailure> invalidCallFailures = new ArrayList<>();
		invalidCallFailures.add(new InvalidCallFailure());
		when(callFailureService.getInvalidCallFailures()).thenReturn(invalidCallFailures);
		Response response = callFailureResource.findInvalidCallFailures();
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testFindIMSICallFailuresWithinTimePeriod() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.findIMSICallFailuresWithinTimePeriod("1/11/20", "1/11/20","11:11","11:11")).thenReturn(callFailures);
		Response response = callFailureResource.findIMSICallFailuresWithinTimePeriod("1/11/20", "1/11/20","11:11","11:11");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testInvalidFindIMSICallFailuresWithinTimePeriod() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.findIMSICallFailuresWithinTimePeriod("1/11/20", "1/11/20","11:10","11:10")).thenReturn(callFailures);
		Response response = callFailureResource.findIMSICallFailuresWithinTimePeriod("1/11/20", "1/11/20","11:11","11:11");
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.getStatus());
	}
	
	
	@Test
	public void testGetImsiCountWithinDate() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.getImsiCountWithinDate("1/11/20", "1/11/20","11:11","11:11", "310560000000012")).thenReturn(callFailures);
		Response response = callFailureResource.getImsiCountWithinDate("1/11/20","11:11", "1/11/20","11:11", "310560000000012");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testGetModelCountWithinDate() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.getModelCountWithinDate("1/11/20", "1/11/20","11:11","11:11", "310560000000012")).thenReturn(callFailures);
		Response response = callFailureResource.getModelCountWithinDate("1/11/20","11:11", "1/11/20","11:11", "310560000000012");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testTop10ModelOperatorCellId() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.findTop10ModelOperatorCellId("1/11/20", "11:11","1/11/20","11:11")).thenReturn(callFailures);
		Response response = callFailureResource.getTop10ModelOperatorCellId("1/11/20","11:11", "1/11/20","11:11");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testInvalidTop10ModelOperatorCellId() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.findTop10ModelOperatorCellId("1/11/20","11:10","1/11/20","11:10")).thenReturn(callFailures);
		Response response = callFailureResource.getTop10ModelOperatorCellId("1/11/20","11:11", "1/11/20","11:11");
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.getStatus());
	}
	
	@Test
	public void testfindtop10IMSICallFailuresWithinTimePeriod() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.getTop10ImsiCallfailuresWithinDate("1/11/20", "11:11","1/11/20","11:11")).thenReturn(callFailures);
		Response response = callFailureResource.findtop10IMSICallFailuresWithinTimePeriod("1/11/20","11:11", "1/11/20","11:11");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testInvalidfindtop10IMSICallFailuresWithinTimePeriod() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.getTop10ImsiCallfailuresWithinDate("1/11/20", "11:10","1/11/20","11:10")).thenReturn(callFailures);
		Response response = callFailureResource.findtop10IMSICallFailuresWithinTimePeriod("1/11/20","11:11", "1/11/20","11:11");
		assertEquals(HttpStatus.SC_BAD_REQUEST, response.getStatus());
	}
	
	@Test
	public void testtop10ModelDrillDown() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.top10ModelDrillDown("1/11/20", "11:11","1/11/20","11:11",344,930,4)).thenReturn(callFailures);
		Response response = callFailureResource.top10ModelDrillDown("1/11/20","11:11", "1/11/20","11:11",344,930,4);
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testtop10ModelSecondDrillDown() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.top10ModelSecondDrillDown("1/11/20", "11:11","1/11/20","11:11",344,930,4,"Call Failed")).thenReturn(callFailures);
		Response response = callFailureResource.top10ModelSecondDrillDown("1/11/20","11:11", "1/11/20","11:11",344,930,4,"Call Failed");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testtop10ImsiSecondDrillDown() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.top10ImsiSecondDrillDown("1/11/20","11:11", "1/11/20","11:11", "310560000000012", 4)).thenReturn(callFailures);
		Response response = callFailureResource.top10ImsiSecondDrillDown("1/11/20","11:11", "1/11/20","11:11", "310560000000012", 4);
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	
	@Test
	public void testtop10ImsiDrillDown() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.top10ImsiDrillDown("1/11/20", "11:11","1/11/20","11:11","310560000000012")).thenReturn(callFailures);
		Response response = callFailureResource.top10ImsiDrillDown("1/11/20","11:11", "1/11/20","11:11", "310560000000012");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
	@Test
	public void testgetIMSIFailureCausen() {
		List<CallFailure> callFailures = new ArrayList<CallFailure>();
		callFailures.add(callFailure);
		when(callFailureService.getIMSIFailureCause("1")).thenReturn(callFailures);
		Response response = callFailureResource.getIMSIFailureCause("1");
		assertEquals(HttpStatus.SC_OK, response.getStatus());
	}
}
