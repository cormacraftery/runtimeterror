package com.test.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.project.model.EventCause;
import com.project.repository.EventCauseRepository;
import com.project.services.EventCauseService;
import com.project.services.EventCauseServiceImpl;

public class EventCauseServicesTest {
	
	EventCauseService eventCauseService;
	EventCauseRepository eventCauseRepository;
	EventCause eventCause;
	
	@Before
	public void init() {
		eventCauseService = new EventCauseServiceImpl();
		eventCauseRepository = mock(EventCauseRepository.class);
		((EventCauseServiceImpl) eventCauseService).eventCauseRepository = eventCauseRepository;
		
		eventCause = new EventCause();
		eventCause.setId(0);
		eventCause.setCauseCode("123");
		eventCause.setDescription("123");
		eventCause.setEventId(4);
	}
	
	@Test
	public void testFindImsiEvents() {
		List<EventCause> eventCauses = new ArrayList<>();
		eventCauses.add(eventCause);
		when(eventCauseRepository.getCallFailure_IMSI("Test")).thenReturn(eventCauses);
		assertEquals(1, eventCauseService.findImsiEvents("Test").size());
	}
	
	@Test
	public void testFindImsiEventsUnique() {
		List<EventCause> eventCauses = new ArrayList<>();
		eventCauses.add(eventCause);
		when(eventCauseRepository.getCallFailure_IMSI_Unique("Test")).thenReturn(eventCauses);
		assertEquals(1, eventCauseService.findImsiEventsUnique("Test").size());
	}
	/*
	@Test
	public void testUplaod() {
		assertEquals(80, eventCauseService.upload().size());
	}
	*/
}
