package com.test.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.project.model.Users;
import com.project.repository.UsersRepository;
import com.project.services.UsersService;
import com.project.services.UsersServiceImpl;

public class UsersServicesTest {
	
	UsersService usersService;
	UsersRepository usersRepository;
	Users user;
	
	@Before
	public void init() {
		usersService = new UsersServiceImpl();
		usersRepository = mock(UsersRepository.class);
		((UsersServiceImpl) usersService).usersRepository = usersRepository;
		
		user = new Users();
	}
	
	@Test
	public void testGetAllUsers() {
		List<Users> users = new ArrayList<>();
		users.add(user);
		when(usersRepository.getAllUsers()).thenReturn(users);
		assertEquals(usersService.getAllUsers().size(), 1);
	}
	
	@Test
	public void testGetUser() {
		when(usersRepository.getUsers(user.getId())).thenReturn(user);
		assertEquals(usersService.getUsers(user.getId()), user);
		
	}
	
	@Test
	public void testgeneratePasswordHash() {
		List<Users> users = new ArrayList<>();
		users.add(user);
		when(usersRepository.generatePasswordHash("P@ssw0rd")).thenReturn("1");
		assertEquals(usersService.generatePasswordHash("P@ssw0rd"), "1");
	}
	
	
	@Test
	public void testSaveUser() {
		when(usersRepository.save(user)).thenReturn(user);
		assertEquals(usersService.save(user), user);
	}
	
	@Test
	public void testGetSpecifiedUser() {
		List<Users> users = new ArrayList<>();
		users.add(user);
		when(usersRepository.getSpecificUser(user.getUserName(), user.getUserPassword())).thenReturn(users);
		assertEquals(1, usersService.getSpecificUser(user.getUserName(), user.getUserPassword()).size());
		List<Users> usersList = usersService.getSpecificUser(user.getUserName(), user.getUserPassword());
		assertEquals(user.getUserType(), usersList.get(0).getUserType());
	}

}
