package com.test.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.project.model.CallFailure;
import com.project.model.FailureClass;
import com.project.model.InvalidCallFailure;
import com.project.repository.CallFailureRepository;
import com.project.services.CallFailureService;
import com.project.services.CallFailureServiceImpl;

public class CallFailureServicesTest {
	
	CallFailureService callFailureService;
	CallFailureRepository callFailureRepository;
	CallFailure callFailure;
	FailureClass failureClass;
	
	@Before
	public void init() {
		callFailureService = new CallFailureServiceImpl();
		callFailureRepository = mock(CallFailureRepository.class);
		((CallFailureServiceImpl) callFailureService).callFailureRepository = callFailureRepository;
		
		callFailure = new CallFailure();
		callFailure.setDateTime("11/1/2020");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
    	callFailure.setOperator(930);
    	callFailure.setCellId(4);
    	callFailure.setDuration(1000);
    	callFailure.setCauseCode("24");
    	callFailure.setNeVersion("11B");
    	callFailure.setImsi("310560000000012");
    	callFailure.setHier3Id("4809532081614990300");
    	callFailure.setHier32Id("8226896360947470300");
    	callFailure.setHier321Id("1150444940909479940");
    	
    	FailureClass failureClass = new FailureClass();
		failureClass.setId(0);
		failureClass.setFailureClass("1");
		failureClass.setDescription("Call failed");
	}
	
	@Test
	public void testGetAllCallFailures() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.getAllCallFailures()).thenReturn(callFailures);
		assertEquals(callFailureService.getAllCallFailures().size(), 1);
	}
	
	@Test
	public void testGetAllInvalidCallFailures() {
		List<InvalidCallFailure> invalidCallFailures = new ArrayList<>();
		InvalidCallFailure invalidCallFailure = new InvalidCallFailure();
		invalidCallFailure.setId(5);
		invalidCallFailures.add(invalidCallFailure);
		when(callFailureRepository.getInvalidCallFailures()).thenReturn(invalidCallFailures);
		assertEquals(callFailureService.getInvalidCallFailures().size(), 1);
		assertEquals(invalidCallFailure.getId(), callFailureService.getInvalidCallFailures().get(0).getId());
	}
	
	@Test
	public void testFindIMSICallFailuresWithinTimePeriod() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.getIMSICallFailuresWithinTimePeriod("11/1/20", "12/1/20", "11:11", "11:11")).thenReturn(callFailures);
		assertEquals(callFailureService.findIMSICallFailuresWithinTimePeriod("11/1/20", "12/1/20","11:11","11:11"), callFailures);
	}
	
	@Test
	public void testGetImsiCountWithinDate() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.getImsiCountWithinDate("11/1/20", "12/1/20", "11:11", "11:11","310560000000012")).thenReturn(callFailures);
		assertEquals(callFailureService.getImsiCountWithinDate("11/1/20", "12/1/20","11:11","11:11", "310560000000012"), callFailures);
	}
	
	@Test
	public void testGetModelCountWithinDate() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.getModelCountWithinDate("11/1/20", "12/1/20", "11:11", "11:11","310560000000012")).thenReturn(callFailures);
		assertEquals(callFailureService.getModelCountWithinDate("11/1/20", "12/1/20","11:11","11:11", "310560000000012"), callFailures);
	}
	
	@Test
	public void testtop10ModelDrillDown() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.top10ModelDrillDown("11/1/20", "12/1/20", "11:11", "11:11",344,930,4)).thenReturn(callFailures);
		assertEquals(callFailureService.top10ModelDrillDown("11/1/20", "12/1/20","11:11","11:11", 344,930,4), callFailures);
	}
	
	@Test
	public void testtop10ModelSecondDrillDown() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.top10ModelSecondDrillDown("11/1/20", "12/1/20", "11:11", "11:11",344,930,4, "Call failed")).thenReturn(callFailures);
		assertEquals(callFailureService.top10ModelSecondDrillDown("11/1/20", "12/1/20","11:11","11:11", 344,930,4, "Call failed"), callFailures);
	}
	
	
	@Test
	public void testtop10ImsiSecondDrillDown() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.top10ImsiSecondDrillDown("11/1/20", "12/1/20", "11:11", "11:11","310560000000012",4)).thenReturn(callFailures);
		assertEquals(callFailureService.top10ImsiSecondDrillDown("11/1/20", "12/1/20", "11:11", "11:11","310560000000012",4), callFailures);
	}
	
	@Test
	public void testtop10ImsiDrillDown() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.top10ImsiDrillDown("11/1/20", "12/1/20", "11:11", "11:11","310560000000012")).thenReturn(callFailures);
		assertEquals(callFailureService.top10ImsiDrillDown("11/1/20", "12/1/20","11:11","11:11", "310560000000012"), callFailures);
	}
	
	@Test
	public void testgetIMSIFailureCause() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.getIMSIFailureCause("1")).thenReturn(callFailures);
		assertEquals(callFailureService.getIMSIFailureCause("1"), callFailures);
	}
	
	@Test
	public void testValidCallFailure() {
		assertTrue(callFailureService.verifyCallFailure(callFailure));
	}
	
	@Test
	public void testInvalidEventId() {
		callFailure.setEventId(4099);
		assertFalse(callFailureService.verifyCallFailure(callFailure));
	}
	
	@Test
	public void testInvalidCauseCode() {
		callFailure.setCauseCode("(null)");
		assertFalse(callFailureService.verifyCallFailure(callFailure));
	}
	
	@Test
	public void testInvalidFailureClass() {
		callFailure.setFailureClass("(null)");
		assertFalse(callFailureService.verifyCallFailure(callFailure));
		callFailure.setFailureClass("5");
		assertFalse(callFailureService.verifyCallFailure(callFailure));
	}
	
	@Test
	public void testInvalidUeType() {
		callFailure.setUeType(21060810);
		assertFalse(callFailureService.verifyCallFailure(callFailure));
		callFailure.setUeType(33000255);
		assertFalse(callFailureService.verifyCallFailure(callFailure));
		callFailure.setUeType(33000257);
		assertFalse(callFailureService.verifyCallFailure(callFailure));
	}
	
	@Test
	public void testInvalidMarketAndOperator1() {
		callFailure.setMarket(355);
		callFailure.setOperator(930);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060900);
		callFailure.setMarket(344);
		callFailure.setOperator(935);
		assertFalse(callFailureService.verifyCallFailure(callFailure));
	}
	
	@Test
	public void testInvalidMarketAndOperator2() {
		callFailure.setMarket(355);
		callFailure.setOperator(930);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060900);
		callFailure.setMarket(345);
		callFailure.setOperator(930);
		assertFalse(callFailureService.verifyCallFailure(callFailure));
	}
	
	@Test
	public void testInvalidMarketAndOperator3() {
		callFailure.setMarket(355);
		callFailure.setCauseCode("24");
		callFailure.setOperator(930);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060900);
		callFailure.setEventId(40);
		assertFalse(callFailureService.verifyCallFailure(callFailure));
	}
	/*
	 * callFailure.setDateTime("11/1/2020");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
    	callFailure.setOperator(930);
    	callFailure.setCellId(4);
    	callFailure.setDuration(1000);
    	callFailure.setCauseCode("24");
    	callFailure.setNeVersion("11B");
    	callFailure.setImsi("310560000000012");
    	callFailure.setHier3Id("4809532081614990300");
    	callFailure.setHier32Id("8226896360947470300");
    	callFailure.setHier321Id("1150444940909479940");
    	
    	
	 if (callFailure.getEventId() == 4099 || callFailure.getCauseCode() == "(null)"
				|| callFailure.getFailureClass().equals("(null)") || Integer.parseInt(callFailure.getFailureClass()) > 4
				|| callFailure.getUeType() == 21060810 || callFailure.getUeType() == 33000255
				|| callFailure.getUeType() == 33000257
				|| (callFailure.getMarket() == 344 && callFailure.getOperator() == 935)
				|| (callFailure.getMarket() == 345 && callFailure.getOperator() == 930)
				|| (callFailure.getMarket() == 355 && callFailure.getOperator() == 930)) {
			callFailureIsValid = false;
		}
	 */
	
	
	@Test
	public void testValidToInvalid() {
		InvalidCallFailure invalidCallFailure = callFailureService.convertValidToInvalid(callFailure);
		assertEquals(invalidCallFailure.getId(), callFailure.getId());
		assertEquals(invalidCallFailure.getCauseCode(), callFailure.getCauseCode());
		assertEquals(invalidCallFailure.getDateTime(), callFailure.getDateTime());
		assertEquals(invalidCallFailure.getEventId(), callFailure.getEventId());
		assertEquals(invalidCallFailure.getFailureClass(), callFailure.getFailureClass());
		assertEquals(invalidCallFailure.getUeType(), callFailure.getUeType());
		assertEquals(invalidCallFailure.getOperator(), callFailure.getOperator());
		assertEquals(invalidCallFailure.getDuration(), callFailure.getDuration());
		assertEquals(invalidCallFailure.getNeVersion(), callFailure.getNeVersion());
	}
	
	@Test
	public void testUploadInvalid() throws FileNotFoundException, IOException {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(new CallFailure());
		assertEquals(1, callFailureService.uploadInvalid(callFailures).size());
		assertEquals(InvalidCallFailure.class, callFailureService.uploadInvalid(callFailures).get(0).getClass());
	}
	
	@Test
	public void testInvalidEventIDReason() {
		callFailure.setEventId(4099);
		assertEquals("Invalid event ID", callFailureService.determineInvalidReason(callFailure));
	}
	
	@Test
	public void testInvalidFailureClassReason() {
		callFailure.setFailureClass("(null)");
		assertEquals("Null failure class", callFailureService.determineInvalidReason(callFailure));
	}
	
	@Test
	public void testInvalidCauseCodeReason() {
		callFailure.setCauseCode("(null)");
		assertEquals("Null cause code", callFailureService.determineInvalidReason(callFailure));
	}
	
	@Test
	public void testInvalidUETypeReason() {
		InvalidCallFailure invalidCallFailure = new InvalidCallFailure();
		callFailure.setUeType(21060810);
		invalidCallFailure.setInvalidReason(callFailureService.determineInvalidReason(callFailure));
		assertEquals("Invalid UE type", invalidCallFailure.getInvalidReason());
		callFailure.setUeType(33000255);
		invalidCallFailure.setInvalidReason(callFailureService.determineInvalidReason(callFailure));
		assertEquals("Invalid UE type", invalidCallFailure.getInvalidReason());
		callFailure.setUeType(33000257);
		invalidCallFailure.setInvalidReason(callFailureService.determineInvalidReason(callFailure));
		assertEquals("Invalid UE type", invalidCallFailure.getInvalidReason());
	}
	
	@Test
	public void test1() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.getImsiCountWithinDate("19/02/2020", "12:00", "21/02/2020", "12:00", "310560000000012")).thenReturn(callFailures);
		assertEquals(1, callFailureService.getImsiCountWithinDate("19/02/2020", "12:00", "21/02/2020", "12:00", "310560000000012").size());
	}
	
	
	@Test
	public void testTop10Query() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.getTop10ModelOperatorCellId("19/02/2020", "12:00", "21/02/2020", "12:00")).thenReturn(callFailures);
		assertEquals(1, callFailureService.findTop10ModelOperatorCellId("19/02/2020", "12:00", "21/02/2020", "12:00").size());
	}
	
	@Test
	public void getTop10ImsiCallfailuresWithinDate() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.getTop10ImsiCallfailuresWithinDate("19/02/2020", "12:00", "21/02/2020", "12:00")).thenReturn(callFailures);
		assertEquals(1, callFailureService.getTop10ImsiCallfailuresWithinDate("19/02/2020", "12:00", "21/02/2020", "12:00").size());
	}

}
