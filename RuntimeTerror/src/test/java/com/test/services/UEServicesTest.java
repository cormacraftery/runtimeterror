package com.test.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.project.model.CallFailure;
import com.project.model.EventCause;
import com.project.model.UE;
import com.project.repository.EventCauseRepository;
import com.project.repository.UERepository;
import com.project.services.EventCauseServiceImpl;
import com.project.services.UEService;
import com.project.services.UEServiceImpl;

public class UEServicesTest {
	
	UEService ueService;
	UERepository ueRepository;
	UE ue;
	
	@Before
	public void init() {
		ueService = new UEServiceImpl();
		ueRepository = mock(UERepository.class);
		((UEServiceImpl) ueService).ueRepository = ueRepository;
		
		ue = new UE();
		ue.setId(0);
		ue.setTac(1);
		ue.setManufacturer("manu");
		ue.setMarketingName("mName");
		ue.setAccessCapability("access");
	}
	/*
	@Test
	public void testFindImsiEvents() {
		List<EventCause> eventCauses = new ArrayList<>();
		eventCauses.add(eventCause);
		when(eventCauseRepository.getCallFailure_IMSI("Test")).thenReturn(eventCauses);
		assertEquals(1, eventCauseService.findImsiEvents("Test").size());
	}
	*/
	@Test
	public void testGetAllUE() {
		List<UE> ueList = new ArrayList<>();
		ueList.add(ue);
		when(ueRepository.getAllUE()).thenReturn(ueList);
		assertEquals(ueService.getAllUE().size(), 1);
	}
	
	@Test
	public void testFindPhoneModelEvents() {
		List<UE> ueList = new ArrayList<>();
		ueList.add(ue);
		when(ueRepository.getPhoneModel("Test")).thenReturn(ueList);
		assertEquals(1, ueService.findPhoneModel("Test").size());
	}
	
	@Test
	public void testGetCellValuesForPhoneModel() {
		List<UE> ueList = new ArrayList<>();
		ueList.add(ue);
		when(ueRepository.getCellValuesForPhoneModel(null, null, 0)).thenReturn(ueList);
		assertEquals(1, ueService.getCellValuesForPhoneModel(null, null, 0).size());
	}
	
	@Test
	public void testgetImsisForCellIds() {
		List<UE> ueList = new ArrayList<>();
		ueList.add(ue);
		when(ueRepository.getImsisForCellIds(null, null, 0, 0)).thenReturn(ueList);
		assertEquals(1, ueService.getImsisForCellIds(null, null, 0, 0).size());
	}
	
	/*
	@Test
	public void testUpload() {
		assertEquals(99, ueService.upload().size());
	}
*/
}
