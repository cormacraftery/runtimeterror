package com.test.services;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.project.model.CallFailure;
import com.project.model.EventCause;
import com.project.model.FailureClass;
import com.project.repository.CallFailureRepository;
import com.project.repository.EventCauseRepository;
import com.project.repository.FailureClassRepository;
import com.project.services.CallFailureService;
import com.project.services.CallFailureServiceImpl;
import com.project.services.EventCauseServiceImpl;
import com.project.services.FailureClassService;
import com.project.services.FailureClassServiceImpl;

public class FailureClassServicesTest {
	
	FailureClassService failureClassService;
	FailureClassRepository failureClassRepository;
	FailureClass failureClass;
	@Before
	public void init() {
		failureClassService = new FailureClassServiceImpl();
		failureClassRepository = mock(FailureClassRepository.class);
		((FailureClassServiceImpl) failureClassService).failureClassRepository = failureClassRepository;
		
		failureClass = new FailureClass();
		failureClass.setId(0);
		failureClass.setFailureClass("1");
		failureClass.setDescription("Call failed");
	}
	@Test
	public void testCellDrillDown() {
		List<FailureClass> failures = new ArrayList<>();
		failures.add(failureClass);
		when(failureClassRepository.cellDrillDown(null, null, null, null, 0)).thenReturn(failures);
		assertEquals(failureClassService.cellDrillDown(null, null, null, null, 0).size(), 1);
	} 
	@Test
	public void testfailureClassDrillDown() {
		List<FailureClass> failures = new ArrayList<>();
		failures.add(failureClass);
		when(failureClassRepository.failureClassDrillDown(null, null, null, null, 0, null)).thenReturn(failures);
		assertEquals(failureClassService.failureClassDrillDown(null, null, null, null, 0, null).size(), 1);
	} 
	@Test
	public void testfindIMSICallFailuresCountAndDurationWithinTimePeriod() {
		List<FailureClass> failures = new ArrayList<>();
		failures.add(failureClass);
		when(failureClassRepository.getIMSICountAndDurationCallFailuresWithinTimePeriod(null, null, null, null)).thenReturn(failures);
		assertEquals(failureClassService.findIMSICallFailuresCountAndDurationWithinTimePeriod(null, null, null, null).size(), 1);
	} 
	@Test
	public void testgetIMSICountAndDurationCallFailuresWithinTimePeriodGrouped() {
		List<FailureClass> failures = new ArrayList<>();
		failures.add(failureClass);
		when(failureClassRepository.getIMSICountAndDurationCallFailuresWithinTimePeriodGrouped(null, null, null, null)).thenReturn(failures);
		assertEquals(failureClassService.getIMSICountAndDurationCallFailuresWithinTimePeriodGrouped(null, null, null, null).size(), 1);
	} 
	@Test
	public void testimsiDrillDown() {
		List<FailureClass> failures = new ArrayList<>();
		failures.add(failureClass);
		when(failureClassRepository.imsiDrillDown(null, null, null, null, 0, null, null)).thenReturn(failures);
		assertEquals(failureClassService.imsiDrillDown(null, null, null, null, 0, null, null).size(), 1);
	} 
	
	
	/*
	
	
	
	@Test
	public void testGetAllCallFailures() {
		List<CallFailure> callFailures = new ArrayList<>();
		callFailures.add(callFailure);
		when(callFailureRepository.getAllCallFailures()).thenReturn(callFailures);
		assertEquals(callFailureService.getAllCallFailures().size(), 1);
	} 
	@Before
	public void init() {
		eventCauseService = new EventCauseServiceImpl();
		eventCauseRepository = mock(EventCauseRepository.class);
		((EventCauseServiceImpl) eventCauseService).eventCauseRepository = eventCauseRepository;
		
		eventCause = new EventCause();
		eventCause.setId(0);
		eventCause.setCauseCode("123");
		eventCause.setDescription("123");
		eventCause.setEventId(4);
	}
	
	@Test
	public void testFindImsiEvents() {
		List<EventCause> eventCauses = new ArrayList<>();
		eventCauses.add(eventCause);
		when(eventCauseRepository.getCallFailure_IMSI("Test")).thenReturn(eventCauses);
		assertEquals(1, eventCauseService.findImsiEvents("Test").size());
	}
	 */
	/*
	@Test
	public void testUpload() {
		assertEquals(5, failureClassService.upload().size());
	}
	*/
}
