package com.test.selenium;

import static org.junit.Assert.*;
//import org.junit.jupiter.api.Test;

import java.util.concurrent.TimeUnit;

import org.junit.Before;
import org.junit.Test;
import org.junit.After;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class GuiTest {

	WebDriver driver;

	@Before
	public void setUp() throws Exception {
		//System.setProperty("webdriver.chrome.driver", "C:\\Users\\sanpc\\OneDrive - Athlone Institute Of Technology\\MASE\\SEM_2\\GroupProject\\chromedriver.exe");
		//driver = new ChromeDriver();
		driver=null;
		WebDriverManager.chromedriver().version("99.0.4844.51").setup();
		ChromeOptions options = new ChromeOptions();
		options.addArguments("start-maximized"); 
		options.addArguments("enable-automation"); 
		options.addArguments("--no-sandbox"); 
		options.addArguments("--disable-infobars");
		options.addArguments("--disable-dev-shm-usage");
		options.addArguments("--disable-browser-side-navigation"); 
		options.addArguments("--disable-gpu"); 
		driver = new ChromeDriver(options); 
		driver.get("http://localhost:8080/RuntimeTerror/Dashboard.html");

		WebElement signinButton=driver.findElement(By.xpath("//*[@id=\"homeLogInButton\"]"));
		signinButton.click();
		WebElement userId=driver.findElement(By.id("logInUsernameInput"));
		userId.sendKeys("0000000001");

		WebElement pass=driver.findElement(By.id("logInPasswordInput"));
		pass.sendKeys("P@ssw0rd");
		WebElement login=driver.findElement(By.xpath("//*[@id=\"logInModalLogInButton\"]"));
		login.click();
	}


	@Test
	public void testloginAdminSuccess() {
		String adminpage=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/RuntimeTerror/Dashboard.html", adminpage);
	}


	@Test
	public void testCustomerServiceLink() {
		WebElement hyperlink=driver.findElement(By.xpath("//a[@href='#customerServiceRepModule']"));
		hyperlink.click();
		String hyperlinkPage=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/RuntimeTerror/Dashboard.html#customerServiceRepModule", hyperlinkPage);
	}
	
	@Test
	public void testsupportEngineerLink() {
		WebElement hyperlink=driver.findElement(By.xpath("//a[@href='#supportEngineerModule']"));
		hyperlink.click();
		String hyperlinkPage=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/RuntimeTerror/Dashboard.html#supportEngineerModule", hyperlinkPage);
	}
	
	@Test
	public void testnetworkManagementLink() {
		WebElement hyperlink=driver.findElement(By.xpath("//a[@href='#networkManagementModule']"));
		hyperlink.click();
		String hyperlinkPage=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/RuntimeTerror/Dashboard.html#networkManagementModule", hyperlinkPage);
	}
	
	@Test
	public void testsystemAdministratorLink() {
		WebElement hyperlink=driver.findElement(By.xpath("//a[@href='#systemAdministratorModule']"));
		hyperlink.click();
		String hyperlinkPage=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/RuntimeTerror/Dashboard.html#systemAdministratorModule", hyperlinkPage);
	}
	
	@Test
	public void testCreateUser() {

		WebElement hyperlink=driver.findElement(By.xpath("//a[@href='#systemAdministratorModule']"));
		hyperlink.click();
		
		try {
	        Thread.sleep((int) (3000));
	    } catch (InterruptedException e) {
	        e.printStackTrace();
	    }
		
		WebElement newUserModelButton=driver.findElement(By.id("createUserButton"));
		newUserModelButton.click();
		
		WebElement userId=driver.findElement(By.id("createUserUsernameInput"));
		userId.sendKeys("0000000002");

		WebElement pass=driver.findElement(By.id("createUserPasswordInput"));
		pass.sendKeys("P@ssw0rd");
		
		WebElement passConfirm=driver.findElement(By.id("createUserConfirmPasswordInput"));
		passConfirm.sendKeys("P@ssw0rd");
		
		Select se = new Select(driver.findElement(By.xpath("//*[@id=\"createUserSelect\"]")));
		se.selectByValue("Support Engineer");

		
		WebElement createUserButton=driver.findElement(By.xpath("//*[@id=\"createUserModalCreateUserButton\"]"));
		createUserButton.click();
		
		String hyperlinkPage=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/RuntimeTerror/Dashboard.html#systemAdministratorModule", hyperlinkPage);
	}
	
	
	@Test
	public void testCustomerServiceQuery() {
		WebElement hyperlink=driver.findElement(By.xpath("//a[@href='#customerServiceRepModule']"));
		hyperlink.click();
		try {
	        Thread.sleep((int) (3000));
	    } catch (InterruptedException e) {
	        e.printStackTrace();
	    }
		WebElement imsiSearchButon=driver.findElement(By.xpath("//*[@id=\"imsiSearchButton\"]"));
		imsiSearchButon.click();
		WebElement imsiSearch=driver.findElement(By.id("imsiSearchModalImsiInput"));
		imsiSearch.sendKeys("191911000456426");
		WebElement imsiSearchModalRunButton=driver.findElement(By.xpath("//*[@id=\"imsiSearchModalRunButton\"]"));
		imsiSearchModalRunButton.click();
		String hyperlinkQueryPage=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/RuntimeTerror/Dashboard.html#customerServiceRepModule", hyperlinkQueryPage);
		
		
	}
	
	@Test
	public void testTechnicalSupportQuery() {
		WebElement hyperlink=driver.findElement(By.xpath("//a[@href='#supportEngineerModule']"));
		hyperlink.click();
		try {
	        Thread.sleep((int) (3000));
	    } catch (InterruptedException e) {
	        e.printStackTrace();
	    }
		WebElement imsiSearchButon=driver.findElement(By.xpath("//*[@id=\"imsiByFailureClassButton\"]"));
		imsiSearchButon.click();
		Select FailureSearch= new Select(driver.findElement(By.id("imsiFailureClassModalInput")));
		 FailureSearch.selectByVisibleText("MT ACCESS");
		WebElement imsiFailureClassModalRunButton=driver.findElement(By.xpath("//*[@id=\"imsiFailureClassModalRunButton\"]"));
		imsiFailureClassModalRunButton.click();
		String hyperlinkQueryPage=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/RuntimeTerror/Dashboard.html#supportEngineerModule", hyperlinkQueryPage);
		
		
	}
	
	@Test
	public void testNetworkManagementQuery() {
		WebElement hyperlink=driver.findElement(By.xpath("//a[@href='#networkManagementModule']"));
		hyperlink.click();
		try {
	        Thread.sleep((int) (3000));
	    } catch (InterruptedException e) {
	        e.printStackTrace();
	    }
		WebElement phoneModelSearchButon=driver.findElement(By.xpath("//*[@id=\"PhoneModelSearchButton\"]"));
		phoneModelSearchButon.click();
		WebElement PhoneModelSearch= driver.findElement(By.id("PhoneModelSearchModalPhoneModelInput"));
		PhoneModelSearch.sendKeys("R100");
		WebElement PhoneModelSearchModalRunButton=driver.findElement(By.xpath("//*[@id=\"PhoneModelSearchModalRunButton\"]"));
		PhoneModelSearchModalRunButton.click();
		WebElement PhoneModelSearchModalGraphButton=driver.findElement(By.xpath("//*[@id=\"PhoneModelSearchModalGraphButton\"]"));
		PhoneModelSearchModalGraphButton.click();
		
		String hyperlinkQueryPage=driver.getCurrentUrl();
		assertEquals("http://localhost:8080/RuntimeTerror/Dashboard.html#networkManagementModule", hyperlinkQueryPage);
		
		
	}
	
	
	
	
	@Test
	public void testlogOutAdminSuccess() {
		String adminpage=driver.getCurrentUrl();
		WebElement logout=driver.findElement(By.xpath("//*[@id=\"homeLogOutButton\"]"));
		logout.click();
		WebElement confirmButton=driver.findElement(By.xpath("//*[@id=\"logOutConfirmationModalConfirmButton\"]"));
		confirmButton.click();
		assertEquals("http://localhost:8080/RuntimeTerror/Dashboard.html", adminpage);
	}
	
	
	
	@After
	public void teardown() {
		driver.quit();
	}

}
