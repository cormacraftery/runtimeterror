package com.test.repository;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.project.model.CallFailure;
import com.project.model.EventCause;
import com.project.model.UE;
import com.project.repository.CallFailureRepository;
import com.project.repository.EventCauseRepository;
import com.project.repository.UERepository;

public class UERepositoryTest {

	private EntityManagerFactory emf;

	@PersistenceContext
	private EntityManager em;

	UERepository ueRepository;
	UE ue;
	CallFailure callFailure;
	CallFailureRepository callFailureRepository;
	EventCause eventCause;
	EventCauseRepository eventCauseRepository;

	DBCommandTransactionalExecutor dBCommandTransactionalExecutor;

	@Before
	public void init() {
		emf = Persistence.createEntityManagerFactory("networkdb");
		em = emf.createEntityManager();
		ueRepository = new UERepository();
		ue = new UE();
		callFailure = new CallFailure();
		ueRepository.em = em;
		eventCauseRepository = new EventCauseRepository();
		eventCause = new EventCause();
		eventCauseRepository.em = em;
		callFailureRepository = new CallFailureRepository();
		callFailureRepository.em = em;
		dBCommandTransactionalExecutor = new DBCommandTransactionalExecutor(em);

		ue.setId(0);
		ue.setTac(21060800);
		ue.setMarketingName("VEA3");
		ue.setManufacturer("S.A.R.L. B  & B International");
		ue.setAccessCapability("GSM 1800, GSM 900");

		eventCause.setId(0);
		eventCause.setCauseCode("24");
		eventCause.setEventId(4098);
		eventCause.setDescription("Failure");

		callFailure.setDateTime("1/11/20 11:11");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
		callFailure.setOperator(930);
		callFailure.setCellId(4);
		callFailure.setDuration(1000);
		callFailure.setCauseCode("24");
		callFailure.setNeVersion("11B");
		callFailure.setImsi("310560000000012");
		callFailure.setHier3Id("4809532081614990300");
		callFailure.setHier32Id("8226896360947470300");
		callFailure.setHier321Id("1150444940909479940");

	}

	@After
	public void closeEntityManager() {
		em.close();
		emf.close();
	}
	
	@Test
	public void testConvertDate() {
		String dateTime = ueRepository.dateFormatter("12/01/2022","12:00");
		assertEquals("2201121200", dateTime);
	}
	@Test
	public void testSaveUE() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return ueRepository.save(ue);
		});
		List<UE> ues = ueRepository.getAllUE();
		assertEquals(1, ues.size());
		assertEquals(ue.getId(), ues.get(0).getId());
	}

	@Test
	public void testgetCellValuesForPhoneModel() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return ueRepository.save(ue);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		eventCause.setDescription("Call failure");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return eventCauseRepository.save(eventCause);
		});

		List<UE> ueList = ueRepository.getCellValuesForPhoneModel("VEA3", "24", 4098);
		assertEquals(1, ueList.size());
		List<UE> invalidUEList = ueRepository.getCellValuesForPhoneModel("ABC", "21", 1234);
		assertEquals(0, invalidUEList.size());
	}

	@Test
	public void testgetImsisForCellIds() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return ueRepository.save(ue);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		eventCause.setDescription("Call failure");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return eventCauseRepository.save(eventCause);
		});

		List<UE> ueList = ueRepository.getImsisForCellIds("VEA3", "24", 4098, 4);
		assertEquals(1, ueList.size());
		List<UE> invalidUEList = ueRepository.getImsisForCellIds("ABC", "21", 1234, 4);
		assertEquals(0, invalidUEList.size());
	}

}
