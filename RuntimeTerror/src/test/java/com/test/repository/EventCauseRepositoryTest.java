package com.test.repository;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.project.model.CallFailure;
import com.project.model.EventCause;
import com.project.repository.CallFailureRepository;
import com.project.repository.EventCauseRepository;

public class EventCauseRepositoryTest {
	
private EntityManagerFactory emf;
	
	@PersistenceContext
	private EntityManager em;
	
	EventCauseRepository eventCauseRepository;
	CallFailureRepository callFailureRepository;
	EventCause eventCause;
	CallFailure callFailure;
	DBCommandTransactionalExecutor dBCommandTransactionalExecutor;
	
	@Before
	public void init() {
		emf = Persistence.createEntityManagerFactory("networkdb");
		em = emf.createEntityManager();
		eventCauseRepository = new EventCauseRepository();
		callFailureRepository = new CallFailureRepository();
		eventCause = new EventCause();
		callFailure = new CallFailure();
		eventCauseRepository.em = em;
		callFailureRepository.em = em;
		dBCommandTransactionalExecutor = new DBCommandTransactionalExecutor(em);
		
		eventCause.setId(0);
		eventCause.setCauseCode("24");
		eventCause.setEventId(4098);
		eventCause.setDescription("Call failure");
		
		callFailure.setId(0);
		callFailure.setDateTime("1/11/20 11:11");
		callFailure.setFormattedDateTime("2002191200");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
    	callFailure.setOperator(930);
    	callFailure.setCellId(4);
    	callFailure.setDuration(1000);
    	callFailure.setCauseCode("24");
    	callFailure.setNeVersion("11B");
    	callFailure.setImsi("310560000000012");
    	callFailure.setHier3Id("4809532081614990300");
    	callFailure.setHier32Id("8226896360947470300");
    	callFailure.setHier321Id("1150444940909479940");
	}
	
	@After
	public void closeEntityManager() {
		em.close();
		emf.close();
	}
	
	@Test
	public void testSaveEventCause() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return eventCauseRepository.save(eventCause);
		});
		List<EventCause> list = eventCauseRepository.getAllEventCauses();
		assertEquals(1, list.size());
		assertEquals(eventCause.getId(), list.get(0).getId());
	}
	
	@Test
	public void testGetCallFailureIMSI() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return eventCauseRepository.save(eventCause);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<EventCause> list = eventCauseRepository.getCallFailure_IMSI(callFailure.getImsi());
		assertEquals(1, list.size());
		List<EventCause> list2 = eventCauseRepository.getCallFailure_IMSI("12345");
		assertEquals(0, list2.size());
	}

	@Test
	public void testgetCallFailure_IMSI_Unique() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return eventCauseRepository.save(eventCause);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<EventCause> list = eventCauseRepository.getCallFailure_IMSI_Unique(callFailure.getImsi());
		assertEquals(1, list.size());
		List<EventCause> list2 = eventCauseRepository.getCallFailure_IMSI_Unique("12345");
		assertEquals(0, list2.size());
	}
}
