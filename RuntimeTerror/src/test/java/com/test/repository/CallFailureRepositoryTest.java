package com.test.repository;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.project.model.CallFailure;
import com.project.model.EventCause;
import com.project.model.FailureClass;
import com.project.model.InvalidCallFailure;
import com.project.model.UE;
import com.project.repository.CallFailureRepository;
import com.project.repository.EventCauseRepository;
import com.project.repository.FailureClassRepository;
import com.project.repository.UERepository;

public class CallFailureRepositoryTest {

	private EntityManagerFactory emf;

	@PersistenceContext
	private EntityManager em;

	CallFailureRepository callFailureRepository;
	FailureClassRepository failureClassRepository;
	CallFailure callFailure;
	DBCommandTransactionalExecutor dBCommandTransactionalExecutor;

	UERepository ueRepository;
	EventCause eventCause;
	EventCauseRepository eventCauseRepository;
	FailureClass failureClass;

	@Before
	public void init() {
		emf = Persistence.createEntityManagerFactory("networkdb");
		em = emf.createEntityManager();
		callFailureRepository = new CallFailureRepository();
		failureClassRepository = new FailureClassRepository();
		ueRepository = new UERepository();
		eventCause = new EventCause();
		eventCauseRepository = new EventCauseRepository();
		eventCauseRepository.em = em;
		callFailureRepository.em = em;
		failureClassRepository.em = em;
		ueRepository.em = em;
		dBCommandTransactionalExecutor = new DBCommandTransactionalExecutor(em);

		failureClass = new FailureClass();
		failureClass.setFailureClass("1");
		failureClass.setDescription("Call failed");

		eventCause.setCauseCode("24");
		eventCause.setEventId(4098);
		eventCause.setDescription("Call failure");

		callFailure = new CallFailure();
		callFailure.setDateTime("2/19/20 12:00");
		callFailure.setFormattedDateTime("2002191200");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
		callFailure.setOperator(930);
		callFailure.setCellId(4);
		callFailure.setDuration(1000);
		callFailure.setCauseCode("24");
		callFailure.setNeVersion("11B");
		callFailure.setImsi("310560000000012");
		callFailure.setHier3Id("4809532081614990300");
		callFailure.setHier32Id("8226896360947470300");
		callFailure.setHier321Id("1150444940909479940");
	}

	@After
	public void closeEntityManager() {
		em.close();
		emf.close();
	}

	@Test
	public void testSaveEventCause() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return eventCauseRepository.save(eventCause);
		});
		List<EventCause> list = eventCauseRepository.getAllEventCauses();
		assertEquals(1, list.size());
	}

	@Test
	public void testSaveFailureClass() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return failureClassRepository.save(failureClass);
		});
		List<FailureClass> list = failureClassRepository.getAllFailureClasses();
		assertEquals(1, list.size());
	}

	@Test
	public void testSaveCallFailure() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<CallFailure> list = callFailureRepository.getAllCallFailures();
		assertEquals(1, list.size());
	}

	@Test
	public void testSaveInvalidCallFailure() {
		InvalidCallFailure invalidCallFailure = new InvalidCallFailure();
		invalidCallFailure.setDateTime("1/11/20 11:11");
		invalidCallFailure.setEventId(4098);
		invalidCallFailure.setFailureClass("1");
		invalidCallFailure.setUeType(21060800);
		invalidCallFailure.setMarket(344);
		invalidCallFailure.setOperator(930);
		invalidCallFailure.setCellId(4);
		invalidCallFailure.setDuration(1000);
		invalidCallFailure.setCauseCode("24");
		invalidCallFailure.setNeVersion("11B");
		invalidCallFailure.setImsi("310560000000012");
		invalidCallFailure.setHier3Id("4809532081614990300");
		invalidCallFailure.setHier32Id("8226896360947470300");
		invalidCallFailure.setHier321Id("1150444940909479940");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(invalidCallFailure).getCauseCode();
		});
		List<InvalidCallFailure> list = callFailureRepository.getInvalidCallFailures();
		assertEquals(1, list.size());
		assertEquals(invalidCallFailure.getDateTime(), list.get(0).getDateTime());
		assertEquals(invalidCallFailure.getCellId(), list.get(0).getCellId());
		assertEquals(invalidCallFailure.getMarket(), list.get(0).getMarket());
		assertEquals(invalidCallFailure.getImsi(), list.get(0).getImsi());
		assertEquals(invalidCallFailure.getHier3Id(), list.get(0).getHier3Id());
		assertEquals(invalidCallFailure.getHier32Id(), list.get(0).getHier32Id());
		assertEquals(invalidCallFailure.getHier321Id(), list.get(0).getHier321Id());
	}

	@Test
	public void testGetInvalidImsiCountWithinDate() {

		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<CallFailure> callFailureList = callFailureRepository.getImsiCountWithinDate("2020/01/11", "11:10",
				"2020/01/11", "11:13", "310560000000012");

		assertEquals(0, callFailureList.size());
	}

	@Test
	public void testGetInvalidModelCountWithinDate() {

		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<CallFailure> callFailureList = callFailureRepository.getModelCountWithinDate("2020/01/11", "11:10",
				"2020/01/11", "11:13", "21060800");

		assertEquals(0, callFailureList.size());
	}

	@Test
	public void testDateRange() {
		FailureClass failureClass = new FailureClass();
		failureClass.setFailureClass("1");
		failureClass.setDescription("Call failed");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return failureClassRepository.save(failureClass);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
	}

	@Test
	public void testValidGetImsiCountWithinDate() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<CallFailure> callFailureList = callFailureRepository.getImsiCountWithinDate("18/02/2020", "11:10",
				"22/02/2020", "12:13", "310560000000012");
		assertEquals(1, callFailureList.size());
	}

	@Test
	public void testValidTop10Query() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<CallFailure> callFailureList = callFailureRepository.getTop10ModelOperatorCellId("18/02/2020", "11:10",
				"22/02/2020", "12:13");
		assertEquals(1, callFailureList.size());
	}

	@Test
	public void testInValidTop10Query() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<CallFailure> callFailureList = callFailureRepository.getTop10ModelOperatorCellId("18/02/2020", "11:10",
				"18/02/2020", "12:10");
		assertEquals(0, callFailureList.size());
	}
//
	@Test
	public void testValidTop10ModelDrillDown() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			
			return failureClassRepository.save(failureClass);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<CallFailure> callFailureList = callFailureRepository.top10ModelDrillDown("18/02/2020", "11:10",
				"22/02/2020", "12:13", 344, 930, 4);
		assertEquals(1, callFailureList.size());
	}

	@Test
	public void testValidtop10ImsiDrillDown() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<CallFailure> callFailureList = callFailureRepository.top10ImsiDrillDown("18/02/2020", "11:10",
				"22/02/2020", "12:13", "310560000000012");
		
		assertEquals(1, callFailureList.size());
	}
	

	@Test
	public void testValidtop10ModelSecondDrillDown() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			
			return failureClassRepository.save(failureClass);
		    });
			dBCommandTransactionalExecutor.executeCommand(() -> {
				return callFailureRepository.save(callFailure);
			});
			dBCommandTransactionalExecutor.executeCommand(() -> {
				return eventCauseRepository.save(eventCause);
			});
		
		List<CallFailure> callFailureList = callFailureRepository.top10ModelSecondDrillDown("18/02/2020", "11:10",
				"22/02/2020", "12:13", 344, 930, 4, "Call failed");
		assertEquals(1, callFailureList.size());
	}

	@Test
	public void testGetInvalidIMSICallFailuresWithinTimePeriod() {

		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<CallFailure> callFailureList = callFailureRepository.getIMSICallFailuresWithinTimePeriod("2020/01/11",
				"11:10", "2020/01/11", "11:13");

		assertEquals(0, callFailureList.size());
	}

	@Test
	public void test2() {
		UE ue = new UE();
		ue.setAccessCapability("1");
		ue.setManufacturer("2");
		ue.setMarketingName("123");
		ue.setTac(21060800);
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return ueRepository.save(ue);
		});
		List<CallFailure> callFailureList = callFailureRepository.getModelCountWithinDate("18/02/2020", "11:10",
				"22/02/2020", "11:13", "123");
		assertEquals(1, callFailureList.size());
	}

	@Test
	public void testValidgetTop10ImsiCallfailuresWithinDateQuery() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});

		List<CallFailure> callFailureList = callFailureRepository.getTop10ImsiCallfailuresWithinDate("18/02/2020",
				"11:10", "22/02/2020", "11:13");

		assertEquals(1, callFailureList.size());
	}

	@Test
	public void testInValidgetTop10ImsiCallfailuresWithinDateQuery() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<CallFailure> callFailureList = callFailureRepository.getTop10ImsiCallfailuresWithinDate("18/02/2020",
				"11:10", "18/02/2020", "11:10");
		assertEquals(0, callFailureList.size());
	}

	@Test
	public void testValidtop10ImsiDrillDownQuery() {

		dBCommandTransactionalExecutor.executeCommand(() -> {
			
			return failureClassRepository.save(failureClass);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		
		List<CallFailure> callFailureList= callFailureRepository.top10ImsiDrillDown("18/02/2020", "11:10","22/02/2020", "11:13", "310560000000012");
				
		assertEquals(1, callFailureList.size());
	}

	@Test
	public void testgetIMSIFailureCauseQuery() {
		dBCommandTransactionalExecutor.executeCommand(() -> {

			return failureClassRepository.save(failureClass);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});

		List<CallFailure> callFailureList = callFailureRepository.getIMSIFailureCause("1");

		assertEquals(1, callFailureList.size());
	}

}