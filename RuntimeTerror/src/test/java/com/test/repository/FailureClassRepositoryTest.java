package com.test.repository;

import static org.junit.Assert.*;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.project.model.CallFailure;
import com.project.model.EventCause;
import com.project.model.FailureClass;
import com.project.repository.CallFailureRepository;
import com.project.repository.EventCauseRepository;
import com.project.repository.FailureClassRepository;

public class FailureClassRepositoryTest {
	
private EntityManagerFactory emf;
	
	@PersistenceContext
	private EntityManager em;

    FailureClassRepository failureClassRepository;
    FailureClass failureClass;
    CallFailureRepository callFailureRepository;
    EventCause eventCause;
    CallFailure callFailure;
	EventCauseRepository eventCauseRepository;
	DBCommandTransactionalExecutor dBCommandTransactionalExecutor;
	
	@Before
	public void init() {
		emf = Persistence.createEntityManagerFactory("networkdb");
		em = emf.createEntityManager();
		failureClassRepository = new FailureClassRepository();
		callFailureRepository = new CallFailureRepository();
		failureClass = new FailureClass();
		eventCause = new EventCause();
		eventCauseRepository = new EventCauseRepository();
		eventCauseRepository.em = em;
		failureClassRepository.em = em;
		callFailureRepository.em = em;
		dBCommandTransactionalExecutor = new DBCommandTransactionalExecutor(em);
		
		failureClass.setId(0);
		failureClass.setFailureClass("1");
		failureClass.setDescription("Call failed");
		
		eventCause.setCauseCode("24");
		eventCause.setEventId(4098);
		eventCause.setDescription("Call failure");
		
		 callFailure = new CallFailure();
		callFailure.setDateTime("2/19/20 12:00");
		callFailure.setFormattedDateTime("2002191200");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
		callFailure.setOperator(930);
		callFailure.setCellId(4);
		callFailure.setDuration(1000);
		callFailure.setCauseCode("24");
		callFailure.setNeVersion("11B");
		callFailure.setImsi("310560000000012");
		callFailure.setHier3Id("4809532081614990300");
		callFailure.setHier32Id("8226896360947470300");
		callFailure.setHier321Id("1150444940909479940");
	}
	
	@After
	public void closeEntityManager() {
		em.close();
		emf.close();
	}
	
	@Test
	public void testSaveFailureClass() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return failureClassRepository.save(failureClass);
		});
		List<FailureClass> failureClasses = failureClassRepository.getAllFailureClasses();
		assertEquals(1, failureClasses.size());
		assertEquals(failureClass.getId(), failureClasses.get(0).getId());
	}
	
	@Test
	public void testSaveEventCause() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return eventCauseRepository.save(eventCause);
		});
		List<EventCause> list = eventCauseRepository.getAllEventCauses();
		assertEquals(1, list.size());
	}
	
	@Test
	public void testSaveCallFailure() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<CallFailure> list = callFailureRepository.getAllCallFailures();
		assertEquals(1, list.size());
	}
	
	
	
	@Test
	public void testGetInvalidIMSICountAndDurationCallFailuresWithinTimePeriod() {
		CallFailure callFailure = new CallFailure();
		callFailure.setDateTime("2/19/20 12:00");
		callFailure.setFormattedDateTime("2002191200");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
		callFailure.setOperator(930);
		callFailure.setCellId(4);
		callFailure.setDuration(1000);
		callFailure.setCauseCode("24");
		callFailure.setNeVersion("11B");
		callFailure.setImsi("310560000000012");
		callFailure.setHier3Id("4809532081614990300");
		callFailure.setHier32Id("8226896360947470300");
		callFailure.setHier321Id("1150444940909479940");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return failureClassRepository.save(failureClass);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<FailureClass> failureClasses = failureClassRepository.getIMSICountAndDurationCallFailuresWithinTimePeriod("18/02/2020", "11:10","21/02/2020", "11:13");
		assertEquals(1, failureClasses.size());
	}
	
	@Test
	public void testcellDrillDown() {		

		dBCommandTransactionalExecutor.executeCommand(() -> {
			return failureClassRepository.save(failureClass);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<FailureClass> failureClasses = failureClassRepository.cellDrillDown("18/02/2020", "11:10","21/02/2020", "11:13",4);
		assertEquals(1, failureClasses.size());
	}
	
	@Test
	public void testImsiDrillDown() {		
		CallFailure callFailure = new CallFailure();
		callFailure.setDateTime("2/19/20 12:00");
		callFailure.setFormattedDateTime("2002191200");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
		callFailure.setOperator(930);
		callFailure.setCellId(4);
		callFailure.setDuration(1000);
		callFailure.setCauseCode("24");
		callFailure.setNeVersion("11B");
		callFailure.setImsi("310560000000012");
		callFailure.setHier3Id("4809532081614990300");
		callFailure.setHier32Id("8226896360947470300");
		callFailure.setHier321Id("1150444940909479940");
		
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return failureClassRepository.save(failureClass);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return eventCauseRepository.save(eventCause);
		});
		List<FailureClass> failureClasses = failureClassRepository.imsiDrillDown("18/02/2020", "11:10",
				"22/02/2020", "12:13",4, "Call failed","Call failure");
		assertEquals(1, failureClasses.size());
	}
	
	
	@Test
	public void testValidgetIMSICountAndDurationCallFailuresWithinTimePeriodGrouped() {
		
		CallFailure callFailure = new CallFailure();
		callFailure.setDateTime("2/19/20 12:00");
		callFailure.setFormattedDateTime("2002191200");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
		callFailure.setOperator(930);
		callFailure.setCellId(4);
		callFailure.setDuration(1000);
		callFailure.setCauseCode("24");
		callFailure.setNeVersion("11B");
		callFailure.setImsi("310560000000012");
		callFailure.setHier3Id("4809532081614990300");
		callFailure.setHier32Id("8226896360947470300");
		callFailure.setHier321Id("1150444940909479940");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return failureClassRepository.save(failureClass);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		List<FailureClass> failureClasses = failureClassRepository.getIMSICountAndDurationCallFailuresWithinTimePeriodGrouped("18/02/2020", "11:10",
				"22/02/2020", "12:13");
		assertEquals(1, failureClasses.size());
	}
	
	@Test
	public void testfailureClassDrillDown() {
		
		CallFailure callFailure = new CallFailure();
		callFailure.setDateTime("2/19/20 12:00");
		callFailure.setFormattedDateTime("2002191200");
		callFailure.setEventId(4098);
		callFailure.setFailureClass("1");
		callFailure.setUeType(21060800);
		callFailure.setMarket(344);
		callFailure.setOperator(930);
		callFailure.setCellId(4);
		callFailure.setDuration(1000);
		callFailure.setCauseCode("24");
		callFailure.setNeVersion("11B");
		callFailure.setImsi("310560000000012");
		callFailure.setHier3Id("4809532081614990300");
		callFailure.setHier32Id("8226896360947470300");
		callFailure.setHier321Id("1150444940909479940");
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return failureClassRepository.save(failureClass);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return callFailureRepository.save(callFailure);
		});
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return eventCauseRepository.save(eventCause);
		});
		List<FailureClass> failureClasses = failureClassRepository.failureClassDrillDown("18/02/2020", "11:10",
				"22/02/2020", "12:13", 4, "Call failed" );
		assertEquals(1, failureClasses.size());
	}
	
	

}
