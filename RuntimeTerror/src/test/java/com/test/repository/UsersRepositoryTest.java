package com.test.repository;

import static org.junit.Assert.*;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.project.model.Users;
import com.project.repository.UsersRepository;

public class UsersRepositoryTest {
	
	private EntityManagerFactory emf;
	
	@PersistenceContext
	private EntityManager em;

	Users user;
    UsersRepository usersRepository;
	DBCommandTransactionalExecutor dBCommandTransactionalExecutor;
	
	@Before
	public void init() {
		emf = Persistence.createEntityManagerFactory("networkdb");
		em = emf.createEntityManager();
		usersRepository = new UsersRepository();
		usersRepository.em = em;
		dBCommandTransactionalExecutor = new DBCommandTransactionalExecutor(em);
		
		user = new Users();
		user.setId(0);
		user.setUserName("1234567899");
		user.setUserPassword("P@ssw0rd");
		user.setUserType("Admin");
	}
	
	@After
	public void closeEntityManager() {
		em.close();
		emf.close();
	}
	
	@Test
	public void testSaveUsers() {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return usersRepository.save(user);
		});
		List<Users> list = usersRepository.getAllUsers();
		assertEquals(1, list.size());
		Users users2 = usersRepository.getUsers(user.getId());
		assertEquals(user.getUserName(), users2.getUserName());
	}

	@Test
	public void addUserAndFindIt() throws NoSuchAlgorithmException {
		dBCommandTransactionalExecutor.executeCommand(() -> {
			return usersRepository.save(user);
		});
		List<Users> list = usersRepository.getSpecificUser(user.getUserName(), "P@ssw0rd");
		assertEquals(1, list.size());
	}
	
	@Test
	public void testPasswordHashing() {
		String hashedPassword = usersRepository.generatePasswordHash("Jack@123");
		assertEquals("KUCa87a3ymc//WtEB3QeCVx0GqKNLFXGVEnkYMBRwB0=", hashedPassword);
	}

}
