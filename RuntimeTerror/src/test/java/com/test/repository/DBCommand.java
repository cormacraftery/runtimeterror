package com.test.repository;

import org.junit.Ignore;

@Ignore
public interface DBCommand<T> {

	T execute();

}