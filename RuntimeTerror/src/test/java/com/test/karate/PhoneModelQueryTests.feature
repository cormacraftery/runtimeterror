Feature: Phone Model Query

Scenario: Querying a valid phone model
Given url 'http://localhost:8080/RuntimeTerror/rest/ues/phonemodel/9109%20PA'
When method GET
Then status 200

Scenario: Querying an invalid phone model
Given url 'http://localhost:8080/RuntimeTerror/rest/ues/phonemodel/9108%20PA'
When method GET
Then status 400

Scenario: First drill down for a valid phone model
Given url 'http://localhost:8080/RuntimeTerror/rest/ues/phonemodel/9109%20PA/22/4106'
When method GET
Then status 200

Scenario: Second drill down for a valid phone model
Given url 'http://localhost:8080/RuntimeTerror/rest/ues/phonemodel/9109%20PA/22/4106/1'
When method GET
Then status 200