Feature: Assessing call failure details for a given IMSI

Scenario: Non-unique details query (valid)
Given url 'http://localhost:8080/RuntimeTerror/rest/eventcause/imsi/191911000456426'
When method GET
Then status 200

Scenario: Unique details query (valid)
Given url 'http://localhost:8080/RuntimeTerror/rest/eventcause/imsiUnique/191911000312238'
When method GET
Then status 200
