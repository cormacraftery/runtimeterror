Feature: Call failures within date range query

Scenario: Querying a valid date range
Given url 'http://localhost:8080/RuntimeTerror/rest/callfailures/callfailuredates/19-02-2020/12:00/21-02-2020/12:00'
When method GET
Then status 200

Scenario: Querying an invalid date range
Given url 'http://localhost:8080/RuntimeTerror/rest/callfailures/callfailuredates/19-02-2000/12:00/21-02-2001/12:00'
When method GET
Then status 400