Feature: Top 10 call failure model tests within a date range

Scenario: Querying a valid date range
Given url 'http://localhost:8080/RuntimeTerror/rest/callfailures/Top10Model/20-02-2020/12:00/21-02-2020/13:00'
When method GET
Then status 200

Scenario: Querying an invalid date range
Given url 'http://localhost:8080/RuntimeTerror/rest/callfailures/Top10Model/21-02-2000/12:00/21-02-2001/12:00'
When method GET
Then status 400

Scenario: First drill down
Given url 'http://localhost:8080/RuntimeTerror/rest/callfailures/Top10Model/20-02-2020/12:00/21-02-2020/13:00/505/71/3'
When method GET
Then status 200

Scenario: Second drill down
Given url 'http://localhost:8080/RuntimeTerror/rest/callfailures/Top10Model/20-02-2020/12:00/21-02-2020/13:00/505/71/3/EMERGENCY'
When method GET
Then status 200
