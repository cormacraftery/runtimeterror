Feature: Assessing number of call failures and their duration within each IMSI query

Scenario: Assessing a valid date range for the table query
Given url 'http://localhost:8080/RuntimeTerror/rest/failureclass/imsicallfailurescount/20-02-2020/12:00/21-02-2020/12:00'
When method GET
Then status 200

Scenario: Assessing an invalid date range for the table query
Given url 'http://localhost:8080/RuntimeTerror/rest/failureclass/imsicallfailurescount/20-02-2000/12:00/21-02-2001/12:00'
When method GET
Then status 400

Scenario: Assessing a valid date range for the graph query
Given url 'http://localhost:8080/RuntimeTerror/rest/failureclass/imsicallfailuresgrouped/20-02-2020/12:00/21-02-2020/12:00'
When method GET
Then status 200

Scenario: Assessing an invalid date range for the graph query
Given url 'http://localhost:8080/RuntimeTerror/rest/failureclass/imsicallfailuresgrouped/20-02-2000/12:00/21-02-2001/12:00'
When method GET
Then status 400

Scenario: Assessing first drill down for the graph query
Given url 'http://localhost:8080/RuntimeTerror/rest/failureclass/imsicallfailuresgrouped/20-02-2020/12:00/21-02-2020/12:00/1'
When method GET
Then status 200

Scenario: Assessing second drill down for the graph query
Given url 'http://localhost:8080/RuntimeTerror/rest/failureclass/imsicallfailuresgrouped/20-02-2020/12:00/21-02-2020/12:00/1/EMERGENCY'
When method GET
Then status 200

Scenario: Assessing third drill down for the graph query
Given url 'http://localhost:8080/RuntimeTerror/rest/failureclass/imsicallfailuresgrouped/20-02-2020/12:00/21-02-2020/12:00/1/EMERGENCY/S1%20SIG%20CONN%20SETUP-S1%20INTERFACE%20DOWN'
When method GET
Then status 200
