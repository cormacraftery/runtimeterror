package com.test.dataconverter;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Before;
import org.junit.Test;

import com.project.dataconverter.CallFailureConverter;
import com.project.dataconverter.UEConverter;
import com.project.model.CallFailure;
import com.project.model.UE;


public class UEConverterTest {
	
	UEConverter ueConverter;
	UE ue;
	
	@Before
	public void init() {
		ueConverter = new UEConverter();
		ue = new UE();
	}
	
	@Test
	public void testConvertRowToUE() throws FileNotFoundException, IOException {
		Workbook wb = new HSSFWorkbook();  
		Sheet sheet = wb.createSheet("New Sheet");  
        Row row = sheet.createRow(1);  
        
        Cell tac = row.createCell(0);
        tac.setCellValue(ue.getTac());
        Cell marketingName = row.createCell(1);
        marketingName.setCellValue(ue.getMarketingName());
        Cell manufacturer = row.createCell(2);
        manufacturer.setCellValue(ue.getManufacturer());
        Cell accessCapability = row.createCell(3);
        accessCapability.setCellValue(ue.getAccessCapability());
        UE ue2 = ueConverter.convertRowToUE(row);
        assertEquals(UE.class, ue2.getClass());
	}
}
