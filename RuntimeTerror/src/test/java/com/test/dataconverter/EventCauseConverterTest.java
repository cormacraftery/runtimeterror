package com.test.dataconverter;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Before;
import org.junit.Test;

import com.project.dataconverter.EventCauseConverter;
import com.project.model.EventCause;

public class EventCauseConverterTest {
	
	EventCauseConverter eventCauseConverter;
	EventCause eventCause;
	
	@Before
	public void init() {
		eventCauseConverter = new EventCauseConverter();
		eventCause = new EventCause();
	}
	
	
	@Test
	public void testConvertRowToEventCause() {
		eventCause.setCauseCode("123");
		eventCause.setEventId(123);
		eventCause.setDescription("Call failure");
		Workbook wb = new HSSFWorkbook();  
		Sheet sheet = wb.createSheet("New Sheet");  
        Row row = sheet.createRow(1);
        Cell causeCodeCell = row.createCell(0);
        causeCodeCell.setCellValue(eventCause.getCauseCode());
        Cell eventIdCell = row.createCell(1);
        eventIdCell.setCellValue(eventCause.getEventId());
        Cell descriptionCell = row.createCell(2);
        descriptionCell.setCellValue(eventCause.getDescription());
        EventCause eventCause2 = eventCauseConverter.convertRowToEventCause(row);
        assertEquals(eventCause2.getClass(), EventCause.class);
	}

}
