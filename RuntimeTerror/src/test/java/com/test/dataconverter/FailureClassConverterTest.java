package com.test.dataconverter;

import static org.junit.Assert.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Before;
import org.junit.Test;

import com.project.dataconverter.FailureClassConverter;
import com.project.model.FailureClass;

public class FailureClassConverterTest {
	
	FailureClassConverter failureClassConverter;
	FailureClass failureClass;
	
	@Before
	public void init() {
		failureClassConverter = new FailureClassConverter();
		failureClass = new FailureClass();
	}
	
	@Test
	public void testConvertRowToFailureClass() {
		failureClass.setFailureClass("2");
		failureClass.setDescription("Call fail");
		Workbook wb = new HSSFWorkbook();  
		Sheet sheet = wb.createSheet("New Sheet");  
        Row row = sheet.createRow(1);
        Cell failureClassCell = row.createCell(0);
        failureClassCell.setCellValue(failureClass.getFailureClass());
        Cell descriptionCell = row.createCell(1);
        descriptionCell.setCellValue(failureClass.getDescription());
        FailureClass failureClass2 = failureClassConverter.convertRowToFailureClass(row);
        assertEquals(FailureClass.class, failureClass2.getClass());
	}

}
