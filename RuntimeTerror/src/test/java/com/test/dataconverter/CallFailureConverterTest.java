package com.test.dataconverter;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Before;
import org.junit.Test;

import com.project.dataconverter.CallFailureConverter;
import com.project.dataconverter.EventCauseConverter;
import com.project.dataconverter.FailureClassConverter;
import com.project.dataconverter.FileManager;
import com.project.dataconverter.UEConverter;
import com.project.model.CallFailure;
import com.project.model.EventCause;
import com.project.model.FailureClass;
import com.project.model.UE;

public class CallFailureConverterTest {

	CallFailureConverter callFailureConverter;
	CallFailure callFailure;
	FailureClassConverter failureClassConverter;
	FailureClass failureClass;
	EventCauseConverter eventCauseConverter;
	EventCause eventCause;
	UEConverter ueConverter;
	UE ue;
	FileManager fileManager = new FileManager();

	@Before
	public void init() {
		callFailureConverter = new CallFailureConverter();
		callFailure = new CallFailure();
		failureClassConverter = new FailureClassConverter();
		failureClass = new FailureClass();
		eventCauseConverter = new EventCauseConverter();
		eventCause = new EventCause();
		ueConverter = new UEConverter();
		ue = new UE();
	}

	@Test
	public void testConvertListToCallFailures() throws FileNotFoundException, IOException {

		fileManager.moveFile("AIT Group Project 2022- Dataset 3A.xls", "imported-data", "data");
		assertEquals(ArrayList.class, eventCauseConverter.convertEventCauseToList().getClass());
		assertEquals(ArrayList.class, failureClassConverter.convertFailureClassesToList().getClass());
		assertEquals(ArrayList.class, ueConverter.convertUEToList().getClass());
		assertEquals(ArrayList.class, callFailureConverter.convertCallFailuresToList().getClass());

	}

	@Test
	public void testConvertRowToCallFailure() throws FileNotFoundException, IOException {
		Workbook wb = new HSSFWorkbook();
		Sheet sheet = wb.createSheet("New Sheet");
		Row row = sheet.createRow(1);
		Cell dateTime = row.createCell(0);
		dateTime.setCellValue(callFailure.getDateTime());
		Cell eventId = row.createCell(1);
		eventId.setCellValue(callFailure.getEventId());
		Cell failureClass = row.createCell(2);
		failureClass.setCellValue(callFailure.getFailureClass());
		Cell ueType = row.createCell(3);
		ueType.setCellValue(callFailure.getUeType());
		Cell market = row.createCell(4);
		market.setCellValue(callFailure.getMarket());
		Cell operator = row.createCell(5);
		operator.setCellValue(callFailure.getOperator());
		Cell cellId = row.createCell(6);
		cellId.setCellValue(callFailure.getCellId());
		Cell duration = row.createCell(7);
		duration.setCellValue(callFailure.getDuration());
		Cell causeCode = row.createCell(8);
		causeCode.setCellValue(callFailure.getCauseCode());
		Cell neVersion = row.createCell(9);
		neVersion.setCellValue(callFailure.getNeVersion());
		Cell imsi = row.createCell(10);
		imsi.setCellValue(callFailure.getImsi());
		Cell hier3Id = row.createCell(11);
		hier3Id.setCellValue(callFailure.getHier3Id());
		Cell hier32Id = row.createCell(12);
		hier32Id.setCellValue(callFailure.getHier32Id());
		Cell hier321Id = row.createCell(13);
		hier321Id.setCellValue(callFailure.getHier321Id());
		
		//CallFailure callFailure2 = callFailureConverter.convertRowToCallFailure(row);
		//assertEquals(CallFailure.class, callFailure2.getClass());
	}

}
