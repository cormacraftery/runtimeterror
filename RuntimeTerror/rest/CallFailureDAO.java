package com.project.rest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

@Stateless
@LocalBean
public class CallFailureDAO {

	@PersistenceContext
	private EntityManager em;

	public List<CallFailure> getAllCallFailures() {
		Query query = em.createQuery("SELECT w FROM CallFailure w");
		List<CallFailure> totalList = query.getResultList();
		return totalList;
	}

	public List<CallFailure> getInvalidCallFailures() {
		List<CallFailure> totalList = getAllCallFailures();
		List<CallFailure> invalidList = new ArrayList<CallFailure>();
		for (CallFailure callFailure : totalList) {
			if (callFailure.getCauseCode().equals("(null)") || callFailure.getFailureClass().equals("(null)")
					|| Integer.parseInt(callFailure.getFailureClass()) > 4) {
				invalidList.add(callFailure);
			}
		}
		return invalidList; // More validation conditions are required
	}

	public List<CallFailure> getCallFailure_IMSI(float imsi) {
		Query query2 = em.createQuery("SELECT i.eventId, i.causeCode FROM CallFailure AS i " + "WHERE i.imsi =:imsi");
		query2.setParameter("imsi", imsi);
		return query2.getResultList();
	}

	// To list all IMSI's with callfailures within timeperiod
	public List<CallFailure> getIMSICallFailuresWithinTimePeriod(String fromDate, String toDate) {
		Date date = new Date();
		Date date2 = new Date();
		//date.setDate("12-09-2021");
		date.parse("12-09-2021");
		date2.parse("12-09-2021");
		Query query1 = em.createQuery("SELECT CF.dateTime,CF.imsi,CF.failureClass\n" + "FROM CallFailure CF \n"
				+ "WHERE CF.dateTime BETWEEN :fromDate AND :toDate \n"
				+ "GROUP BY CF.dateTime,CF.imsi,CF.failureClass\n" + "ORDER BY CF.dateTime,CF.imsi,CF.failureClass");

		query1.setParameter("fromDate", fromDate);
		query1.setParameter("toDate", toDate);
		return query1.getResultList();
	}

	public List<CallFailure> getValidCallFailures() {
		List<CallFailure> totalList = getAllCallFailures();
		totalList.removeAll(getInvalidCallFailures());
		return totalList;
	}

	public List<CallFailure> getImsiEntered(String imsi) {
		List<CallFailure> validData = getValidCallFailures();
		List<CallFailure> searchedList = new ArrayList<CallFailure>();
		for (CallFailure callFailure : validData) {
			if (callFailure.getImsi().equals(imsi)) {
				searchedList.add(callFailure);
			}
		}
		return searchedList;
	}

	public CallFailure getCallFailure(int id) {
		return em.find(CallFailure.class, id);
	}

	public void save(CallFailure callFailure) {
		em.persist(callFailure);
	}
	
	// C:\\Users\\35385\\Downloads\\BaseData.xlsx
	// C:\Users\35385\Downloads\NewProjectRepo\runtimeterror\RuntimeTerror\src\main\webapp\data\BaseData.xlsx
	public void loadData() throws IOException {
		FileInputStream inputStream = new FileInputStream("C:\\Users\\35385\\Downloads\\BaseData.xlsx");
		Workbook workbook = new XSSFWorkbook(inputStream);
        Sheet firstSheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = firstSheet.iterator();
        rowIterator.next();
        while (rowIterator.hasNext()) {
            Row nextRow = rowIterator.next();
            Iterator<Cell> cellIterator = nextRow.cellIterator();
            DataFormatter formatter = new DataFormatter();
            CallFailure callFailure = new CallFailure();
            while (cellIterator.hasNext()) {
                Cell nextCell = cellIterator.next();
                int columnIndex = nextCell.getColumnIndex();
                switch (columnIndex) {
                case 0:
                    callFailure.setDateTime(formatter.formatCellValue(nextCell));
                    break;
                case 1:
                	callFailure.setEventId((int) nextCell.getNumericCellValue());
                    break;
                case 2:
                	callFailure.setFailureClass(formatter.formatCellValue(nextCell));
                    break;
                case 3:
                	callFailure.setUeType((int) nextCell.getNumericCellValue());
                    break;
                case 4:
                	callFailure.setMarket((int) nextCell.getNumericCellValue());
                    break;
                case 5:
                	callFailure.setOperator((int) nextCell.getNumericCellValue());
                    break;
                case 6:
                	callFailure.setCellId((int) nextCell.getNumericCellValue());
                    break;
                case 7:
                	callFailure.setDuration((int) nextCell.getNumericCellValue());
                    break;
                case 8:
                	callFailure.setCauseCode(formatter.formatCellValue(nextCell));
                    break;
                case 9:
                	callFailure.setNeVersion(formatter.formatCellValue(nextCell));
                    break;
                case 10:
                	callFailure.setImsi(formatter.formatCellValue(nextCell));
                    break;
                case 11:
                	callFailure.setHier3Id(formatter.formatCellValue(nextCell));
                    break;
                case 12:
                	callFailure.setHier32Id(formatter.formatCellValue(nextCell));
                    break;
                case 13:
                	callFailure.setHier321Id(formatter.formatCellValue(nextCell));
                    break;
                }
            }
            if(verifyCallFailure(callFailure)) {
            	save(callFailure);
            }
        }
	}
	
	public static boolean verifyCallFailure(CallFailure callFailure) {
		boolean callFailureIsValid = true;
		if(callFailure.getEventId()==4099 || callFailure.getCauseCode() == "(null)"
			|| callFailure.getFailureClass() == "(null)" || Integer.parseInt(callFailure.getFailureClass()) > 4
			|| callFailure.getUeType() == 21060810 || callFailure.getUeType() == 33000255 || callFailure.getUeType() == 33000257
			|| (callFailure.getMarket() == 344 && callFailure.getOperator() == 935)
			|| (callFailure.getMarket() == 345 && callFailure.getOperator() == 930)
			|| (callFailure.getMarket() == 355 && callFailure.getOperator() == 930)) {
			callFailureIsValid = false;
		}
		return callFailureIsValid;
	}
	
}
