package com.project.rest;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/users")
@Stateless
@LocalBean
public class UsersWS {

	@EJB
	private UsersDAO usersDAO;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/valid")
	public Response findValidUsers() {
		List<Users> users = usersDAO.getValidUsers();
		return Response.status(200).entity(users).build();
	}
	
	@POST
	@Path("/valid")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Response saveWine(Users user) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(user.getUserPassword().getBytes(StandardCharsets.UTF_8));
		user.setUserPassword(Base64.getEncoder().encodeToString(hash));
		usersDAO.save(user);
		return Response.status(201).entity(user).build();
	}
	
	@GET
	@Path("/search/{userName}/{password}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findUser(@PathParam("userName") String userName, @PathParam("password") String password) throws NoSuchAlgorithmException {
		List<Users> wines=usersDAO.getSpecificUser(userName, password);
		return Response.status(200).entity(wines).build();
	}
	
}
