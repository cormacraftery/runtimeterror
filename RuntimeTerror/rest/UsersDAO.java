package com.project.rest;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

@Stateless
@LocalBean
public class UsersDAO {

	@PersistenceContext
	private EntityManager em;
	
	public List<Users> getAllUsers() {
		Query query = em.createQuery("SELECT w FROM Users w");
		List<Users> totalList = query.getResultList();
		return totalList;
	}
	
	public List<Users> getValidUsers() {
		List<Users> totalList = getAllUsers();
		//totalList.removeAll(getInvalidCallFailures());
		return totalList;
	}
	
	public void save(Users user) {
		em.persist(user);
	}
	
	public List<Users> getSpecificUser(String userName, String password) throws NoSuchAlgorithmException {
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] hash = digest.digest(password.getBytes(StandardCharsets.UTF_8));
		String hashedPassword = Base64.getEncoder().encodeToString(hash);
    	Query query=em.createQuery("SELECT w FROM Users AS w "+
    								"WHERE w.userName LIKE ?1 and w.userPassword LIKE ?2");
    	query.setParameter(1, userName);
    	query.setParameter(2, hashedPassword);
        return query.getResultList();
    }
	
}
