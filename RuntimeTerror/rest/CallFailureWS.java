package com.project.rest;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.project.csv.CsvConverter;

@Path("/networks")
@Stateless
@LocalBean
public class CallFailureWS {

	@EJB
	private CallFailureDAO callFailureDAO;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/all")
	public Response findAllCallFailures() {
		List<CallFailure> callFailures = callFailureDAO.getAllCallFailures();
		return Response.status(200).entity(callFailures).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/valid")
	public Response findValidCallFailures() {
		List<CallFailure> callFailures = callFailureDAO.getValidCallFailures();
		return Response.status(200).entity(callFailures).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/invalid")
	public Response findInvalidCallFailures() {
		List<CallFailure> callFailures = callFailureDAO.getInvalidCallFailures();
		return Response.status(200).entity(callFailures).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{imsi}")
	public Response findImsiEntered(@PathParam("imsi") String imsi) {
		List<CallFailure> callFailures = callFailureDAO.getImsiEntered(imsi);
		return Response.status(200).entity(callFailures).build();
	}

	// method to get all IMSI's with call failures within time period
	// http://localhost:8080/RuntimeTerror/rest/networks/query?fromDate=1/11/20
	// 17:15&toDate=1/11/20 17:18
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/query") // http://localhost:8080/networks/query
	public Response findIMSICallFailuresWithinTimePeriod(@QueryParam("fromDate") String fromDate,
			@QueryParam("toDate") String toDate) {
		List<CallFailure> callFailures = callFailureDAO.getIMSICallFailuresWithinTimePeriod(fromDate, toDate);
		System.out.println("find IMSI's with in Time period: " + fromDate + toDate);
		return Response.status(200).entity(callFailures).build();
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{imsi}")
	public Response findImsiEvents(@PathParam("imsi") float imsi) {
		List<CallFailure> callFailures = callFailureDAO.getCallFailure_IMSI(imsi);
		return Response.status(200).entity(callFailures).build();
	}
	
	@POST
	@Path("/upload")
	public Response saveCallFailure() throws IOException {
		callFailureDAO.loadData();
		return Response.status(200).build();
	}

}
